using SimpleInjector;
using SimpleInjector.Integration.Web.Mvc;
using System.Web.Mvc;
using TimeSheet.Shared.TimeSheetServices;

namespace MVCSimpleInjectorDemo.App_Start
{
    public class SimpleInjectorConfig
    {
        public void RegisterComponents()
        {
            TimeSheetServices timeSheetServices = new TimeSheetServices();
            Container container = new Container();
            container.Register(() => timeSheetServices.GetCountryService());
            container.Register(() => timeSheetServices.GetClientService());
            container.Verify();
            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
        }
    }
}
