﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;
using TimeSheet.Shared.Models.Interfaces;
using TimeSheet.Shared.Models.Implementation;
using TimeSheet.DAL.Repositories.Repository.Interfaces;
using TimeSheet.DAL.Repositories.DbService.Interfaces;

namespace TimeSheet.DAL.Repositories.Repository.Implementation
{
    public class ProjectDAL : IProjectDAL
    {
        private IDbService _dbService;

        public ProjectDAL(IDbService dbService)
        {
            if (dbService == null)
            {
                throw new ArgumentNullException("Value cannot be null", nameof(dbService));
            }
            _dbService = dbService;
        }

        public IEnumerable<IProject> GetProjects()
        {
            List<IProject> projectList = new List<IProject>() { };
            using (IDbConnection connection = _dbService.CreateDbConnection())
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.AddCommand("SELECT * FROM Projects");

                    using (IDataReader dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            projectList.Add(MapProject(dataReader));
                        }

                    }
                }
            }
            return projectList;
        }

        public IProject GetProjectById(Guid id)
        {
            using (IDbConnection connection = _dbService.CreateDbConnection())
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.AddCommand("SELECT * FROM Projects WHERE Id = @id");
                    command.Parameters.Add(command.CreateParameter("@id", id));

                    using (IDataReader dataReader = command.ExecuteReader(CommandBehavior.SingleRow))
                    {
                        if (dataReader.Read())
                        {
                            IProject project = MapProject(dataReader);
                            return project;
                        }
                    }
                }
            }
            throw new KeyNotFoundException("Project not found");
        }

        public void UpdateProjectById(IProject project)
        {
            using (IDbConnection connection = _dbService.CreateDbConnection())
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.AddCommand("Update Projects SET Name=@name, Description=@description, ClientId=@clientId, TeamMemberId=@teamMemberId, IsActive=@isActive WHERE Id=@id");
                    AddParameters(command, project);

                    try
                    {
                        command.ExecuteNonQuery(); 
                    }
                    catch (SqlException ex)
                    {
                        throw new ConstraintException(ex.Message);
                    }

                }
            }
        }

        public bool RemoveProjectById(Guid id)
        {
            using (IDbConnection connection = _dbService.CreateDbConnection())
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.AddCommand("DELETE FROM Projects WHERE Id=@id;");
                    command.Parameters.Add(command.CreateParameter("@id", id));
                    return command.ExecuteNonQuery() > 0;
                }
            }
        }

        public void AddProject(IProject newProject)
        {
            using (IDbConnection connection = _dbService.CreateDbConnection())
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.AddCommand("INSERT INTO Projects (Id, Name, Description, ClientId, TeamMemberId, IsActive) VALUES (@id, @name, @description, @clientId, @teamMemberId, @isActive)");
                    AddParameters(command, newProject);
                    try
                    {
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException ex)
                    {
                        throw new ConstraintException(ex.Message);
                    }
                }
            }
        }

        public IEnumerable<IProject> FilterProjectByClient(Guid clientId)
        {
            List<IProject> projectList = new List<IProject>();
            using (IDbConnection connection = _dbService.CreateDbConnection())
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.AddCommand("SELECT * FROM Projects INNER JOIN Clients ON Projects.ClientId = Clients.Id WHERE Clients.Id = @clientId;");
                    command.Parameters.Add(command.CreateParameter("@clientId", clientId));
                    using (IDataReader dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            projectList.Add(MapProject(dataReader));
                        }

                    }
                }
            }
            return projectList;
        }

        public IEnumerable<IProject> FilterProjectByClientAndTeamMember(Guid clientId, Guid teamMemberId)
        {
            List<IProject> projectList = new List<IProject>();
            using (IDbConnection connection = _dbService.CreateDbConnection())
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.AddCommand("SELECT * FROM Projects " +
                        "INNER JOIN Clients " +
                        "ON Projects.ClientId = Clients.Id " +
                        "INNER JOIN TeamMembers " +
                        "ON Projects.TeamMemberId = TeamMembers.Id " +
                        "WHERE Clients.Id = @clientId AND TeamMembers.Id = @teamMemberId;");
                    command.Parameters.Add(command.CreateParameter("@clientId", clientId));
                    command.Parameters.Add(command.CreateParameter("@teamMemberId", teamMemberId));
                    using (IDataReader dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            projectList.Add(MapProject(dataReader));
                        }

                    }
                }
            }
            return projectList;
        }

        public IEnumerable<IProject> FilterProjectsByName(string projectName)
        {
            List<IProject> projectList = new List<IProject>();
            using (IDbConnection connection = _dbService.CreateDbConnection())
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.AddCommand("SELECT * FROM Projects WHERE lower(Name) LIKE @name;");
                    command.Parameters.Add(command.CreateParameter("@name", $"%{projectName.ToLower()}%"));
                    using (IDataReader dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            projectList.Add(MapProject(dataReader));
                        }

                    }
                }
            }
            return projectList;
        }

        public IEnumerable<IProject> FilterProjectsByFirstLetter(char firstLetter)
        {
            List<IProject> projectList = new List<IProject>();
            using (IDbConnection connection = _dbService.CreateDbConnection())
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.AddCommand("SELECT * FROM Projects WHERE lower(Name) LIKE @name;");
                    command.Parameters.Add(command.CreateParameter("@name", $"{firstLetter.ToString().ToLower()}%"));

                    using (IDataReader dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            projectList.Add(MapProject(dataReader));

                        }
                    }
                }
            }
            return projectList;
        }

        private IProject MapProject(IDataRecord dataRecord)
              => new Project(
                                dataRecord.GetSafeGuid(0),
                                dataRecord.GetSafeString(1),
                                dataRecord.GetSafeGuid(2),
                                dataRecord.GetSafeGuid(3),
                                dataRecord.GetBoolean(4),
                                dataRecord.GetSafeString(5)
                      );
        private void AddParameters(IDbCommand command, IProject project)
        {
            command.Parameters.Add(command.CreateParameter("@id", project.Id.GetDBNull()));
            command.Parameters.Add(command.CreateParameter("@name", project.Name.GetDBNull()));
            command.Parameters.Add(command.CreateParameter("@description", project.Description.GetDBNull()));
            command.Parameters.Add(command.CreateParameter("@clientId", project.ClientId.GetDBNull()));
            command.Parameters.Add(command.CreateParameter("@teamMemberId", project.TeamMemberId.GetDBNull()));
            command.Parameters.Add(command.CreateParameter("@isActive", project.IsActive.GetDBNull()));
        }


    }
}

