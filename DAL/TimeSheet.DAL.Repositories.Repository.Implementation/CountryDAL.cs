﻿using System;
using System.Collections.Generic;
using System.Data;
using TimeSheet.DAL.Repositories.DbService.Interfaces;
using TimeSheet.DAL.Repositories.Repository.Interfaces;
using TimeSheet.Shared.Models.Implementation;
using TimeSheet.Shared.Models.Interfaces;

namespace TimeSheet.DAL.Repositories.Repository.Implementation
{
    public class CountryDAL : ICountryDAL
    {
        private IDbService _dbService;

        public CountryDAL(IDbService dbService)
        {
            if (dbService == null)
            {
                throw new ArgumentNullException("Value cannot be null", nameof(dbService));
            }
            _dbService = dbService;
        }

        public IEnumerable<ICountry> GetCountries()
        {
            List<ICountry> countryList = new List<ICountry>() { };
            using (IDbConnection connection = _dbService.CreateDbConnection())
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.AddCommand("SELECT * FROM Countries");

                    using (IDataReader dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            countryList.Add(new Country(dataReader.GetSafeGuid(0), dataReader.GetSafeString(1)));
                        }
                    }
                }
            }

            return countryList;
        }
    }
}

