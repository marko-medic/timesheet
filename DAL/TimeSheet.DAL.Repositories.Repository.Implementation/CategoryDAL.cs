﻿using System;
using System.Collections.Generic;
using System.Data;
using TimeSheet.DAL.Repositories.DbService.Interfaces;
using TimeSheet.DAL.Repositories.Repository.Interfaces;
using TimeSheet.Shared.Models.Implementation;
using TimeSheet.Shared.Models.Interfaces;

namespace TimeSheet.DAL.Repositories.Repository.Implementation
{
    public class CategoryDAL : ICategoryDAL
    {
        private IDbService _dbService;

        public CategoryDAL(IDbService dbService)
        {
            if (dbService == null)
            {
                throw new ArgumentNullException("Value cannot be null", nameof(dbService));
            }
            _dbService = dbService;
        }

        public IEnumerable<ICategory> GetCategories()
        {
            List<ICategory> categoryList = new List<ICategory>() { };
            using (IDbConnection connection = _dbService.CreateDbConnection())
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.AddCommand("SELECT * FROM Categories");

                    using (IDataReader dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            categoryList.Add(new Category(dataReader.GetSafeGuid(0), dataReader.GetSafeString(1)));
                        }
                    }
                }
            }

            return categoryList;
        }
    }
}

