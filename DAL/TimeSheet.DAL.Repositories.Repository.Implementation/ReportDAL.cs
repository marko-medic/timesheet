﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeSheet.DAL.Repositories.DbService.Interfaces;
using TimeSheet.DAL.Repositories.Repository.Interfaces;
using TimeSheet.Shared.Models.Implementation;
using TimeSheet.Shared.Models.Interfaces;

namespace TimeSheet.DAL.Repositories.Repository.Implementation
{
    public class ReportDAL : IReportDAL
    {
        private IDbService _dbService;

        public ReportDAL(IDbService dbService)
        {
            if (dbService == null)
            {
                throw new ArgumentNullException("Db service cannot be null", nameof(dbService));
            }
            _dbService = dbService;
        }

        public IEnumerable<IReport> GetAllReports()
        {
            List<IReport> reportList = new List<IReport>() { };
            using (IDbConnection connection = _dbService.CreateDbConnection())
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    string sqlQuery = "SELECT DISTINCT" +
                        " DayRecords.DayDate as Date," +
                        " TeamMembers.Name as TeamMemberName," +
                        " Projects.Name as ProjectName," +
                        " Categories.Name as CategoryName," +
                        " DayRecords.Description as Description," +
                        " DayRecords.Time as Time" +
                        " FROM TeamMembers" +
                        " INNER JOIN Projects ON TeamMembers.Id = Projects.TeamMemberId" +
                        " INNER JOIN Clients ON Clients.Id = Projects.ClientId" +
                        " INNER JOIN DayRecords ON Projects.Id = DayRecords.ProjectId" +
                        " INNER JOIN Categories ON DayRecords.CategoryId = Categories.Id";
                        //" WHERE DayRecords.DayDate = CONVERT(VARCHAR(10),GETDATE(),10)";
                    command.AddCommand(sqlQuery);


                    using (IDataReader dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            reportList.Add(MapReport(dataReader));
                        }

                    }
                }
            }
            return reportList;
        }

        public IEnumerable<IReport> GetFilteredReports(IReportSearch reportSearch)
        {
            List<IReport> reportList = new List<IReport>() { };
            using (IDbConnection connection = _dbService.CreateDbConnection())
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    string sqlQuery = "SELECT DISTINCT" +
                        " DayRecords.DayDate as Date," +
                        " TeamMembers.Name as TeamMemberName," +
                        " Projects.Name as ProjectName," +
                        " Categories.Name as CategoryName," +
                        " DayRecords.Description as Description," +
                        " DayRecords.Time as Time" +
                        " FROM TeamMembers" +
                        " INNER JOIN Projects ON TeamMembers.Id = Projects.TeamMemberId" +
                        " INNER JOIN Clients ON Clients.Id = Projects.ClientId" +
                        " INNER JOIN DayRecords ON Projects.Id = DayRecords.ProjectId" +
                        " INNER JOIN Categories ON DayRecords.CategoryId = Categories.Id" +
                        " WHERE (TeamMembers.Id=@teamMemberId OR @teamMemberId IS NULL)" +
                        " AND (Clients.Id=@clientId OR @clientId IS NULL)" +
                        " AND (Projects.Id = @projectId OR @projectId IS NULL)" +
                        " AND (Categories.Id = @categoryId OR @categoryId IS NULL)" +
                        " AND (DayRecords.DayDate >= @startDate OR @startDate IS NULL)" +
                        " AND (DayRecords.DayDate <= @endDate OR @endDate IS NULL);";
                    
                    command.AddCommand(sqlQuery);
                    AddParameters(command, reportSearch);

                    using (IDataReader dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            reportList.Add(MapReport(dataReader));
                        }

                    }
                }
            }
            return reportList;
        }


        private IReport MapReport(IDataReader dataRecord)
            => new Report(
                    dataRecord.GetDateTime(0),
                    dataRecord.GetSafeString(1),
                    dataRecord.GetSafeString(2),
                    dataRecord.GetSafeString(3),
                    dataRecord.GetSafeString(4),
                    dataRecord.GetSafeDecimal(5)
                );


        private void AddParameters(IDbCommand command, IReportSearch reportSearch)
        {
            command.Parameters.Add(command.CreateParameter("@teamMemberId", reportSearch.TeamMemberId.GetDBNull()));
            command.Parameters.Add(command.CreateParameter("@clientId", reportSearch.ClientId.GetDBNull()));
            command.Parameters.Add(command.CreateParameter("@projectId", reportSearch.ProjectId.GetDBNull()));
            command.Parameters.Add(command.CreateParameter("@categoryId", reportSearch.CategoryId.GetDBNull()));
            command.Parameters.Add(command.CreateParameter("@startDate", reportSearch.StartDate.GetDBNull()));
            command.Parameters.Add(command.CreateParameter("@endDate", reportSearch.EndDate.GetDBNull()));
        }
    }
}

