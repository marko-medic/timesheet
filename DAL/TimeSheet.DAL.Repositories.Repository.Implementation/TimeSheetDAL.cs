﻿using System;
using System.Collections.Generic;
using System.Data;
using TimeSheet.DAL.Repositories.DbService.Interfaces;
using TimeSheet.DAL.Repositories.Repository.Interfaces;
using TimeSheet.Shared.Models.Interfaces;
using TimeSheet.Shared.Models.Implementation;
using System.Data.SqlClient;

namespace TimeSheet.DAL.Repositories.Repository.Implementation
{
    public class TimeSheetDAL : ITimeSheetDAL
    {
        private IDbService _dbService;

        public TimeSheetDAL(IDbService dbService)
        {
            if (dbService == null)
            {
                throw new ArgumentNullException("Value cannot be null", nameof(dbService));
            }
            _dbService = dbService;
        }


        public IEnumerable<IDay> GetTimeSheets(DateTime begin, DateTime end)
        {
            List<IDay> dayList = new List<IDay>() { };
            using (IDbConnection connection = _dbService.CreateDbConnection())
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.AddCommand("SELECT DISTINCT DayDate FROM DayRecords WHERE DayDate >= @begin AND DayDate <= @end ORDER BY DayDate");
                    command.Parameters.Add(command.CreateParameter("@begin", begin));
                    command.Parameters.Add(command.CreateParameter("@end", end));

                    using (IDataReader dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            dayList.Add(new Day(dataReader.GetDateTime(0)));
                        }

                    }
                }
            }
            return dayList;
        }

        public IEnumerable<IDayRecord> GetTimeSheetDay(DateTime date)
        {
            List<IDayRecord> recordList = new List<IDayRecord>() { };
            using (IDbConnection connection = _dbService.CreateDbConnection())
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.AddCommand("SELECT * FROM DayRecords WHERE DayDate = @date");
                    command.Parameters.Add(command.CreateParameter("@date", date));

                    using (IDataReader dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            recordList.Add(new DayRecord(
                                dataReader.GetSafeGuid(0),
                                dataReader.GetDateTime(1),
                                dataReader.GetSafeGuid(2),
                                dataReader.GetSafeGuid(3),
                                dataReader.GetSafeDecimal(4),
                                dataReader.GetSafeString(5),
                                dataReader.GetSafeDecimal(6)
                             ));
                        }

                    }
                }
            }
            return recordList;
        }

        public void AddTimeSheetDay(IDayRecord record)
        {
            using (IDbConnection connection = _dbService.CreateDbConnection())
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                  command.AddCommand("INSERT INTO DayRecords (Id, DayDate, ProjectId, CategoryId, Time, Description, Overtime) VALUES (@id, @dayDate, @projectId, @categoryId, @time, @description, @overtime)");
                  AddParameters(command, record);
                    try
                    {
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException ex)
                    {
                        throw new ConstraintException(ex.Message);
                    }

                }
            }
        }

        public bool DeleteRecordsByDate(DateTime date)
        {
            using (IDbConnection connection = _dbService.CreateDbConnection())
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.AddCommand("DELETE FROM DayRecords WHERE DayDate = @dayDate");
                    command.Parameters.Add(command.CreateParameter("@dayDate", date));
    
                    try
                    {    
                        return command.ExecuteNonQuery() > 0;
                    }
                    catch (SqlException ex)
                    {
                        throw new ConstraintException(ex.Message);
                    }

                }
            }
        }

        public decimal GetTotalHours(DateTime begin, DateTime end)
        {
            decimal totalHours = 0;
            using (IDbConnection connection = _dbService.CreateDbConnection())
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.AddCommand("SELECT Sum(Time) FROM DayRecords WHERE dayDate >= @begin AND dayDate <= @end");
                    command.Parameters.Add(command.CreateParameter("@begin", begin));
                    command.Parameters.Add(command.CreateParameter("@end", end));

                    using (IDataReader dataReader = command.ExecuteReader(CommandBehavior.SingleRow))
                    {
                        if (dataReader.Read())
                        {
                            totalHours = dataReader.GetSafeDecimal(0);
                        }

                    }
                }
            }
            return totalHours;
        }


        private void AddParameters(IDbCommand command, IDayRecord dayRecord)
        {
            command.Parameters.Add(command.CreateParameter("@id", dayRecord.Id.GetDBNull()));
            command.Parameters.Add(command.CreateParameter("@dayDate", dayRecord.DayDate.GetDBNull()));
            command.Parameters.Add(command.CreateParameter("@projectId", dayRecord.ProjectId.GetDBNull()));
            command.Parameters.Add(command.CreateParameter("@categoryId", dayRecord.CategoryId.GetDBNull()));
            command.Parameters.Add(command.CreateParameter("@time", dayRecord.Time.GetDBNull()));
            command.Parameters.Add(command.CreateParameter("@description", dayRecord.Description.GetDBNull()));
            command.Parameters.Add(command.CreateParameter("@overtime", dayRecord.Overtime.GetDBNull()));
        }

    }
}
