﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeSheet.DAL.Repositories.DbService.Interfaces;
using TimeSheet.DAL.Repositories.Repository.Interfaces;
using TimeSheet.Shared.Models.Implementation;
using TimeSheet.Shared.Models.Interfaces;

namespace TimeSheet.DAL.Repositories.Repository.Implementation
{
    public class TeamMemberDAL : ITeamMemberDAL
    {
        private IDbService _dbService;
        public TeamMemberDAL(IDbService dbService)
        {
            if (dbService == null)
            {
                throw new ArgumentNullException("Value cannot be null", nameof(dbService));
            }
            _dbService = dbService;
        }

        public IEnumerable<ITeamMember> GetTeamMembers()
        {
            List<ITeamMember> teamMemberList = new List<ITeamMember>() { };
            using (IDbConnection connection = _dbService.CreateDbConnection())
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.AddCommand("SELECT * FROM TeamMembers");

                    using (IDataReader dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            teamMemberList.Add(MapTeamMember(dataReader));
                        }

                    }
                }
            }
            return teamMemberList;
        }

        public ITeamMember GetTeamMemberById(Guid id)
        {
            using (IDbConnection connection = _dbService.CreateDbConnection())
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.AddCommand("SELECT * FROM TeamMembers WHERE Id = @id");
                    command.Parameters.Add(command.CreateParameter("@id", id));

                    using (IDataReader dataReader = command.ExecuteReader(CommandBehavior.SingleRow))
                    {
                        if (dataReader.Read())
                        {
                            ITeamMember teamMember = MapTeamMember(dataReader);
                            return teamMember;
                        }
                    }
                }
            }
            throw new KeyNotFoundException("TeamMember not found");
        }

        public void AddTeamMember(ITeamMember newTeamMember)
        {
            using (IDbConnection connection = _dbService.CreateDbConnection())
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.AddCommand("INSERT INTO TeamMembers (Id, Name, Username, HoursPerWeek, Email, IsActive, IsAdmin) VALUES (@id, @name, @username, @hoursPerWeek, @email, @isActive, @isAdmin)");
                    AddParameters(command, newTeamMember);
                    try
                    {
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException ex)
                    {
                        throw new ConstraintException(ex.Message);
                    }
                }
            }
        }

        public bool RemoveTeamMemberById(Guid id)
        {
            using (IDbConnection connection = _dbService.CreateDbConnection())
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.AddCommand("DELETE FROM TeamMembers WHERE Id=@id;");
                    command.Parameters.Add(command.CreateParameter("@id", id));
                    return command.ExecuteNonQuery() > 0;
                }
            }
        }

        public void UpdateTeamMemberById(ITeamMember teamMemberToEdit)
        {
            using (IDbConnection connection = _dbService.CreateDbConnection())
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.AddCommand("Update TeamMembers SET Name=@name, Username=@username, hoursPerWeek=@hoursPerWeek, Email=@email, IsActive=@isActive, IsAdmin=@isAdmin WHERE Id=@id");
                    AddParameters(command, teamMemberToEdit);

                    if (command.ExecuteNonQuery() == 0)
                    {
                        throw new KeyNotFoundException("TeamMember not found");
                    }

                }
            }
        }

        private ITeamMember MapTeamMember(IDataRecord dataRecord)
             => new TeamMember(
                               dataRecord.GetSafeGuid(0),
                               dataRecord.GetSafeString(1),
                               dataRecord.GetSafeString(2),
                               dataRecord.GetDecimal(3),
                               dataRecord.GetSafeString(4),
                               dataRecord.GetBoolean(5),
                               dataRecord.GetBoolean(6)
                     );

        private void AddParameters(IDbCommand command, ITeamMember teamMember)
        {
            command.Parameters.Add(command.CreateParameter("@id", teamMember.Id.GetDBNull()));
            command.Parameters.Add(command.CreateParameter("@name", teamMember.Name.GetDBNull()));
            command.Parameters.Add(command.CreateParameter("@username", teamMember.Username.GetDBNull()));
            command.Parameters.Add(command.CreateParameter("@hoursPerWeek", teamMember.HoursPerWeek.GetDBNull()));
            command.Parameters.Add(command.CreateParameter("@email", teamMember.Email.GetDBNull()));
            command.Parameters.Add(command.CreateParameter("@isActive", teamMember.IsActive.GetDBNull()));
            command.Parameters.Add(command.CreateParameter("@isAdmin", teamMember.IsAdmin.GetDBNull()));
        }
    }
}
