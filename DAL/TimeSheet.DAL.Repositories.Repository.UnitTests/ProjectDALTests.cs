﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TimeSheet.DAL.Repositories.Repository.Interfaces;
using TimeSheet.DAL.Repositories.Repository.Implementation;
using TimeSheet.DAL.Repositories.DbService.Implementation;
using TimeSheet.Shared.Models.Interfaces;
using TimeSheet.Shared.Models.Implementation;
using System.Data;

namespace TimeSheet.DAL.Repositories.Repository.UnitTests
{
    [TestClass]
    public class ProjectDALTests
    {
        private string _connectionStringName = "Connection";
        private DbSeeder _dbSeeder;

        [TestInitialize]
        public void TestInitialize()
        {
            _dbSeeder = new DbSeeder(_connectionStringName);
            _dbSeeder.ResetTables();
        }

        [TestMethod]
        public void ProjectDAL_InitWithValidDBService_ReturnsInstance()
        {
            IProjectDAL projectDAL = new ProjectDAL(new DBService(new DbConnectionService(_connectionStringName)));
            Assert.IsTrue(projectDAL != null);
        }

        [TestMethod]
        public void ProjectDAL_InitWithNullDBService_ThrowsArgumentNullException()
        {
            Assert.ThrowsException<ArgumentNullException>(() => new ProjectDAL(null));
        }

        [TestMethod]
        public void GetProjects_GetProjectsFromNonEmptyTable_ReturnsTableWithList()
        {
            IProjectDAL projectDAL = new ProjectDAL(new DBService(new DbConnectionService(_connectionStringName)));
            List<IProject> projectList = projectDAL.GetProjects().ToList();
            Assert.IsTrue(projectList.Count() == DbSeeder.projectList.Count());
        }



        [TestMethod]
        public void GetProjects_GetProjectsFromEmptyTable_ReturnsEmptyTable()
        {
            _dbSeeder.EmptyTables();
            IProjectDAL projectDAL = new ProjectDAL(new DBService(new DbConnectionService(_connectionStringName)));
            IEnumerable<IProject> projectList = projectDAL.GetProjects();
            Assert.IsTrue(projectList.Count() == 0);
        }


        [TestMethod]
        public void UpdateProjectById_PassValidProjectObject_ReturnsTableWithUpdatedProject()
        {
            IProjectDAL projectDAL = new ProjectDAL(new DBService(new DbConnectionService(_connectionStringName)));
            IProject newProject = new Project(DbSeeder.TestProjectId, "Foo", DbSeeder.TestClientId, DbSeeder.TestTeamMemberId, true, "Desc");
            projectDAL.UpdateProjectById(newProject);
            List<IProject> updatedList = projectDAL.GetProjects().ToList();
            IProject updatedProject = updatedList.Find(project => project.Id == DbSeeder.TestProjectId);
            Assert.IsTrue(updatedProject.Name == "Foo" && updatedProject.Description == "Desc");
        }


        [TestMethod]
        public void UpdateProjectById_PassInvalidProjectObject_ThrowsKeyNotFoundException()
        {
            IProjectDAL projectDAL = new ProjectDAL(new DBService(new DbConnectionService(_connectionStringName)));
            IProject newProject = new Project(Guid.NewGuid(), "Foo", DbSeeder.TestClientId, DbSeeder.TestTeamMemberId, true, "Desc");
            Assert.ThrowsException<KeyNotFoundException>(() => projectDAL.UpdateProjectById(newProject));
        }

        [TestMethod]
        public void RemoveProjectById_PassExistingProjectId_ReturnsTableWithoutDeletedProject()
        {
            IProjectDAL projectDAL = new ProjectDAL(new DBService(new DbConnectionService(_connectionStringName)));
            IEnumerable<IProject> projectList = projectDAL.GetProjects();
            int oldCount = projectList.Count();
            bool isProjectDeleted = projectDAL.RemoveProjectById(DbSeeder.TestProjectId);
            int newCount = projectDAL.GetProjects().Count();
            Assert.IsTrue(isProjectDeleted && newCount == oldCount - 1);
        }

        [TestMethod]
        public void RemoveProjectById_PassNonExistingProjectId_ReturnsSameTable()
        {
            IProjectDAL projectDAL = new ProjectDAL(new DBService(new DbConnectionService(_connectionStringName)));
            IEnumerable<IProject> projectList = projectDAL.GetProjects();
            int oldCount = projectList.Count();
            bool isProjectDeleted = projectDAL.RemoveProjectById(Guid.NewGuid());
            int newCount = projectDAL.GetProjects().Count();
            Assert.IsTrue(!isProjectDeleted && newCount == oldCount);
        }

        [TestMethod]
        public void GetProjectById_PassValidGuid_ReturnsProjectInstance()
        {
            IProjectDAL projectDAL = new ProjectDAL(new DBService(new DbConnectionService(_connectionStringName)));
            IProject project = projectDAL.GetProjectById(DbSeeder.TestProjectId);
            Assert.IsTrue(project != null);
        }

        [TestMethod]
        public void GetProjectById_PassNewGuid_ThrowsKeyNotFoundException()
        {
            IProjectDAL projectDAL = new ProjectDAL(new DBService(new DbConnectionService(_connectionStringName)));
            Assert.ThrowsException<KeyNotFoundException>(() => projectDAL.GetProjectById(Guid.NewGuid()));
        }

        [TestMethod]
        public void AddProject_PassNewProject_ReturnsTableWithAddedProject()
        {
            IProjectDAL projectDAL = new ProjectDAL(new DBService(new DbConnectionService(_connectionStringName)));
            IEnumerable<IProject> projectList = projectDAL.GetProjects();
            int oldCount = projectList.Count();
            Guid id = Guid.NewGuid();
            projectDAL.AddProject(new Project(id, "Foo", DbSeeder.TestClientId, DbSeeder.TestTeamMemberId, true, "Desc"));
            List<IProject> newList = projectDAL.GetProjects().ToList();
            bool isNewProjectFound = newList.FindIndex(project => project.Id == id) > -1;
            Assert.IsTrue(isNewProjectFound && newList.Count() == oldCount + 1);
        }

        [TestMethod]
        public void AddProject_PassProjectWithSameId_ThrowsException()
        {
            IProjectDAL projectDAL = new ProjectDAL(new DBService(new DbConnectionService(_connectionStringName)));
            Assert.ThrowsException<ConstraintException>(() => projectDAL.AddProject(new Project(DbSeeder.TestProjectId, "Foo", DbSeeder.TestClientId, DbSeeder.TestTeamMemberId, true, "Desc")));
        }


        [TestMethod]
        public void FilterProjectsByName_PassExistingProjectName_ReturnsTableFiltSearchedProject()
        {
            IProjectDAL projectDAL = new ProjectDAL(new DBService(new DbConnectionService(_connectionStringName)));
            List<IProject> filteredList = projectDAL.FilterProjectsByName(DbSeeder.TestProjectName).ToList();
            bool isProjectInList = filteredList.FindIndex(project => project.Name == DbSeeder.TestProjectName) > -1;
            Assert.IsTrue(isProjectInList);
        }

        [TestMethod]
        public void FilterProjectsByName_PassNonExistingProjectName_ReturnsEmptyTable()
        {
            IProjectDAL projectDAL = new ProjectDAL(new DBService(new DbConnectionService(_connectionStringName)));
            IEnumerable<IProject> filteredList = projectDAL.FilterProjectsByName("NonExistingName");
            Assert.IsTrue(filteredList.Count() == 0);
        }

        [TestMethod]
        public void FilterProjectsFirstLetter_PassExistingFirstLetter_ReturnsTableFiltSearchedProject()
        {
            IProjectDAL projectDAL = new ProjectDAL(new DBService(new DbConnectionService(_connectionStringName)));
            List<IProject> filteredList = projectDAL.FilterProjectsByFirstLetter(DbSeeder.TestProjectName[0]).ToList();
            bool isProjectInList = filteredList.FindIndex(project => project.Name == DbSeeder.TestProjectName) > -1;
            Assert.IsTrue(isProjectInList);
        }

        [TestMethod]
        public void FilterProjectsByName_PassNonExistingProjectFirstLetter_ReturnsEmptyTable()
        {
            IProjectDAL projectDAL = new ProjectDAL(new DBService(new DbConnectionService(_connectionStringName)));
            IEnumerable<IProject> filteredList = projectDAL.FilterProjectsByFirstLetter(']');
            Assert.IsTrue(filteredList.Count() == 0);
        }
    }
}
