﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeSheet.DAL.Repositories.DbService.Implementation;
using TimeSheet.DAL.Repositories.Repository.Implementation;
using TimeSheet.Shared.Models.Implementation;
using TimeSheet.Shared.Models.Interfaces;

namespace TimeSheet.DAL.Repositories.Repository.UnitTests
{
    [TestClass]
    public class TeamMemberDALTests
    {
        private string _connectionStringName = "Connection";
        private DbSeeder _dbSeeder;

        [TestInitialize]
        public void TestInitialize()
        {
            _dbSeeder = new DbSeeder(_connectionStringName);
            _dbSeeder.ResetTables();
        }

        [TestMethod]
        public void TeamMemberDAL_InitWithValidDBService_ReturnsInstance()
        {
            TeamMemberDAL teamMemberDAL = new TeamMemberDAL(new DBService(new DbConnectionService(_connectionStringName)));
            Assert.IsTrue(teamMemberDAL != null);
        }

        [TestMethod]
        public void TeamMemberDAL_InitWithNullDBService_ThrowsArgumentNullException()
        {
            Assert.ThrowsException<ArgumentNullException>(() => new TeamMemberDAL(null));
        }

        [TestMethod]
        public void GetTeamMembers_GetTeamMembersFromNonEmptyTable_ReturnsTableWithList()
        {
            TeamMemberDAL teamMemberDAL = new TeamMemberDAL(new DBService(new DbConnectionService(_connectionStringName)));
            List<ITeamMember> teamMemberList = teamMemberDAL.GetTeamMembers().ToList();
            Assert.IsTrue(teamMemberList.Count() == DbSeeder.teamMemberList.Count());
        }

        [TestMethod]
        public void GetTeamMembers_GetTeamMembersFromEmptyTable_ReturnsEmptyTable()
        {
            _dbSeeder.EmptyTables();
            TeamMemberDAL teamMemberDAL = new TeamMemberDAL(new DBService(new DbConnectionService(_connectionStringName)));
            IEnumerable<ITeamMember> teamMemberList = teamMemberDAL.GetTeamMembers();
            Assert.IsTrue(teamMemberList.Count() == 0);
        }

        [TestMethod]
        public void UpdatTeamMemberById_PassValidTeamMember_ReturnsTableWithUpdatedTeamMember()
        {
            TeamMemberDAL teamMemberDAL = new TeamMemberDAL(new DBService(new DbConnectionService(_connectionStringName)));
            ITeamMember newTeamMember = new TeamMember(DbSeeder.TestTeamMemberId, "Marko", "m", 23, "mail@mail.com", true, true);
            teamMemberDAL.UpdateTeamMemberById(newTeamMember);
            List<ITeamMember> updatedList = teamMemberDAL.GetTeamMembers().ToList();
            ITeamMember updatedTeamMember = updatedList.Find(tm => tm.Id == DbSeeder.TestTeamMemberId);
            Assert.IsTrue(updatedTeamMember.Id == DbSeeder.TestTeamMemberId && updatedTeamMember.Name == "Marko");
        }

        [TestMethod]
        public void UpdateTeamMemberById_PassNonExistingTeamMember_ThrowsKeyNotFoundException()
        {
            TeamMemberDAL teamMemberDAL = new TeamMemberDAL(new DBService(new DbConnectionService(_connectionStringName)));
            ITeamMember newTeamMember = new TeamMember(Guid.NewGuid(), "Marko", "m", 23, "mail@mail.com", true, true);
            Assert.ThrowsException<KeyNotFoundException>(() => teamMemberDAL.UpdateTeamMemberById(newTeamMember));
        }

        [TestMethod]
        public void RemoveTeamMemberById_PassExistingTeamMemberId_ReturnsTableWithoutDeletedTeamMember()
        {
            _dbSeeder.EmptyProjectTable();
            TeamMemberDAL teamMemberDAL = new TeamMemberDAL(new DBService(new DbConnectionService(_connectionStringName)));
            IEnumerable<ITeamMember> teamMemberList = teamMemberDAL.GetTeamMembers();
            int oldCount = teamMemberList.Count();
            bool isTeamMemberDeleted = teamMemberDAL.RemoveTeamMemberById(DbSeeder.TestTeamMemberId);
            int newCount = teamMemberDAL.GetTeamMembers().Count();
            Assert.IsTrue(isTeamMemberDeleted && newCount == oldCount - 1);
        }

        [TestMethod]
        public void RemoveTeamMemberById_TryRemoveWhenInProjects_ThrowsException()
        {
            TeamMemberDAL teamMemberDAL = new TeamMemberDAL(new DBService(new DbConnectionService(_connectionStringName)));
            Assert.ThrowsException<SqlException>(() => teamMemberDAL.RemoveTeamMemberById(DbSeeder.TestTeamMemberId));
        }

        [TestMethod]
        public void RemoveTeamMemberById_PassNonExistingTeamMemberId_ReturnsSameTable()
        {
            TeamMemberDAL teamMemberDAL = new TeamMemberDAL(new DBService(new DbConnectionService(_connectionStringName)));
            IEnumerable<ITeamMember> teamMemberList = teamMemberDAL.GetTeamMembers();
            int oldCount = teamMemberList.Count();
            bool isTeamMemberDeleted = teamMemberDAL.RemoveTeamMemberById(Guid.NewGuid());
            int newCount = teamMemberDAL.GetTeamMembers().Count();
            Assert.IsTrue(!isTeamMemberDeleted && newCount == oldCount);
        }

        [TestMethod]
        public void GetTeamMemberById_PassValidGuid_ReturnsTeamMemberInstance()
        {
            TeamMemberDAL teamMemberDAL = new TeamMemberDAL(new DBService(new DbConnectionService(_connectionStringName)));
            ITeamMember teamMember = teamMemberDAL.GetTeamMemberById(DbSeeder.TestTeamMemberId);
            Assert.IsTrue(teamMember != null);
        }

        [TestMethod]
        public void GetTeamMemberById_PassNewGuid_ThrowsKeyNotFoundException()
        {
            TeamMemberDAL teamMemberDAL = new TeamMemberDAL(new DBService(new DbConnectionService(_connectionStringName)));
            Assert.ThrowsException<KeyNotFoundException>(() => teamMemberDAL.GetTeamMemberById(Guid.NewGuid()));
        }

        [TestMethod]
        public void AddTeamMember_PassNewTeamMember_ReturnsTableWithAddedTeamMember()
        {
            TeamMemberDAL teamMemberDAL = new TeamMemberDAL(new DBService(new DbConnectionService(_connectionStringName)));
            IEnumerable<ITeamMember> teamMemberList = teamMemberDAL.GetTeamMembers();
            int oldCount = teamMemberList.Count();
            Guid id = Guid.NewGuid();
            teamMemberDAL.AddTeamMember(new TeamMember(id, "Marko", "m", 4, "mail@mail.com", true, true));
            List<ITeamMember> newList = teamMemberDAL.GetTeamMembers().ToList();
            bool isNewTeamMemberFound = newList.FindIndex(tm => tm.Id == id) > -1;
            Assert.IsTrue(isNewTeamMemberFound && newList.Count() == oldCount + 1);
        }

        [TestMethod]
        public void AddTeamMember_PassTeamMemberWithExistingId_ThrowsException()
        {
            TeamMemberDAL teamMemberDAL = new TeamMemberDAL(new DBService(new DbConnectionService(_connectionStringName)));
            Assert.ThrowsException<ConstraintException>(() => teamMemberDAL.AddTeamMember(new TeamMember(DbSeeder.TestTeamMemberId, "Marko", "m", 23, "mail@mail.com", true, true)));
        }

    }
}
