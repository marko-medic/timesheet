﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TimeSheet.DAL.Repositories.Repository.Interfaces;
using TimeSheet.DAL.Repositories.Repository.Implementation;
using TimeSheet.DAL.Repositories.DbService.Implementation;
using TimeSheet.Shared.Models.Interfaces;

namespace TimeSheet.DAL.Repositories.Repository.UnitTests
{
    [TestClass]
    public class CategoryDALTests
    {
        private string _connectionStringName = "Connection";
        private DbSeeder _dbSeeder;

        [TestInitialize]
        public void TestInitialize()
        {
            _dbSeeder = new DbSeeder(_connectionStringName);
            _dbSeeder.ResetTables();
        }

        [TestMethod]
        public void CategoryDAL_InitWithValidDBService_ReturnsInstance()
        {
            ICategoryDAL categoryDAL = new CategoryDAL(new DBService(new DbConnectionService(_connectionStringName)));
            Assert.IsTrue(categoryDAL != null);
        }

        [TestMethod]
        public void GetCategories_GetCategoriesFromNonEmptyTable_ReturnsTableWithList()
        {
            ICategoryDAL categoryDAL = new CategoryDAL(new DBService(new DbConnectionService(_connectionStringName)));
            List<ICategory> categoryList = categoryDAL.GetCategories().ToList();
            Assert.IsTrue(categoryList.Count() == DbSeeder.categoryList.Count());
        }

        [TestMethod]
        public void GetCategories_GetCategoriesFromEmptyTable_ReturnsEmptyTable()
        {
            _dbSeeder.EmptyTables();
            ICategoryDAL categoryDAL = new CategoryDAL(new DBService(new DbConnectionService(_connectionStringName)));
            IEnumerable<ICategory> categoryList = categoryDAL.GetCategories();
            Assert.IsTrue(categoryList.Count() == 0);
        }
    }
}
