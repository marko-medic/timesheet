﻿using System;
using System.Data;
using System.Collections.Generic;
using TimeSheet.DAL.Repositories.DbService.Interfaces;
using TimeSheet.DAL.Repositories.DbService.Implementation;
using TimeSheet.DAL.Repositories.Repository.Implementation;
using TimeSheet.Shared.Models.Interfaces;
using TimeSheet.Shared.Models.Implementation;

namespace TimeSheet.DAL.Repositories.Repository.UnitTests
{
    public class DbSeeder
    {
        public static Guid TestCountryId = Guid.Parse("cb77cce6-c2cb-473b-bdd2-5dac8c93b756");
        public static Guid TestCategoryId = Guid.Parse("cb77cce6-c2cb-473b-bdd9-5dac8c93b756");
        public static Guid TestClientId = Guid.Parse("cb77cce6-c2cb-472b-bdd1-5dac8c93b756");
        public static Guid TestTeamMemberId = Guid.Parse("cb77cce6-c2cb-474b-bdd1-5dac8c93b756");
        public static Guid TestProjectId = Guid.Parse("cb77cce6-c2cb-474b-bdd7-5dac8c93b756");

        public static string TestClientName = "Max";
        public static List<ICountry> countryList = new List<ICountry>() { new Country(TestCountryId, "Serbia") };
        public static List<ICategory> categoryList = new List<ICategory>() { new Category(TestCategoryId, "Front end") };
        public static List<IClient> clientList = new List<IClient>() { new Client(TestClientId, TestClientName, "Ns", "Addr", "2122", TestCountryId) };
        public static List<ITeamMember> teamMemberList = new List<ITeamMember>() { new TeamMember(TestTeamMemberId, "Tim", "t", 15, "mail@mail.com", false, false) };
        public static List<IProject> projectList = new List<IProject>() { new Project(TestProjectId, "ProjectX", TestClientId, TestTeamMemberId, true, "opis") };
        public static string TestProjectName = "ProjectX";
        private IDbService _dbService;

        public DbSeeder(string connectionStringName)
        {
            if (string.IsNullOrEmpty(connectionStringName))
            {
                throw new ArgumentNullException("Value cannot be null or empty string", nameof(connectionStringName));
            }
            _dbService = new DBService(new DbConnectionService(connectionStringName));
        }

        public void ResetTables()
        {
            EmptyTables();

            foreach (ICountry country in countryList)
            {
                ExecuteQuery($"INSERT INTO [dbo].[Country] ([Id], [Name]) VALUES (N'{country.Id}', N'{country.Name}');");
            }

            foreach (ICategory category in categoryList)
            {
                ExecuteQuery($"INSERT INTO [dbo].[Category] ([Id], [Name]) VALUES (N'{category.Id}', N'{category.Name}');");
            }

            foreach (IClient client in clientList)
            {
                ExecuteQuery($"INSERT INTO [dbo].[Client] ([Id], [Name], [Address], [City], [ZipCode], [CountryId]) VALUES (N'{client.Id}', N'{client.Name}', N'{client.City}', N'{client.Address}', N'{client.ZipCode}', N'{client.CountryId}');");
            }

            foreach (ITeamMember teamMember in teamMemberList)
            {
                ExecuteQuery($"INSERT INTO [dbo].[TeamMember] ([Id], [Name], [Username], [HoursPerWeek], [Email], [IsActive], [IsAdmin]) VALUES (N'{teamMember.Id}', N'{teamMember.Name}', N'{teamMember.Username}', N'{teamMember.HoursPerWeek}', N'{teamMember.Email}', N'{teamMember.IsActive}', N'{teamMember.IsAdmin}');");
            }

            foreach (IProject project in projectList)
            {
                ExecuteQuery($"INSERT INTO [dbo].[Project] ([Id], [Name], [Description], [ClientId], [TeamMemberId], [IsActive]) VALUES (N'{project.Id}', N'{project.Name}', N'{project.Description}', N'{project.ClientId}', N'{project.TeamMemberId}', N'{project.IsActive}');");
            }
        }

        public void EmptyTables()
        {
            ExecuteQuery("DELETE FROM Project");
            ExecuteQuery("DELETE FROM Client");
            ExecuteQuery("DELETE FROM TeamMember");
            ExecuteQuery("DELETE FROM Country");
            ExecuteQuery("DELETE FROM Category");
        }

        public void EmptyProjectTable()
        {
            ExecuteQuery("DELETE FROM PROJECT");
        }

        private void ExecuteQuery(string sql)
        {
            using (IDbConnection connection = _dbService.CreateDbConnection())
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.AddCommand(sql);
                    command.ExecuteNonQuery();
                }
            }
        }

    }

}

