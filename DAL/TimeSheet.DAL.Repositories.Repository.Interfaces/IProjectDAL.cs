﻿using System;
using System.Collections.Generic;
using TimeSheet.Shared.Models.Interfaces;

namespace TimeSheet.DAL.Repositories.Repository.Interfaces
{
    public interface IProjectDAL
    {
        IEnumerable<IProject> GetProjects();
        void UpdateProjectById(IProject updatedProject);
        bool RemoveProjectById(Guid projectId);
        void AddProject(IProject newProject);
        IProject GetProjectById(Guid id);
        IEnumerable<IProject> FilterProjectByClient(Guid clientId);
        IEnumerable<IProject> FilterProjectByClientAndTeamMember(Guid clientId, Guid teamMemberId);
        IEnumerable<IProject> FilterProjectsByName(string projectName);
        IEnumerable<IProject> FilterProjectsByFirstLetter(char firstLetter);
    }
}

