﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeSheet.Shared.Models.Interfaces;

namespace TimeSheet.DAL.Repositories.Repository.Interfaces
{
    public interface ICategoryDAL
    {
        IEnumerable<ICategory> GetCategories();
    }
}
