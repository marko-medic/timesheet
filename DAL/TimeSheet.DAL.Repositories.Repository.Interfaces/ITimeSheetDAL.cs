﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeSheet.Shared.Models.Interfaces;

namespace TimeSheet.DAL.Repositories.Repository.Interfaces
{
    public interface ITimeSheetDAL
    {
        IEnumerable<IDay> GetTimeSheets(DateTime begin, DateTime end);
        IEnumerable<IDayRecord> GetTimeSheetDay(DateTime date);
        bool DeleteRecordsByDate(DateTime date);
        decimal GetTotalHours(DateTime begin, DateTime end);
        void AddTimeSheetDay(IDayRecord record);
    }
}
