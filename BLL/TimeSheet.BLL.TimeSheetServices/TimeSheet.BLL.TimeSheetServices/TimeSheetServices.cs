﻿
using TimeSheet.BLL.Service.Implementation;
using TimeSheet.BLL.Service.Interfaces;
using TimeSheet.DAL.Repositories.DbService.Implementation;
using TimeSheet.DAL.Repositories.DbService.Interfaces;
using TimeSheet.DAL.Repositories.Repository.Implementation;

namespace TimeSheet.Shared.TimeSheetServices
{
    public class TimeSheetServices
    {
        public string ConnectionString { get; set; } = "Connection";

        public ICountryService GetCountryService()
            => new CountryService(new CountryDAL(GetDbService()));

        public IClientService GetClientService()
           => new ClientService(new ClientDAL(GetDbService()));

        public ITeamMemberService GetTeamMemberService()
            => new TeamMemberService(new TeamMemberDAL(GetDbService()));

        public IProjectService GetProjectService()
            => new ProjectService(new ProjectDAL(GetDbService()));

        public ICategoryService GetCategoryService()
            => new CategoryService(new CategoryDAL(GetDbService()));

        public ITimeSheetService GetTimeSheetService()
               => new TimeSheetService(new TimeSheetDAL(GetDbService()));

        public IReportService GetReportService()
            => new ReportService(new ReportDAL(GetDbService()));

        private IDbService GetDbService()
            => new DBService(new DbConnectionService(ConnectionString));
    }
}