﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeSheet.BLL.Service.Interfaces;
using TimeSheet.DAL.Repositories.Repository.Interfaces;
using TimeSheet.Shared.Models.Implementation;
using TimeSheet.Shared.Models.Interfaces;

namespace TimeSheet.BLL.Service.Implementation
{
    public class TimeSheetService : ITimeSheetService
    {

        private ITimeSheetDAL _timeSheetDAL;

        public TimeSheetService(ITimeSheetDAL timeSheetDAL)
        {
            if (timeSheetDAL == null)
            {
                throw new ArgumentNullException("TimeSheet dal cannot be null", nameof(timeSheetDAL));
            }
            _timeSheetDAL = timeSheetDAL;
        }


        public IEnumerable<IDay> GetTimeSheets(DateTime begin, DateTime end)
        {
            return _timeSheetDAL.GetTimeSheets(begin, end);
        }

        public IEnumerable<IDayRecord> GetTimeSheetDay(DateTime date)
        {
            return _timeSheetDAL.GetTimeSheetDay(date);
        }

        public void AddTimeSheetDay(IDayRecord dayRecord)
        {
            Validate(dayRecord);
            _timeSheetDAL.AddTimeSheetDay(dayRecord);
        }

        public decimal GetTotalHours(DateTime begin, DateTime end)
        {
            return _timeSheetDAL.GetTotalHours(begin, end);
        }

        public decimal GetTotalHours(DateTime date)
        {
            return _timeSheetDAL.GetTotalHours(date, date);
        }

        public bool DeleteRecordsByDate(DateTime date)
        {
           return _timeSheetDAL.DeleteRecordsByDate(date);
        }

        public void UpdateTimeSheetDay(DateTime date, DayRecord[] dayRecords)
        {
            DeleteRecordsByDate(date);
            foreach (DayRecord dayRecord in dayRecords)
            {
                AddTimeSheetDay(dayRecord);
            }
        }


        private void Validate(IDayRecord dayRecord)
        {
            dayRecord.Validate();
        }
    }
}
