﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeSheet.BLL.Service.Interfaces;
using TimeSheet.DAL.Repositories.Repository.Interfaces;
using TimeSheet.Shared.Models.Interfaces;

namespace TimeSheet.BLL.Service.Implementation
{
    public class ReportService : IReportService
    {
        private IReportDAL _reportDAL;

        public ReportService(IReportDAL reportDAL)
        {
            if (reportDAL == null)
            {
                throw new ArgumentNullException("Report DAL cannot be null", nameof(reportDAL));
            }
            _reportDAL = reportDAL;
        }
        public IEnumerable<IReport> GetReports(IReportSearch reportSearch)
        {
            if (reportSearch == null)
            {
                return _reportDAL.GetAllReports();
            }
            else
            {
                return _reportDAL.GetFilteredReports(reportSearch);
            }
        }

    }
}
