﻿using System;
using System.Collections.Generic;
using TimeSheet.Shared.Models.Interfaces;
using TimeSheet.BLL.Service.Interfaces;
using TimeSheet.DAL.Repositories.Repository.Interfaces;

namespace TimeSheet.BLL.Service.Implementation
{
    public class CountryService : ICountryService
    {
        private ICountryDAL _countryDAL { get; }

        public CountryService(ICountryDAL countryDAL)
        {
            if (countryDAL == null)
            {
                throw new ArgumentNullException("Argument cannot be null", nameof(countryDAL));
            }
            _countryDAL = countryDAL;
        }

        public IEnumerable<ICountry> GetCountries()
            => _countryDAL.GetCountries();
    }
}
