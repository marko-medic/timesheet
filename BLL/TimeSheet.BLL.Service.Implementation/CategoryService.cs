﻿using System;
using System.Collections.Generic;
using TimeSheet.Shared.Models.Interfaces;
using TimeSheet.BLL.Service.Interfaces;
using TimeSheet.DAL.Repositories.Repository.Interfaces;

namespace TimeSheet.BLL.Service.Implementation
{
    public class CategoryService : ICategoryService
    {
        private ICategoryDAL _categoryDAL { get; }

        public CategoryService(ICategoryDAL categoryDAL)
        {
            if (categoryDAL == null)
            {
                throw new ArgumentNullException("Argument cannot be null", nameof(categoryDAL));
            }
            _categoryDAL = categoryDAL;
        }

        public IEnumerable<ICategory> GetCategories()
            => _categoryDAL.GetCategories();
    }
}
