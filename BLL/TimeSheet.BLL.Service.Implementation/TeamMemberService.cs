﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeSheet.BLL.Service.Interfaces;
using TimeSheet.DAL.Repositories.Repository.Interfaces;
using TimeSheet.Shared.Models.Interfaces;

namespace TimeSheet.BLL.Service.Implementation
{

    public class TeamMemberService : ITeamMemberService
    {

        private ITeamMemberDAL _teamMemberDAL;

        public TeamMemberService(ITeamMemberDAL teamMemberDAL)
        {
            if (teamMemberDAL == null)
            {
                throw new ArgumentNullException("Value cannot be null", nameof(teamMemberDAL));
            }
            _teamMemberDAL = teamMemberDAL;
        }

        public IEnumerable<ITeamMember> GetTeamMembers()
            => _teamMemberDAL.GetTeamMembers();

        public ITeamMember GetTeamMemberById(Guid id)
        {
            if (id == Guid.Empty)
            {
                throw new ArgumentException("Id cannot be empty guid", nameof(id));
            }
            return _teamMemberDAL.GetTeamMemberById(id);
        }

        public void AddTeamMember(ITeamMember newTeamMember)
        {
            Validate(newTeamMember);
            _teamMemberDAL.AddTeamMember(newTeamMember);
        }

        public bool RemoveTeamMemberById(Guid id)
            => _teamMemberDAL.RemoveTeamMemberById(id);

        public void UpdateTeamMemberById(ITeamMember teamMemberToEdit)
        {
            Validate(teamMemberToEdit);
            _teamMemberDAL.UpdateTeamMemberById(teamMemberToEdit);
        }

        private void Validate(ITeamMember teamMember)
        {
            teamMember.Validate();
        }
    }
}
