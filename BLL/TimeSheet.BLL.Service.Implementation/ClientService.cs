﻿using System;
using System.Collections.Generic;
using TimeSheet.DAL.Repositories.Repository.Interfaces;
using TimeSheet.BLL.Service.Interfaces;
using TimeSheet.Shared.Models.Interfaces;

namespace TimeSheet.BLL.Service.Implementation
{
    public class ClientService : IClientService
    {
        private IClientDAL _clientDAL { get; }

        public ClientService(IClientDAL clientDAL)
        {
            if (clientDAL == null)
            {
                throw new ArgumentNullException("Value cannot be null", nameof(clientDAL));
            }
            _clientDAL = clientDAL;
        }

        public IEnumerable<IClient> GetClients()
          => _clientDAL.GetClients();

        public IClient GetClientById(Guid id)
            => _clientDAL.GetClientById(id);
        

        public void UpdateClientById(IClient client)
        {
            Validate(client);
            _clientDAL.UpdateClientById(client);
        }

        public bool RemoveClientById(Guid id)
          => _clientDAL.RemoveClientById(id);


        public void AddClient(IClient newClient)
        {
            Validate(newClient);
            _clientDAL.AddClient(newClient);
        }

        public IClient GetClientByProjectId(Guid projectId)
            => _clientDAL.GetClientByProjectId(projectId);

        public IEnumerable<IClient> FilterClientsByName(string clientName)
        {
            if (clientName == null)
            {
                throw new ArgumentNullException("Value cannot be null", nameof(clientName));
            }
            return _clientDAL.FilterClientsByName(clientName);
        }

        public IEnumerable<IClient> GetClientsByPaging(int offset, int rowsCount)
            => _clientDAL.GetClientsByPaging(offset, rowsCount);

        public IEnumerable<IClient> FilterClientsByFirstLetter(char firstLetter)
         => _clientDAL.FilterClientsByFirstLetter(firstLetter);


        private void Validate(IClient client)
        {
            client.Validate();
        }

    }
}
