﻿using System;
using System.Collections.Generic;
using TimeSheet.DAL.Repositories.Repository.Interfaces;
using TimeSheet.BLL.Service.Interfaces;
using TimeSheet.Shared.Models.Interfaces;

namespace TimeSheet.BLL.Service.Implementation
{
    public class ProjectService : IProjectService
    {
        private IProjectDAL _projectDAL { get; }

        public ProjectService(IProjectDAL projectDAL)
        {
            if (projectDAL == null)
            {
                throw new ArgumentNullException("Value cannot be null", nameof(projectDAL));
            }
            _projectDAL = projectDAL;
        }

        public IEnumerable<IProject> GetProjects()
          => _projectDAL.GetProjects();

        public IProject GetProjectById(Guid id)
        {
            Validate(id);
            return _projectDAL.GetProjectById(id);
        }

        public void UpdateProjectById(IProject project)
        {
            Validate(project);
            _projectDAL.UpdateProjectById(project);
        }

        public bool RemoveProjectById(Guid id)
          => _projectDAL.RemoveProjectById(id);


        public void AddProject(IProject newProject)
        {
            Validate(newProject);
            _projectDAL.AddProject(newProject);
        }

        public IEnumerable<IProject> FilterProjectsByClient(Guid clientId)
        {
            Validate(clientId);
            return _projectDAL.FilterProjectByClient(clientId);
        }

        public IEnumerable<IProject> FilterProjectsByClientAndTeamMember(Guid clientId, Guid teamMemberId)
        {
            Validate(clientId);
            Validate(teamMemberId);
            return _projectDAL.FilterProjectByClientAndTeamMember(clientId, teamMemberId);
        }

        public IEnumerable<IProject> FilterProjectsByName(string projectName)
        {
            if (projectName == null)
            {
                throw new ArgumentNullException("Value cannot be null", nameof(projectName));
            }
            return _projectDAL.FilterProjectsByName(projectName);
        }

        public IEnumerable<IProject> FilterProjectsByFirstLetter(char firstLetter)
         => _projectDAL.FilterProjectsByFirstLetter(firstLetter);

        private void Validate(IProject project)
        {
            project.Validate();
        }

        private void Validate(Guid id)
        {
            if (id == null || id == Guid.Empty)
            {
                throw new ArgumentException("Invalid id", nameof(id));
            }
        }

    }
}
