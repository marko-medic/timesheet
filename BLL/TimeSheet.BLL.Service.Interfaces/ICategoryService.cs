﻿using System.Collections.Generic;
using TimeSheet.Shared.Models.Interfaces;

namespace TimeSheet.BLL.Service.Interfaces
{
    public interface ICategoryService
    {
        IEnumerable<ICategory> GetCategories();
    }
}
