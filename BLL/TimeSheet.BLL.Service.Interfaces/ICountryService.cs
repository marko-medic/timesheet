﻿using System.Collections.Generic;
using TimeSheet.Shared.Models.Interfaces;

namespace TimeSheet.BLL.Service.Interfaces
{
    public interface ICountryService
    {
        IEnumerable<ICountry> GetCountries();
    }
}
