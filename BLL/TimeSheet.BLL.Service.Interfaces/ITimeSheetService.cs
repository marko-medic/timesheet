﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeSheet.Shared.Models.Implementation;
using TimeSheet.Shared.Models.Interfaces;

namespace TimeSheet.BLL.Service.Interfaces
{
    public interface ITimeSheetService
    {
        IEnumerable<IDay> GetTimeSheets(DateTime begin, DateTime end);
        IEnumerable<IDayRecord> GetTimeSheetDay(DateTime date);
        bool DeleteRecordsByDate(DateTime date);
        decimal GetTotalHours(DateTime begin, DateTime end);
        decimal GetTotalHours(DateTime date);
        void AddTimeSheetDay(IDayRecord record);
        void UpdateTimeSheetDay(DateTime Date, DayRecord[] records);
    }
}
