﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeSheet.Shared.Models.Interfaces;

namespace TimeSheet.BLL.Service.Interfaces
{
    public interface ITeamMemberService
    {
        IEnumerable<ITeamMember> GetTeamMembers();
        ITeamMember GetTeamMemberById(Guid id);
        void UpdateTeamMemberById(ITeamMember teamMemberToEdit);
        bool RemoveTeamMemberById(Guid id);
        void AddTeamMember(ITeamMember newTeamMember);
    }
}
