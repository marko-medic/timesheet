﻿using System;
using System.Collections.Generic;
using TimeSheet.Shared.Models.Interfaces;

namespace TimeSheet.BLL.Service.Interfaces
{
    public interface IProjectService
    {
        IEnumerable<IProject> GetProjects();
        void UpdateProjectById(IProject projectToEdit);
        bool RemoveProjectById(Guid id);
        void AddProject(IProject newProject);
        IProject GetProjectById(Guid id);
        IEnumerable<IProject> FilterProjectsByClient(Guid clientId);
        IEnumerable<IProject> FilterProjectsByClientAndTeamMember(Guid clientId, Guid teamMemberId);
        IEnumerable<IProject> FilterProjectsByName(string projectName);
        IEnumerable<IProject> FilterProjectsByFirstLetter(char firstLetter);
    }
}