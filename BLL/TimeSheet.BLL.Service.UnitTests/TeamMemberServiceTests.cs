﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Linq;
using TimeSheet.BLL.Service.Implementation;
using TimeSheet.DAL.Repositories.Repository.Interfaces;
using TimeSheet.Shared.Models.Implementation;
using TimeSheet.Shared.Models.Interfaces;

[TestClass]
public class TeamMemberServiceTests
{
    ITeamMemberDAL _teamMemberDAL;
    List<ITeamMember> _teamMemberList;
    private static Guid _id = Guid.Parse("cb77cce6-c2cb-473b-bdd2-5dac8c93b755");

    [TestInitialize]
    public void TestInitialize()
    {
        _teamMemberDAL = Substitute.For<ITeamMemberDAL>();
        _teamMemberList = new List<ITeamMember>() { new TeamMember(_id, "Marko", "m", 23, "mail@mail.com", false, true), new TeamMember(Guid.NewGuid(), "ZXc", "z", 133, "mail@mail.com", false, true), new TeamMember(Guid.NewGuid(), "Neko", "m", 3, "mail@mail.com", false, true) };
    }

    [TestMethod]
    public void TeamMemberService_InitWithNotNullDAL_ReturnsTeamMemberServiceInstance()
    {
        // act
        TeamMemberService teamMemberService = new TeamMemberService(_teamMemberDAL);
        // assert
        Assert.IsTrue(teamMemberService != null);
    }

    [TestMethod]
    public void TeamMemberService_InitWithNullInsteadDAL_ThrowsArgumentNullException()
    {
        Assert.ThrowsException<ArgumentNullException>(() => new TeamMemberService(null));
    }

    [TestMethod]
    public void GetTeamMembers_GetListWithTeamMembers_ReturnsNonEmptyList()
    {
        // Arrange
        _teamMemberDAL.GetTeamMembers().Returns(_teamMemberList);
        TeamMemberService teamMemberService = new TeamMemberService(_teamMemberDAL);
        // Act
        IEnumerable<ITeamMember> teamMemberList = teamMemberService.GetTeamMembers();
        // Assert
        Assert.IsTrue(teamMemberList.Count() == _teamMemberList.Count());
    }


    [TestMethod]
    public void GetTeamMembers_GetListWithNoTeamMembers_ReturnsEmptyList()
    {
        // Arrange
        _teamMemberDAL.GetTeamMembers().Returns(new List<ITeamMember>() { });
        TeamMemberService teamMemberService = new TeamMemberService(_teamMemberDAL);
        // Act
        IEnumerable<ITeamMember> teamMemberList = teamMemberService.GetTeamMembers();
        // Assert
        Assert.IsTrue(teamMemberList.Count() == 0);
    }

    [TestMethod]
    public void AddTeamMember_AddNewTeamMember_ReturnsTrue()
    {
        // Arrange
        TeamMemberService teamMemberService = new TeamMemberService(_teamMemberDAL);
        ITeamMember teamMemberToAdd = new TeamMember(Guid.NewGuid(), "Marko", "m", 23, "mail@mail.com", true, false);
        // Act
        teamMemberService.AddTeamMember(teamMemberToAdd);
        // Assert
        _teamMemberDAL.Received(1).AddTeamMember(Arg.Is<ITeamMember>(tm => tm.Name == "Marko"));

    }

    [TestMethod]
    public void AddTeamMember_AddNullInsteadTeamMember_ThrowsArgumentNullException()
    {
        TeamMemberService teamMemberService = new TeamMemberService(_teamMemberDAL);
        Assert.ThrowsException<ArgumentNullException>(() => teamMemberService.AddTeamMember(null));
        _teamMemberDAL.DidNotReceive().AddTeamMember(Arg.Any<ITeamMember>());
    }

    [TestMethod]
    public void AddTeamMember_AddTeamMemberWithInvalidFields_ThrowsException()
    {
        TeamMemberService teamMemberService = new TeamMemberService(_teamMemberDAL);
        Assert.ThrowsException<ArgumentException>(() => teamMemberService.AddTeamMember(new TeamMember(Guid.NewGuid(), null, null, 23, "mail@mail.com", true, false)));
        _teamMemberDAL.DidNotReceive().AddTeamMember(Arg.Any<ITeamMember>());
    }

    [TestMethod]
    public void RemoveTeamMemberById_RemoveTeamMemberWithExistingId_ReturnsTrue()
    {
        // Arrange
        _teamMemberDAL.GetTeamMembers().Returns(_teamMemberList);
        TeamMemberService teamMemberService = new TeamMemberService(_teamMemberDAL);
        List<ITeamMember> teamMemberList = teamMemberService.GetTeamMembers().ToList();
        Guid teamMemberId = (Guid)teamMemberList[0].Id;
        _teamMemberDAL.RemoveTeamMemberById(teamMemberId).Returns(true);
        // Act
        bool teamMemberRemoved = teamMemberService.RemoveTeamMemberById(teamMemberId);
        // Assert
        Assert.IsTrue(teamMemberRemoved);
        _teamMemberDAL.Received(1).RemoveTeamMemberById(Arg.Is<Guid>(id => id == teamMemberId));
    }

    [TestMethod]
    public void RemoveTeamMemberById_PassTeamMemberWithNonExistingId_ReturnsTrue()
    {
        // Arrange
        _teamMemberDAL.GetTeamMembers().Returns(_teamMemberList);
        TeamMemberService teamMemberService = new TeamMemberService(_teamMemberDAL);
        Guid teamMemberId = Guid.NewGuid();
        _teamMemberDAL.RemoveTeamMemberById(teamMemberId).Returns(false);
        // Act
        bool isTeamMemberRemoved = teamMemberService.RemoveTeamMemberById(teamMemberId);
        // Assert

        Assert.IsFalse(isTeamMemberRemoved);
        _teamMemberDAL.Received(1).RemoveTeamMemberById(Arg.Is<Guid>(id => id == teamMemberId));
    }

    [TestMethod]
    public void GetTeamMemberById_PassValidGuid_ReturnsTeamMemberInstance()
    {
        // Arrange
        _teamMemberDAL.GetTeamMemberById(_id).Returns(_teamMemberList.First());
        // Act
        TeamMemberService teamMemberService = new TeamMemberService(_teamMemberDAL);
        // Assert
        Assert.IsTrue(teamMemberService.GetTeamMemberById(_id).Id == _id);
        _teamMemberDAL.Received(1).GetTeamMemberById(Arg.Is<Guid>(id => id == _id));
    }

    [TestMethod]
    public void GetTeamMemberById_PassEmptyGuid_ThrowsArgumentException()
    {
        // Arrange
        Guid emptyGuid = Guid.Empty;
        // Act
        TeamMemberService teamMemberService = new TeamMemberService(_teamMemberDAL);
        // Assert
        Assert.ThrowsException<ArgumentException>(() => teamMemberService.GetTeamMemberById(emptyGuid));
        _teamMemberDAL.DidNotReceive().GetTeamMemberById(Arg.Is<Guid>(id => id == emptyGuid));
    }


    [TestMethod]
    public void UpdateTeamMemberById_PassExistingTeamMember_ReturnsIsTeamMemberEdited()
    {
        // Arrange
        TeamMemberService teamMemberService = new TeamMemberService(_teamMemberDAL);
        ITeamMember newTeamMember = new TeamMember(_teamMemberList.First().Id, "Marko", "m", 23, "mail@mail.com", true, false);

        // Act
        teamMemberService.UpdateTeamMemberById(newTeamMember);
        // Assert
        _teamMemberDAL.Received(1).UpdateTeamMemberById(Arg.Is<ITeamMember>(teamMember => teamMember.Id == _teamMemberList.First().Id));
    }

    [TestMethod]
    public void UpdateTeamMemberById_PassNull_ThrowsArgumentNullException()
    {
        TeamMemberService teamMemberService = new TeamMemberService(_teamMemberDAL);
        Assert.ThrowsException<ArgumentNullException>(() => teamMemberService.UpdateTeamMemberById(null));
        _teamMemberDAL.DidNotReceive().UpdateTeamMemberById(Arg.Any<ITeamMember>());
    }

    [TestMethod]
    public void UpdateTeamMemberById_PassTeamMemberWithInvalidFields_ThrowsArgumentException()
    {
        TeamMemberService teamMemberService = new TeamMemberService(_teamMemberDAL);
        Assert.ThrowsException<ArgumentException>(() => teamMemberService.UpdateTeamMemberById(new TeamMember(Guid.NewGuid(), null, null, -5, null, true, false)));
        _teamMemberDAL.DidNotReceive().UpdateTeamMemberById(Arg.Any<ITeamMember>());
    }

}