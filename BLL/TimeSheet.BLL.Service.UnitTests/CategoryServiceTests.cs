﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using NSubstitute;
using TimeSheet.Shared.Models.Interfaces;
using TimeSheet.DAL.Repositories.Repository.Interfaces;
using TimeSheet.BLL.Service.Implementation;
using TimeSheet.Shared.Models.Implementation;

namespace TimeSheetLogic.UnitTests
{
    [TestClass]
    public class CategoryServiceTests
    {
        private ICategoryDAL _categoryDAL;
        private List<ICategory> _categoryList;

        [TestInitialize]
        public void TestInitialize()
        {
            _categoryDAL = Substitute.For<ICategoryDAL>();
            _categoryList = new List<ICategory>() { new Category(Guid.NewGuid(), "Japan"), new Category(Guid.NewGuid(), "France"), new Category(Guid.NewGuid(), "Spain") };
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException),
         "Argument cannot be null")]
        public void CategoryService_InitWithNullArg_ReturnsException()
        {
            new CategoryService(null);
        }

        [TestMethod]
        public void CategoryService_InitWithValidCategoryDAL_ReturnsCategoryServiceInstance()
        {
            // Arrange
            CategoryService categoryService = new CategoryService(_categoryDAL);
            // Assert
            Assert.IsTrue(categoryService != null);
        }

        [TestMethod]
        public void GetCategories_GetsCategoryList_ReturnsCorrectCategoryList()
        {
            // Arrange
            _categoryDAL.GetCategories().Returns(_categoryList);
            CategoryService categoryService = new CategoryService(_categoryDAL);

            // Act
            List<ICategory> categoryList = categoryService.GetCategories().ToList();

            // Assert
            Assert.IsTrue(categoryList.Count() == _categoryList.Count());
        }

        [TestMethod]
        public void GetCategories_GetsEmptyCategoryList_ReturnsCorrectCategoryList()
        {
            // Arrange
            _categoryDAL.GetCategories().Returns(new List<ICategory>() { });
            CategoryService categoryService = new CategoryService(_categoryDAL);
            // Act
            List<ICategory> categoryList = categoryService.GetCategories().ToList();
            // Assert
            Assert.IsTrue(categoryList.Count() == 0);
        }
    }
}
