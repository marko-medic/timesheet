﻿using System;
using NSubstitute;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using TimeSheet.Shared.Models.Implementation;
using TimeSheet.Shared.Models.Interfaces;
using TimeSheet.BLL.Service.Implementation;
using TimeSheet.DAL.Repositories.Repository.Interfaces;

namespace TimeSheetLogic.UnitTests
{
    [TestClass]
    public class ProjectServiceTests
    {
        IProjectDAL _projectDAL;
        List<IProject> _projectList;
        private static Guid _id = Guid.Parse("cb77cce6-c2cb-473b-bdd2-5dac8c93b755");
        private static Guid _clientId = Guid.Parse("cb77cce6-c2cb-473b-bdd8-5dac8c93b755");
        private static Guid _teamMemberId = Guid.Parse("cb77cce6-c2cb-473b-bdd9-5dac8c93b755");

        [TestInitialize]
        public void TestInitialize()
        {
            _projectDAL = Substitute.For<IProjectDAL>();
            _projectList = new List<IProject>() { new Project(_id, "ProjectX", _clientId, _teamMemberId, false, "Desc"), new Project(Guid.NewGuid(), "ProjectX", _clientId, _teamMemberId, false, "Desc"), new Project(Guid.NewGuid(), "ProjectX", _clientId, _teamMemberId, false, "Desc") };
        }

        [TestMethod]
        public void ProjectService_InitWithNotNullDAL_ReturnsProjectServiceInstance()
        {
            // act
            ProjectService projectService = new ProjectService(_projectDAL);
            // assert
            Assert.IsTrue(projectService != null);
        }

        [TestMethod]
        public void ProjectService_InitWithNullInsteadDAL_ThrowsArgumentNullException()
        {
            Assert.ThrowsException<ArgumentNullException>(() => new ProjectService(null));
        }

        [TestMethod]
        public void GetProjects_GetListWithProjects_ReturnsNonEmptyList()
        {
            // Arrange
            _projectDAL.GetProjects().Returns(_projectList);
            ProjectService projectService = new ProjectService(_projectDAL);
            // Act
            IEnumerable<IProject> projectList = projectService.GetProjects();
            // Assert
            Assert.IsTrue(projectList.Count() == _projectList.Count());
        }

        [TestMethod]
        public void GetProjects_GetListWithNoProjects_ReturnsEmptyList()
        {
            // Arrange
            _projectDAL.GetProjects().Returns(new List<IProject>() { });
            ProjectService projectService = new ProjectService(_projectDAL);
            // Act
            IEnumerable<IProject> projectList = projectService.GetProjects();
            // Assert
            Assert.IsTrue(projectList.Count() == 0);
        }

        [TestMethod]
        public void AddProject_AddNewProject_ReturnsTrue()
        {
            // Arrange
            ProjectService projectService = new ProjectService(_projectDAL);
            Project projectToAdd = new Project(Guid.NewGuid(), "ProjectX", _clientId, _teamMemberId, false, "Desc");
            // Act
            projectService.AddProject(projectToAdd);
            // Assert
            _projectDAL.Received(1).AddProject(Arg.Is<IProject>(project => project.Name == "ProjectX"));

        }

        [TestMethod]
        public void AddProject_AddNullInsteadProject_ThrowsArgumentNullException()
        {
            ProjectService projectService = new ProjectService(_projectDAL);
            Assert.ThrowsException<ArgumentNullException>(() => projectService.AddProject(null));
            _projectDAL.DidNotReceive().AddProject(Arg.Any<IProject>());
        }

        [TestMethod]
        public void AddProject_AddProjectWithInvalidFields_ThrowsException()
        {
            ProjectService projectService = new ProjectService(_projectDAL);
            Assert.ThrowsException<ArgumentException>(() => projectService.AddProject(new Project(Guid.NewGuid(), null, _clientId, _teamMemberId, false, "Desc")));
            _projectDAL.DidNotReceive().AddProject(Arg.Any<IProject>());
        }

        [TestMethod]
        public void RemoveProjectById_RemoveProjectWithExistingId_ReturnsTrue()
        {
            // Arrange
            _projectDAL.GetProjects().Returns(_projectList);
            ProjectService projectService = new ProjectService(_projectDAL);
            List<IProject> projectList = projectService.GetProjects().ToList();
            Guid projectId = (Guid)projectList[0].Id;
            _projectDAL.RemoveProjectById(projectId).Returns(true);
            // Act
            bool projectRemoved = projectService.RemoveProjectById(projectId);
            // Assert
            Assert.IsTrue(projectRemoved);
            _projectDAL.Received(1).RemoveProjectById(Arg.Is<Guid>(id => id == projectId));
        }

        [TestMethod]
        public void RemoveProjectById_PassProjectWithNonExistingId_ReturnsTrue()
        {
            // Arrange
            _projectDAL.GetProjects().Returns(_projectList);
            ProjectService projectService = new ProjectService(_projectDAL);
            Guid projectId = Guid.NewGuid();
            _projectDAL.RemoveProjectById(projectId).Returns(false);
            // Act
            bool isProjectRemoved = projectService.RemoveProjectById(projectId);
            // Assert

            Assert.IsFalse(isProjectRemoved);
            _projectDAL.Received(1).RemoveProjectById(Arg.Is<Guid>(id => id == projectId));
        }

        [TestMethod]
        public void GetProjectById_PassValidGuid_ReturnsProjectInstance()
        {
            // Arrange
            _projectDAL.GetProjectById(_id).Returns(_projectList.First());
            // Act
            ProjectService projectService = new ProjectService(_projectDAL);
            // Assert
            Assert.IsTrue(projectService.GetProjectById(_id).Id == _id);
            _projectDAL.Received(1).GetProjectById(Arg.Is<Guid>(projectId => projectId == _id));
        }

        [TestMethod]
        public void GetProjectById_PassEmptyGuid_ThrowsArgumentException()
        {
            // Arrange
            Guid emptyGuid = Guid.Empty;
            // Act
            ProjectService projectService = new ProjectService(_projectDAL);
            // Assert
            Assert.ThrowsException<ArgumentException>(() => projectService.GetProjectById(emptyGuid));
            _projectDAL.DidNotReceive().GetProjectById(Arg.Is<Guid>(id => id == emptyGuid));
        }

        [TestMethod]
        public void FilterProjectsByName_FilterProjectsWithExistingName_ReturnsFilteredListWithValue()
        {
            // Arrange
            string projectName = "Marko";
            _projectDAL.FilterProjectsByName(projectName).Returns(new List<IProject>() { new Project(_id, "Marko", _clientId, _teamMemberId, false, "Desc") });
            ProjectService projectService = new ProjectService(_projectDAL);

            // Act
            IEnumerable<IProject> filteredProjects = projectService.FilterProjectsByName(projectName);

            // Assert
            Assert.IsTrue(filteredProjects.Count() == 1);
            _projectDAL.Received(1).FilterProjectsByName(Arg.Is<string>(name => name == projectName));
        }

        [TestMethod]
        public void FilterProjectsByName_FilterProjectsWithNonExistingName_ReturnsEmptyProjectList()
        {
            // Arrange
            ProjectService projectService = new ProjectService(_projectDAL);
            string searchText = "NonExistingName";
            _projectDAL.FilterProjectsByName(searchText).Returns(new List<IProject>() { });

            // Act
            IEnumerable<IProject> filteredProjects = projectService.FilterProjectsByName(searchText);

            // Assert
            Assert.IsTrue(filteredProjects.Count() == 0);
            _projectDAL.Received(1).FilterProjectsByName(Arg.Is<string>(name => name == searchText));
        }

        [TestMethod]
        public void FilterProjectsByName_FilterProjectsWithNull_ThrowsArgumentNullException()
        {
            ProjectService projectService = new ProjectService(_projectDAL);
            string searchText = null;
            Assert.ThrowsException<ArgumentNullException>(() => projectService.FilterProjectsByName(searchText));
            _projectDAL.DidNotReceive().FilterProjectsByName(Arg.Any<string>());
        }

        [TestMethod]
        public void FilterProjectsByName_FilterProjectsWithEmptyString_ReturnsNonFilteredList()
        {
            string searchText = "";
            _projectDAL.FilterProjectsByName(searchText).Returns(_projectList);
            _projectDAL.GetProjects().Returns(_projectList);
            // Arrange
            ProjectService projectService = new ProjectService(_projectDAL);
            IEnumerable<IProject> projectList = projectService.GetProjects();

            // Act
            IEnumerable<IProject> filteredProjects = projectService.FilterProjectsByName(searchText);

            // Assert
            Assert.IsTrue(filteredProjects.Count() == projectList.Count());
            _projectDAL.Received(1).FilterProjectsByName(Arg.Is<string>(name => name == searchText));
        }

        [TestMethod]
        public void UpdateProjectById_PassExistingProject_ReturnsIsProjectEdited()
        {
            // Arrange
            string projectName = "Maxx";
            ProjectService projectService = new ProjectService(_projectDAL);
            IProject newProject = new Project(_projectList.First().Id, projectName, _clientId, _teamMemberId, true, "desc");

            // Act
            projectService.UpdateProjectById(newProject);
            // Assert
            _projectDAL.Received(1).UpdateProjectById(Arg.Is<IProject>(project => project.Name == projectName));
        }

        [TestMethod]
        public void UpdateProjectById_PassNull_ThrowsArgumentNullException()
        {
            ProjectService projectService = new ProjectService(_projectDAL);
            Assert.ThrowsException<ArgumentNullException>(() => projectService.UpdateProjectById(null));
            _projectDAL.DidNotReceive().UpdateProjectById(Arg.Any<IProject>());
        }

        [TestMethod]
        public void UpdateProjectById_PassProjectWithInvalidFields_ThrowsArgumentException()
        {
            ProjectService projectService = new ProjectService(_projectDAL);
            Assert.ThrowsException<ArgumentException>(() => projectService.UpdateProjectById(new Project(Guid.NewGuid(), null, _clientId, _teamMemberId, false, "Desc")));
            _projectDAL.DidNotReceive().UpdateProjectById(Arg.Any<IProject>());
        }

        [TestMethod]
        public void FilterProjectsByFirstLetter_FilterWithNonEmptyLetter_ReturnsCorrectList()
        {
            // Arrange
            _projectDAL.GetProjects().Returns(_projectList);
            ProjectService projectService = new ProjectService(_projectDAL);
            IEnumerable<IProject> projectList = projectService.GetProjects();
            char firstLetter = 'M';
            _projectDAL.FilterProjectsByFirstLetter(firstLetter).Returns(new List<IProject>() { new Project(_id, "MProjectX", _clientId, _teamMemberId, false, "Desc") });

            // Act
            IEnumerable<IProject> filteredProjectList = projectService.FilterProjectsByFirstLetter(firstLetter);

            // Assert
            Assert.AreNotEqual(projectList, filteredProjectList);
            _projectDAL.Received(1).FilterProjectsByFirstLetter(Arg.Is<char>(firstChar => firstChar == firstLetter));
        }

        [TestMethod]
        public void FilterProjectsByFirstLetter_FilterWithNonExistingLetterInList_ReturnsEmptyList()
        {
            // Arrange
            char firstLetter = ']';
            _projectDAL.FilterProjectsByFirstLetter(firstLetter).Returns(new List<IProject>() { });
            ProjectService projectService = new ProjectService(_projectDAL);

            // Act
            IEnumerable<IProject> filteredProjectList = projectService.FilterProjectsByFirstLetter(firstLetter);

            // Assert
            int projectListCount = filteredProjectList.Count();
            Assert.IsTrue(projectListCount == 0);
            _projectDAL.Received(1).FilterProjectsByFirstLetter(Arg.Is<char>(firstChar => firstChar == firstLetter));
        }
    }
}
