﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TimeSheet.WEB_API.Models.Reports
{
    public class ReportSearchModel
    {
        public Guid? TeamMemberId { get; set; }
        public Guid? ClientId { get; set; }
        public Guid? ProjectId { get; set; }
        public Guid? CategoryId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public ReportSearchModel() { }
    }
}