﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TimeSheet.WEB_API.Models
{
    /// <summary>
    ///  Represents one specific client
    /// </summary>
    public class ProjectModel
    {
        /// <summary>
        /// Project name
        /// </summary>
        [Required(ErrorMessage = "This field is required")]
        public string Name { get; set; }
        /// <summary>
        /// Project description
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Client id
        /// </summary>
        [Required(ErrorMessage = "This field is required")]
        public Guid ClientId { get; set; }
        /// <summary>
        /// Team leader id
        /// </summary>
        [Required(ErrorMessage = "This field is required")]
        public Guid TeamMemberId { get; set; }
        /// <summary>
        /// Is project active
        /// </summary>
        [Required(ErrorMessage = "This field is required")]
        public bool IsActive { get; set; }
        /// <summary>
        /// Client empty constructor
        /// </summary>
        public ProjectModel() { }

    }
}
