﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TimeSheet.WEB_API.Models.TimeSheet
{
    public class DayRecordModel
    {
        public Guid? Id { get; set; }
        [Required(ErrorMessage = "Day date is required")]
        public DateTime DayDate { get; set; }
        [Required(ErrorMessage = "Time is required")]
        public decimal Time { get; set; }
        [Required(ErrorMessage = "Client id is required")]
        public Guid ClientId { get; set; }
        [Required(ErrorMessage = "Project id is required")]
        public Guid ProjectId { get; set; }
        [Required(ErrorMessage = "Category id is required")]
        public Guid CategoryId { get; set; }
        public string Description { get; set; }
        public decimal? Overtime { get; set; }

        public DayRecordModel() { }
    }
}