﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TimeSheet.WEB_API.Models
{
    /// <summary>
    ///  Represents one specific client
    /// </summary>
    public class ClientModel
    {
        /// <summary>
        /// Client name
        /// </summary>
        [Required(ErrorMessage = "This field is required")]
        public string Name { get; set; }
        /// <summary>
        /// Client address
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// Client city
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// Client Zip or postal code
        /// </summary>
        public string ZipCode { get; set; }
        /// <summary>
        /// Country unique id
        /// </summary>
        public Guid? CountryId { get; set; }

        /// <summary>
        /// Client empty constructor
        /// </summary>
        public ClientModel() { }

    }
}
