﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TimeSheet.WEB_API.Models.Team_members
{
    public class TeamMemberModel
    {
        public Guid? Id { get; set; }
        [Required(ErrorMessage = "This field is required")]
        public string Name { get; set; }
        [Required(ErrorMessage = "This field is required")]
        public string Username { get; set; }
        [Required(ErrorMessage = "This field is required")]
        public int HoursPerWeek { get; set; }
        [Required(ErrorMessage = "This field is required")]
        public string Email { get; set; }
        [Required(ErrorMessage = "This field is required")]
        public bool IsActive { get; set; }
        [Required(ErrorMessage = "This field is required")]
        public bool IsAdmin { get; set; }

        public TeamMemberModel() { }
    }
}
