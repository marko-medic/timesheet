using System.Web.Http;
using SimpleInjector;
using SimpleInjector.Integration.WebApi;
using TimeSheet.Shared.Configurations;
using TimeSheet.Shared.TimeSheetServices;
using TimeSheet.WEB_API.Properties;

namespace MVCSimpleInjectorDemo.App_Start
{
    public class SimpleInjectorConfig
    {
        private decimal _expectedWorkingHours;
        public SimpleInjectorConfig()
        {
            _expectedWorkingHours = decimal.Parse(Settings.Default.ExpectedWorkingHours.ToString());
        }
        public void RegisterComponents()
        {
            TimeSheetServices timeSheetServices = new TimeSheetServices();
            Container container = new Container();
            container.Register(() => timeSheetServices.GetCountryService());
            container.Register(() => timeSheetServices.GetClientService());
            container.Register(() => timeSheetServices.GetTeamMemberService());
            container.Register(() => timeSheetServices.GetProjectService());
            container.Register(() => timeSheetServices.GetCategoryService());
            container.Register(() => timeSheetServices.GetTimeSheetService());
            container.Register(() => timeSheetServices.GetReportService());
            container.Register(() => new ConfigurationOptions(_expectedWorkingHours));
            container.RegisterWebApiControllers(GlobalConfiguration.Configuration);
            container.Verify();
            GlobalConfiguration.Configuration.DependencyResolver = new SimpleInjectorWebApiDependencyResolver(container);
        }
    }
}