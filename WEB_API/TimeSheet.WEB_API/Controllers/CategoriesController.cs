﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TimeSheet.BLL.Service.Interfaces;
using TimeSheet.Shared.Models.Interfaces;

namespace TimeSheet.WEB_API.Controllers
{

    [RoutePrefix("api/Categories")]
    public class CategoriesController : ApiController
    {
        private readonly ICategoryService _categoryService;


        public CategoriesController(ICategoryService categoryService)
        {
            if (categoryService == null)
            {
                throw new ArgumentNullException("Category service cannot be null", nameof(categoryService));
            }
            _categoryService = categoryService;
        }

        [Route("")]
        [HttpGet]
        public IHttpActionResult GetCategories()
        {
            IEnumerable<ICategory> categoryList = _categoryService.GetCategories();
            return Ok(categoryList);
        }
    }
}
