﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TimeSheet.BLL.Service.Interfaces;
using TimeSheet.Shared.Models.Implementation;
using TimeSheet.Shared.Models.Interfaces;
using TimeSheet.WEB_API.Models.Reports;

namespace TimeSheet.WEB_API.Controllers
{
    [RoutePrefix("api/Reports")]
    public class ReportsController : ApiController
    {
        private IReportService _reportService;

        public ReportsController(IReportService reportService)
        {
            if (reportService == null)
            {
                throw new ArgumentNullException("Report service cannot be null", nameof(reportService));
            }
            _reportService = reportService;
        }
        // GET: api/Reports
        [Route("")]
        [HttpGet]
        public IHttpActionResult Get([FromUri]ReportSearchModel reportSearchModel)
        {
            IReportSearch mappedSearchReport = reportSearchModel == null ? null : MapReportSearch(reportSearchModel);
            IEnumerable<IReport> reportList = _reportService.GetReports(mappedSearchReport);
            return Ok(reportList);
        }

        private IReportSearch MapReportSearch(ReportSearchModel report)
            => new ReportSearch(report.TeamMemberId, report.ClientId, report.ProjectId, report.CategoryId, report.StartDate, report.EndDate);
    }
}
