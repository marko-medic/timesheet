﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TimeSheet.BLL.Service.Interfaces;
using TimeSheet.Shared.Models.Implementation;
using TimeSheet.Shared.Models.Interfaces;
using TimeSheet.WEB_API.Models;

namespace TimeSheet.WEB_API.Controllers
{
    /// <summary>
    /// Controller for clients page
    /// </summary>
    [RoutePrefix("api/Clients")]
    public class ClientsController : ApiController
    {
        private readonly IClientService _clientService;
        /// <summary>
        /// ClientController constructor for client page
        /// </summary>
        public ClientsController(IClientService clientService)
        {
            if (clientService == null)
            {
                throw new ArgumentNullException("Client service cannot be null", nameof(clientService));
            }
            _clientService = clientService;
        }

        // GET: api/Clients
        /// <summary>
        /// Gets a list of all clients from database
        /// </summary>
        /// <returns>List of all clients</returns>

        [Route("")]
        [HttpGet]
        public IHttpActionResult Get()
        {
            IEnumerable<IClient> clientList = _clientService.GetClients();
            return Ok(clientList);
        }

        // GET: api/Clients
        /// <summary>
        /// Gets a list of clients by paging from database
        /// </summary>
        /// <param name="offset">Offset from row</param>
        /// <param name="rowCount">How menu rows should be returned</param>
        /// <returns>List of clients by paging</returns>
        [Route("")]
        [HttpGet]
        public IHttpActionResult Get(int offset, int rowCount)
        {
            IEnumerable<IClient> clientList = _clientService.GetClientsByPaging(offset, rowCount);
            return Ok(clientList);
        }

        // GET: api/Clients
        /// <summary>
        /// Gets a list of clients filtered by first name
        /// </summary>
        /// <param name="searchTerm">Search term for filtering clients by name</param>
        /// <returns>List of clients filtered by first name</returns>
        [Route("")]
        [HttpGet]
        public IHttpActionResult Get(string searchTerm)
        {
            if (String.IsNullOrEmpty(searchTerm))
            {
                searchTerm = "";
            }
            IEnumerable<IClient> clientList = _clientService.FilterClientsByName(searchTerm);
            return Ok(clientList);
        }

        // GET: api/Clients
        /// <summary>
        /// Gets a list of clients filtered by first letter
        /// </summary>
        /// <param name="firstLetter">Filtering first letter for client name</param>
        /// <returns>List of clients filtered by first letter</returns>
        [Route("")]
        [HttpGet]
        public IHttpActionResult Get(char? firstLetter)
        {
            if (firstLetter == null)
            {
                firstLetter = '\0';
            }
            IEnumerable<IClient> clientList = _clientService.FilterClientsByFirstLetter((char)firstLetter);
            return Ok(clientList);
        }

        // GET: api/Clients/5
        /// <summary>
        /// Gets a client by provided Id
        /// </summary>
        /// <param name="id">The unique identifier for client</param>
        /// <returns>Client by provided id if found</returns>
        [Route("{id:Guid}", Name = "GetClientById")]
        [HttpGet]
        public IHttpActionResult GetClientById(Guid id)
        {
            IClient client = null;
            try
            {
                client = _clientService.GetClientById(id);
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }
            return Ok(client);
        }
        // Get: api/Clients/get-by-project/1
        /// <summary>
        /// Create new client
        /// </summary>
        [Route("filter-by-project/{ProjectId:Guid}")]
        [HttpGet]
        public IHttpActionResult GetClientByProjectId(Guid projectId)
        {
            IClient client = null;
            try
            {
                client = _clientService.GetClientByProjectId(projectId);
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }
            return Ok(client);
        }

        // POST: api/Clients
        /// <summary>
        /// Create new client
        /// </summary>
        [Route("")]
        [HttpPost]
        public IHttpActionResult Post([FromBody]ClientModel clientModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            IClient client = MapClient(clientModel);
            _clientService.AddClient(client);
            return CreatedAtRoute("GetClientById", new { id = client.Id }, client);
        }

        // PUT: api/Clients/Id
        /// <summary>
        /// Update client by id
        /// </summary>
        /// <param name="clientModel">Client object</param>
        /// <param name="id">Client Id</param>
        /// <returns>Updated customer</returns>
        [Route("{id:Guid}")]
        [HttpPut]
        public IHttpActionResult Put(Guid id, [FromBody]ClientModel clientModel)
        {
            if (id == Guid.Empty)
            {
                return BadRequest(id.ToString());
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IClient client = MapClient(clientModel, id);
            _clientService.UpdateClientById(client);
            return Ok(client);
        }

        // DELETE: api/Clients/5
        /// <summary>
        /// Delete client by id
        /// </summary>
        /// <param name="id">Id of customer to delete</param>
        [Route("{id:Guid}")]
        [HttpDelete]
        public IHttpActionResult Delete(Guid id)
        {
            IClient clientToDelete = _clientService.GetClientById(id);

            bool isClientDeleted = _clientService.RemoveClientById(id);
            if (!isClientDeleted)
            {
                return NotFound();
            }
            return Ok(clientToDelete);
        }

        private Client MapClient(ClientModel clientToMap, Guid? id = null)
                => new Client(id, clientToMap.Name, clientToMap.Address, clientToMap.City, clientToMap.ZipCode, clientToMap.CountryId);
    }
}
