﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TimeSheet.BLL.Service.Interfaces;
using TimeSheet.Shared.Models.Interfaces;

namespace TimeSheet.WEB_API.Controllers
{
    /// <summary>
    /// Controller for countries
    /// </summary>
    /// 
    [RoutePrefix("api/Countries")]
    public class CountriesController : ApiController
    {
        private readonly ICountryService _countryService;

        /// <summary>
        /// Constructor for countries controller
        /// Initialize country service
        /// </summary>
        public CountriesController(ICountryService countryService)
        {
            if (countryService == null)
            {
                throw new ArgumentNullException("Country service cannot be null", nameof(countryService));
            }
            _countryService = countryService;
        }

        // GET: api/Countries
        /// <summary>
        /// Gets a list of all countries from database
        /// </summary>
        /// <returns>List of all countries</returns>
        [Route("")]
        [HttpGet]
        public IHttpActionResult GetCountries()
        {
            IEnumerable<ICountry> countryList = _countryService.GetCountries();
            return Ok(countryList);
        }
    }
}
