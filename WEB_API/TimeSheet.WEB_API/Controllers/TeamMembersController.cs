﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TimeSheet.BLL.Service.Interfaces;
using TimeSheet.Shared.Models.Implementation;
using TimeSheet.Shared.Models.Interfaces;
using TimeSheet.WEB_API.Models.Team_members;

namespace TimeSheet.WEB_API.Controllers
{
    [RoutePrefix("api/TeamMembers")]
    public class TeamMembersController : ApiController
    {
        private readonly ITeamMemberService _teamMemberService;

        public TeamMembersController(ITeamMemberService teamMemberService)
        {
            if (teamMemberService == null)
            {
                throw new ArgumentNullException("Team member service cannot be null", nameof(teamMemberService));
            }
            _teamMemberService = teamMemberService;
        }
        // GET: api/TeamMembers
        [Route("")]
        [HttpGet]
        public IHttpActionResult Get()
        {
            IEnumerable<ITeamMember> teamMemberList = _teamMemberService.GetTeamMembers();
            return Ok(teamMemberList);
        }

        // GET: api/TeamMembers/5
        [Route("{id:Guid}", Name = "GetTeamMemberById")]
        [HttpGet]
        public IHttpActionResult Get(Guid id)
        {
            ITeamMember teamMember = null;
            try
            {
                teamMember = _teamMemberService.GetTeamMemberById(id);
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }
            return Ok(teamMember);
        }

        // POST: api/TeamMembers
        [Route("")]
        [HttpPost]
        public IHttpActionResult Post([FromBody]TeamMemberModel teamMemberModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ITeamMember teamMember = MapTeamMember(teamMemberModel);
            _teamMemberService.AddTeamMember(teamMember);
            return CreatedAtRoute("GetTeamMemberById", new { Id = teamMember.Id }, teamMember);
        }

        // PUT: api/TeamMembers/5
        [Route("{id:Guid}")]
        [HttpPut]
        public IHttpActionResult Put(Guid id, [FromBody]TeamMemberModel teamMemberModel)
        {
            if (id == Guid.Empty)
            {
                return BadRequest(id.ToString());
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            ITeamMember teamMember = MapTeamMember(teamMemberModel, id);
            _teamMemberService.UpdateTeamMemberById(teamMember);
            return Ok(teamMember);
        }

        // DELETE: api/TeamMembers/5
        [Route("{id:Guid}")]
        [HttpDelete]
        public IHttpActionResult Delete(Guid id)
        {
            ITeamMember teamMemberToDelete = _teamMemberService.GetTeamMemberById(id);
            bool isTeamMemberDeleted = _teamMemberService.RemoveTeamMemberById(id);
            if (!isTeamMemberDeleted)
            {
                return NotFound();
            }
            return Ok(teamMemberToDelete);
        }

        private TeamMember MapTeamMember(TeamMemberModel tmToMap, Guid? id = null)
            => new TeamMember(id, tmToMap.Name, tmToMap.Username, tmToMap.HoursPerWeek, tmToMap.Email, tmToMap.IsActive, tmToMap.IsAdmin);

    }
}
