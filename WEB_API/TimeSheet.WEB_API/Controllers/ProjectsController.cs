﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TimeSheet.BLL.Service.Interfaces;
using TimeSheet.Shared.Models.Implementation;
using TimeSheet.Shared.Models.Interfaces;
using TimeSheet.WEB_API.Models;

namespace TimeSheet.WEB_API.Controllers
{
    /// <summary>
    /// Controller for projects page
    /// </summary>
    [RoutePrefix("api/Projects")]
    public class ProjectsController : ApiController
    {
        private readonly IProjectService _projectService;
        /// <summary>
        /// ProjectController constructor for project page
        /// </summary>
        public ProjectsController(ICountryService countryService, IProjectService projectService)
        {
            if (projectService == null)
            {
                throw new ArgumentNullException("Project service cannot be null", nameof(projectService));
            }
            _projectService = projectService;
        }

        // GET: api/Projects
        /// <summary>
        /// Gets a list of all projects from database
        /// </summary>
        /// <returns>List of all projects</returns>

        [Route("")]
        [HttpGet]
        public IHttpActionResult Get()
        {
            IEnumerable<IProject> projectList = _projectService.GetProjects();
            return Ok(projectList);
        }

        // GET: api/Projects
        /// <summary>
        /// Gets a list of projects filtered by first name
        /// </summary>
        /// <param name="searchTerm">Search term for filtering projects by name</param>
        /// <returns>List of projects filtered by first name</returns>
        [Route("")]
        [HttpGet]
        public IHttpActionResult Get(string searchTerm)
        {
            if (String.IsNullOrEmpty(searchTerm))
            {
                searchTerm = "";
            }
            IEnumerable<IProject> projectList = _projectService.FilterProjectsByName(searchTerm);
            return Ok(projectList);
        }

        // GET: api/Projects
        /// <summary>
        /// Gets a list of projects filtered by first letter
        /// </summary>
        /// <param name="firstLetter">Filtering first letter for project name</param>
        /// <returns>List of projects filtered by first letter</returns>
        [Route("")]
        [HttpGet]
        public IHttpActionResult Get(char? firstLetter)
        {
            if (firstLetter == null)
            {
                firstLetter = '\0';
            }
            IEnumerable<IProject> projectList = _projectService.FilterProjectsByFirstLetter((char)firstLetter);
            return Ok(projectList);
        }

        // GET: api/Projects/5
        /// <summary>
        /// Gets a project by provided Id
        /// </summary>
        /// <param name="id">The unique identifier for project</param>
        /// <returns>Project by provided id if found</returns>
        [Route("{id:Guid}", Name = "GetProjectById")]
        [HttpGet]
        public IHttpActionResult GetProjectById(Guid id)
        {
            IProject project = null;
            try
            {
                project = _projectService.GetProjectById(id);
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }
            return Ok(project);
        }

        [Route("filter-by-client/{id:Guid}")]
        [HttpGet]
        public IHttpActionResult Get(Guid id)
        {
            IEnumerable<IProject> clientProjectList = _projectService.FilterProjectsByClient(id);
            return Ok(clientProjectList);
        }

        [Route("filter-by-clientAndTeamMember/{clientId:Guid}/{teamMemberId:Guid}")]
        [HttpGet]
        public IHttpActionResult Get(Guid clientId, Guid teamMemberId)
        {
            IEnumerable<IProject> projectList = _projectService.FilterProjectsByClientAndTeamMember(clientId, teamMemberId);
            return Ok(projectList);
        }

        // POST: api/Projects
        /// <summary>
        /// Create new project
        /// </summary>
        [Route("")]
        [HttpPost]
        public IHttpActionResult Post([FromBody]ProjectModel projectModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            IProject project = MapProject(projectModel);
            _projectService.AddProject(project);
            return CreatedAtRoute("GetProjectById", new { Id = project.Id }, project);
        }

        // PUT: api/Projects/Id
        /// <summary>
        /// Update project by id
        /// </summary>
        /// <param name="projectModel">Project object</param>
        /// <param name="id">Project Id</param>
        /// <returns>Updated customer</returns>
        [Route("{id:Guid}")]
        [HttpPut]
        public IHttpActionResult Put(Guid id, [FromBody]ProjectModel projectModel)
        {
            if (id == Guid.Empty)
            {
                return BadRequest(id.ToString());
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IProject project = MapProject(projectModel, id);
            _projectService.UpdateProjectById(project);
            return Ok(project);
        }

        // DELETE: api/Projects/5
        /// <summary>
        /// Delete project by id
        /// </summary>
        /// <param name="id">Id of customer to delete</param>
        [Route("{id:Guid}")]
        [HttpDelete]
        public IHttpActionResult Delete(Guid id)
        {
            IProject projectToDelete = _projectService.GetProjectById(id);

            bool isProjectDeleted = _projectService.RemoveProjectById(id);
            if (!isProjectDeleted)
            {
                return NotFound();
            }
            return Ok(projectToDelete);
        }

        private Project MapProject(ProjectModel projectToMap, Guid? id = null)
                => new Project(id, projectToMap.Name, projectToMap.ClientId, projectToMap.TeamMemberId, projectToMap.IsActive, projectToMap.Description);
    }
}
