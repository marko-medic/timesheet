﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TimeSheet.BLL.Service.Interfaces;
using TimeSheet.Shared.Configurations;
using TimeSheet.Shared.Configurations.Interfaces;
using TimeSheet.Shared.Models.Implementation;
using TimeSheet.Shared.Models.Interfaces;
using TimeSheet.WEB_API.Models;
using TimeSheet.WEB_API.Models.TimeSheet;

namespace TimeSheet.WEB_API.Controllers
{
    [RoutePrefix("api/TimeSheet")]
    public class TimeSheetController : ApiController
    {
        private ITimeSheetService _timeSheetService;
        private IConfigurationOptions _configOptions;

        public TimeSheetController(ITimeSheetService timeSheetService, ConfigurationOptions configOptions)
        {
            if (timeSheetService == null)
            {
                throw new ArgumentNullException("TimeSheetService cannot be null", nameof(timeSheetService));
            }

            if (configOptions == null)
            {
                throw new ArgumentNullException("Config options cannot be null", nameof(configOptions));
            }

            _timeSheetService = timeSheetService;
            _configOptions = configOptions;
        }

        // GET: api/TimeSheet
        [Route("")]
        [HttpGet]
        public IHttpActionResult Get(DateTime begin, DateTime end)
        {
            IEnumerable<IDay> recordList = _timeSheetService.GetTimeSheets(begin, end);
            recordList = recordList.Select(record =>
            {
                record.TotalHours = _timeSheetService.GetTotalHours(record.Date);
                return record;
            });
            return Ok(new { list = recordList, minExpectedHours = _configOptions.MinExpectedWorkingHours });
        }

        [Route("{Date:DateTime}")]
        [HttpGet]
        public IHttpActionResult Get(DateTime date)
        {
            IEnumerable<IDayRecord> dayList = _timeSheetService.GetTimeSheetDay(date);
            return Ok(dayList);
        }

        [Route("TotalHours")]
        [HttpGet]
        public IHttpActionResult GetTotalHours(DateTime begin, DateTime end)
        {
            decimal totalHours = _timeSheetService.GetTotalHours(begin, end);
            return Ok(totalHours);
        }

        [Route("TotalHours")]
        [HttpGet]
        public IHttpActionResult GetTotalHours(DateTime date)
        {
            decimal totalHours = _timeSheetService.GetTotalHours(date);
            return Ok(totalHours);
        }

        // PUT: api/TimeSheet/5
        [Route("{date:DateTime}")]
        [HttpPut]
        public IHttpActionResult Put(DateTime date, [FromBody]DayRecord[] dayRecords)
        {
            try
            {
                _timeSheetService.UpdateTimeSheetDay(date, dayRecords);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Ok(dayRecords);
        }
   
    }
}
