﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeSheet.Shared.Models.Interfaces
{
    public interface IDay
    {
        DateTime Date { get; set; }
        decimal TotalHours { get; set; }
    }
}
