﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeSheet.Shared.Models.Interfaces
{
    public interface IReport
    {
        DateTime Date { get; set; }
        string TeamMemberName { get; set; }
        string ProjectName { get; set; }
        string CategoryName { get; set; }
        string Description { get; set; }
        decimal Time { get; set; }
    }
}
