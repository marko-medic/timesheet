﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeSheet.Shared.Models.Interfaces
{
    public interface IReportSearch
    {
        Guid? TeamMemberId { get; set; }
        Guid? ClientId { get; set; }
        Guid? ProjectId { get; set; }
        Guid? CategoryId { get; set; }
        DateTime? StartDate { get; set; }
        DateTime? EndDate { get; set; }
    }
}
