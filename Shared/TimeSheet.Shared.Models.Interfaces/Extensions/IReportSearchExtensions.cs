﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeSheet.Shared.Models.Interfaces
{
    public static class IReportSearchExtensions
    {
        public static void Validate(this IReportSearch reportSearch)
        {
            if (reportSearch == null)
            {
                throw new ArgumentNullException("Report search cannot be null", nameof(reportSearch));
            }
        }
    }
}
