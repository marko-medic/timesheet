﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeSheet.Shared.Models.Interfaces
{
    public static class IReportExtensions
    {
        public static void Validate(this IReport report)
        {
            if (report == null)
            {
                throw new ArgumentNullException("Report cannot be null", nameof(report));
            }
            if (report.Date < DateTime.MinValue || report.Date > DateTime.MaxValue)
            {
                throw new ArgumentException("Invalid date");
            }
            if (report.Time < 0)
            {
                throw new ArgumentException("Report time cannot be less than zero", nameof(report.Time));
            }
        }
    }
}
