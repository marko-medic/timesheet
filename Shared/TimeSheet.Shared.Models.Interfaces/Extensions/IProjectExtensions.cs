﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeSheet.Shared.Models.Interfaces
{
    public static class IProjectExtensions
    {
        public static void Validate(this IProject project)
        {
            if (project == null)
            {
                throw new ArgumentNullException("Value cannot be null", nameof(project));
            }
            if (String.IsNullOrEmpty(project.Name))
            {
                throw new ArgumentException("Project name cannot be null or empty string", nameof(project.Name));
            }

            if (project.ClientId == Guid.Empty)
            {
                throw new ArgumentException("ClientId cannot be empty guid", nameof(project.ClientId));
            }

            if (project.TeamMemberId == Guid.Empty)
            {
                throw new ArgumentException("TeamMemberId cannot be empty guid", nameof(project.TeamMemberId));
            }
        }
    }
}
