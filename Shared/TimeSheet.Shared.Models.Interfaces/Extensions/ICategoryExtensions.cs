﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeSheet.Shared.Models.Interfaces
{
    public static class ICategoryExtensions
    {
        public static void Validate(this ICategory category)
        {
            if (category == null)
            {
                throw new ArgumentNullException("Category cannot be null", nameof(category));
            }

            if (String.IsNullOrEmpty(category.Name))
            {
                throw new ArgumentException("Category name cannot be null or empty string", nameof(category.Name));
            }
        }
    }
}
