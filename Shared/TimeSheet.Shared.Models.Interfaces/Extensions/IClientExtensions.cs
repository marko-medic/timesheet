﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeSheet.Shared.Models.Interfaces
{
    public static class IClientExtensions
    {
        public static void Validate(this IClient client)
        {
            if (client == null)
            {
                throw new ArgumentNullException("Value cannot be null", nameof(client));
            }

            if (String.IsNullOrEmpty(client.Name))
            {
                throw new ArgumentException("Name cannot be null or empty string", nameof(client.Name));
            }
        }
    }
}
