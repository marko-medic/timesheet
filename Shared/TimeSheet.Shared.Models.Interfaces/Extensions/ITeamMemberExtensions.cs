﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeSheet.Shared.Models.Interfaces
{
    public static class ITeamMemberExtensions
    {
        public static void Validate(this ITeamMember teamMember)
        {
            if (teamMember == null)
            {
                throw new ArgumentNullException("Value cannot be null", nameof(teamMember));
            }

            if (String.IsNullOrEmpty(teamMember.Name))
            {
                throw new ArgumentException("Team member name cannot be null or empty string", nameof(teamMember.Name));
            }

            if (String.IsNullOrEmpty(teamMember.Username))
            {
                throw new ArgumentException("Username cannot be null or empty string", nameof(teamMember.Username));
            }

            if (teamMember.HoursPerWeek < 0)
            {
                throw new ArgumentException("Hours per week cannot be less than zero", nameof(teamMember.HoursPerWeek));
            }

            if (String.IsNullOrEmpty(teamMember.Email))
            {
                throw new ArgumentException("Email cannot be null or empty string", nameof(teamMember.Email));
            }
        }
    }
}
