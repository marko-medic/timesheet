﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeSheet.Shared.Models.Interfaces
{
    public static class ICountryExtensions
    {
        public static void Validate(this ICountry country)
        {
            if (country == null)
            {
                throw new ArgumentNullException("Value cannot be null", nameof(country));
            }
            if (String.IsNullOrEmpty(country.Name))
            {
                throw new ArgumentException("Country name cannot be null or empty string", nameof(country.Name));
            }
        }
    }
}
