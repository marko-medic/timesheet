﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeSheet.Shared.Models.Interfaces
{
    public static class IDayRecordExtensions
    {
        public static void Validate(this IDayRecord dayRecord)
        {
            if (dayRecord == null)
            {
                throw new ArgumentNullException("Day record cannot be null", nameof(dayRecord));
            }

            if (dayRecord.DayDate <= DateTime.MinValue || dayRecord.DayDate >= DateTime.MaxValue)
            {
                throw new ArgumentException("Day date is not valid", nameof(dayRecord.DayDate));
            }

            if (dayRecord.ProjectId == null)
            {
                throw new ArgumentNullException("Project id cannot be null", nameof(dayRecord.ProjectId));
            }

            if (dayRecord.CategoryId == null)
            {
                throw new ArgumentNullException("Category id cannot be null", nameof(dayRecord.CategoryId));
            }

            if (dayRecord.Time < 0)
            {
                throw new ArgumentException("Time cannot be less than zero", nameof(dayRecord.Time));
            }
        }
    }
}
