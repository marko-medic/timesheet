﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeSheet.Shared.Models.Interfaces
{
    public static class IDayExtensions
    {
        public static void Validate(this IDay day)
        {
            if (day == null)
            {
                throw new ArgumentNullException("Day cannot be null", nameof(day));
            }

            if (day.Date <= DateTime.MinValue || day.Date >= DateTime.MaxValue)
            {
                throw new ArgumentException("Invalid date", nameof(day.Date));
            }
            if (day.TotalHours < 0)
            {
                throw new ArgumentException("Total hours cannot be less than 0", nameof(day.TotalHours));
            }
        }
    }
}
