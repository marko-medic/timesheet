﻿using System;

namespace TimeSheet.Shared.Models.Interfaces
{
    public interface ITeamMember
    {
        Guid? Id { get; set; }
        string Name { get; set; }
        string Username { get; set; }
        decimal HoursPerWeek { get; set; }
        string Email { get; set; }
        bool IsActive { get; set; }
        bool IsAdmin { get; set; }
    }
}
