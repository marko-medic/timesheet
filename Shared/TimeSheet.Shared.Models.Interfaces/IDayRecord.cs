﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeSheet.Shared.Models.Interfaces
{
    public interface IDayRecord
    {
        Guid? Id { get; set; }
        DateTime DayDate { get; set; }
        Guid ProjectId { get; set; }
        Guid CategoryId { get; set; }
        decimal Time { get; set; }
        string Description { get; set; }
        decimal? Overtime { get; set; }
    }
}
