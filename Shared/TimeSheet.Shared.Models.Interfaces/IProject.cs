﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeSheet.Shared.Models.Interfaces
{
    public interface IProject
    {
        Guid? Id { get; set; }
        string Name { get; set; }
        string Description { get; set; }
        Guid ClientId { get; set; }
        Guid TeamMemberId { get; set; }
        bool IsActive { get; set; }
    }
}
