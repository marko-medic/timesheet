﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeSheet.Shared.Configurations.Implementation.Properties;
using TimeSheet.Shared.Configurations.Interfaces;

namespace TimeSheet.Shared.Configurations
{
    public class ConfigurationOptions : IConfigurationOptions
    {
        public decimal MinExpectedWorkingHours { get; set; }
        public ConfigurationOptions(decimal minExpectedWorkingHours)
        {
            MinExpectedWorkingHours = minExpectedWorkingHours;

        }
    }
}
