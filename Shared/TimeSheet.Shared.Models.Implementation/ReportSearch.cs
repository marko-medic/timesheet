﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlTypes;
using TimeSheet.Shared.Models.Interfaces;

namespace TimeSheet.Shared.Models.Implementation
{
    public class ReportSearch : IReportSearch
    {
        public Guid? TeamMemberId { get; set; }
        public Guid? ClientId { get; set; }
        public Guid? ProjectId { get; set; }
        public Guid? CategoryId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public ReportSearch(Guid? teamMemberId, Guid? clientId, Guid? projectId, Guid? categoryId, DateTime? startDate, DateTime? endDate)
        {
            TeamMemberId = teamMemberId;
            ClientId = clientId;
            ProjectId = projectId;
            CategoryId = categoryId;
            if (startDate <= DateTime.MinValue || startDate >= DateTime.MaxValue)
            {
                startDate = (DateTime)SqlDateTime.MinValue;
            } else
            {
                StartDate = startDate;
            }
            if (endDate <= DateTime.MinValue || endDate >= DateTime.MaxValue)
            {
                EndDate = DateTime.Now;
            } else
            {
            EndDate = endDate;
            }
            this.Validate();
            }
        }
    }
