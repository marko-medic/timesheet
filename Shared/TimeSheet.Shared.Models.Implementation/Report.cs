﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeSheet.Shared.Models.Interfaces;

namespace TimeSheet.Shared.Models.Implementation
{
    public class Report : IReport
    {
        public DateTime Date { get; set; }
        public string TeamMemberName { get; set; }
        public string ProjectName { get; set; }
        public string CategoryName { get; set; }
        public string Description { get; set; }
        public decimal Time { get; set; }

        public Report(DateTime date, string teamMemberName, string projectName, string categoryName, string description, decimal time)
        {
            Date = date;
            TeamMemberName = teamMemberName;
            ProjectName = projectName;
            CategoryName = categoryName;
            Description = description;
            Time = time;
            this.Validate();
        }
    }
}
