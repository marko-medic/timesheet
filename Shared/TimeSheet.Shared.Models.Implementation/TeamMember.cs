﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeSheet.Shared.Models.Interfaces;

namespace TimeSheet.Shared.Models.Implementation
{
    public class TeamMember : ITeamMember
    {
        public Guid? Id { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
        public decimal HoursPerWeek { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }
        public bool IsAdmin { get; set; }

        public TeamMember(Guid? id, string name, string username, decimal hoursPerWeek, string email, bool isActive, bool isAdmin)
        {
            if (id == null || id == Guid.Empty)
            {
                Id = Guid.NewGuid();
            }
            else
            {
                Id = id;
            }

            Name = name;
            Username = username;
            HoursPerWeek = hoursPerWeek;
            Email = email;
            IsActive = isActive;
            IsAdmin = isAdmin;
            this.Validate();
        }

    }
}
