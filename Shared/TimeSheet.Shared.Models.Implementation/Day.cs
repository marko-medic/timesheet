﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeSheet.Shared.Models.Interfaces;

namespace TimeSheet.Shared.Models.Implementation
{
    public class Day : IDay
    {
        public DateTime Date { get; set; }
        public decimal TotalHours { get; set; }

        public Day(DateTime date, decimal totalHours = 0)
        {
            Date = date;
            TotalHours = totalHours;
            this.Validate();
        }

    }
}
