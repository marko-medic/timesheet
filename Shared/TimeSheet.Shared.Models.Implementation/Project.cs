﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeSheet.Shared.Models.Interfaces;

namespace TimeSheet.Shared.Models.Implementation
{
    public class Project : IProject
    {
        public Guid? Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid ClientId { get; set; }
        public Guid TeamMemberId { get; set; }
        public bool IsActive { get; set; }

        public Project(Guid? id, string name, Guid clientId, Guid teamMemberId, bool isActive, string description = null)
        {
            if (id == null || id == Guid.Empty)
            {
                Id = Guid.NewGuid();
            }
            else
            {
                Id = id;
            }
            Name = name;
            ClientId = clientId;
            TeamMemberId = teamMemberId;
            IsActive = isActive;
            Description = description;
            this.Validate();
        }

    }
}
