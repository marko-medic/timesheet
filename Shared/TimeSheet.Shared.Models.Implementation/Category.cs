﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeSheet.Shared.Models.Interfaces;

namespace TimeSheet.Shared.Models.Implementation
{
    public class Category : ICategory
    {
        public Guid? Id { get; set; }
        public string Name { get; set; }

        public Category(Guid? id, string name)
        {
            if (id == null || id == Guid.Empty)
            {
                Id = Guid.NewGuid();
            }
            else
            {
                Id = id;
            }
            Name = name;
            this.Validate();
        }
    }
}
