﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeSheet.Shared.Models.Interfaces;

namespace TimeSheet.Shared.Models.Implementation
{
    public class DayRecord : IDayRecord
    {
        public Guid? Id { get; set; }
        public DateTime DayDate { get; set; }
        public Guid ProjectId { get; set; }
        public Guid CategoryId { get; set; }
        public decimal Time { get; set; }
        public string Description { get; set; }
        public decimal? Overtime { get; set; }

        public DayRecord(Guid? id, DateTime dayDate, Guid projectId, Guid categoryId, decimal time, string description = null, decimal? overtime = null)
        {
            if (id == null || id == Guid.Empty)
            {
                Id = Guid.NewGuid();
            }
            else
            {
                Id = id;
            }
            DayDate = dayDate;
            ProjectId = projectId;
            CategoryId = categoryId;
            Time = time;
            Description = description;
            Overtime = overtime;
            this.Validate();
        }
    }
}
