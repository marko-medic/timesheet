﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TimeSheet.Shared.Models.Implementation;

namespace TimeSheet.Shared.Models.UnitTests
{
    [TestClass]
    public class CategoryModelTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException),
        "Name can't be null or empty string")]
        public void Category_InitWithNullCategoryName_ReturnsArgumentException()
        {
            Category category = new Category(Guid.NewGuid(), null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException),
        "Name can't be null or empty string")]
        public void Category_InitWithEmptyCategoryName_ReturnsArgumentException()
        {
            Category category = new Category(Guid.NewGuid(), "");
        }

        [TestMethod]
        public void Category_InitWithValidCategoryName_ReturnsCategoryInstance()
        {
            Category category = new Category(null, "Fron end");
            Assert.IsTrue(category != null);
        }
    }
}
