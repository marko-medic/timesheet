﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeSheet.Shared.Models.Implementation;
using TimeSheet.Shared.Models.Interfaces;

namespace TimeSheet.Shared.Models.UnitTests
{
    [TestClass]
    public class ProjectModelTests
    {
        private Guid _testTeamMemberGuid;
        private Guid _testClientGuid;

        public ProjectModelTests()
        {
            _testTeamMemberGuid = Guid.Parse("bb917d24-0de9-49ad-956e-0020b48f3dc7");
            _testClientGuid = Guid.Parse("af99c819-9ef3-4b1d-97b2-3069f5b9b175");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException),
       "Project name can't be null or empty string")]
        public void Client_InitWithNullProjectName_ReturnsArgumentException()
        {
            IProject project = new Project(Guid.NewGuid(), null, _testClientGuid, _testTeamMemberGuid, false, "desc");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException),
        "Project name can't be null or empty string")]
        public void Client_InitWithEmptyProjectName_ReturnsArgumentException()
        {
            IProject project = new Project(Guid.NewGuid(), "", _testClientGuid, _testTeamMemberGuid, false, "desc");
        }

        [TestMethod]
        public void Project_CorrectConstructor_ReturnsProjectInstance()
        {
            IProject project = new Project(Guid.NewGuid(), "ProjectX", _testClientGuid, _testTeamMemberGuid, false, "desc");
            Assert.IsTrue(project != null);
        }

        [TestMethod]
        public void Project_PassEmptyGuidForId_ReturnsProjectInstance()
        {
            IProject project = new Project(Guid.Empty, "Pr", _testClientGuid, _testTeamMemberGuid, false, "desc");
            Assert.IsTrue(project != null);
        }
    }
}
