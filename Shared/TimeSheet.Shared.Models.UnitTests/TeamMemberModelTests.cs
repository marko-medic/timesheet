﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TimeSheet.Shared.Models.Implementation;
using TimeSheet.Shared.Models.Interfaces;

namespace TimeSheet.Shared.Models.UnitTests
{
    [TestClass]
    public class TeamMemberModelTests
    {
        [TestMethod]
        public void TeamMember_InitializeWithCorrectConstructor_ReturnsInstance()
        {
            ITeamMember teamMember = new TeamMember(Guid.NewGuid(), "Max", "maxy", 55, "mail@mail.com", true, false);
            Assert.IsTrue(teamMember != null);
        }

        [TestMethod]
        public void TeamMember_PassNullInsteadGuidforId_ReturnsInstance()
        {
            ITeamMember teamMember = new TeamMember(null, "Max", "maxy", 55, "mail@mail.com", true, false);
            Assert.IsTrue(teamMember != null);
        }

        [TestMethod]
        public void TeamMember_PassEmptyGuidForId_ReturnsInstance()
        {
            ITeamMember teamMember = new TeamMember(Guid.Empty, "Max", "maxy", 55, "mail@mail.com", true, false);
            Assert.IsTrue(teamMember != null);
        }

        [TestMethod]
        public void TeamMember_PassInvalidHoursPerWeek_ThrowsArgumentException()
        {
            Assert.ThrowsException<ArgumentException>(() => new TeamMember(Guid.NewGuid(), "Max", "maxy", -2, null, true, false));
        }

        [TestMethod]
        public void TeamMember_InitializeWithIncorrectConstructor_ThrowsArgumentException()
        {
            Assert.ThrowsException<ArgumentException>(() => new TeamMember(Guid.NewGuid(), "Max", "maxy", 55, null, true, false));
        }
    }
}
