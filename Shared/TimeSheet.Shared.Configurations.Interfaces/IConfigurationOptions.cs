﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeSheet.Shared.Configurations.Interfaces
{
    public interface IConfigurationOptions
    {
        decimal MinExpectedWorkingHours { get; set; }
    }
}
