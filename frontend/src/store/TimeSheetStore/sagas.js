import { put, takeEvery, all } from "redux-saga/effects";
import axios from "../../shared/axiosInstance";
import {
  GET_TIMESHEET_REQUEST,
  GET_TIMESHEET_DAY_REQUEST,
  GET_TOTAL_HOURS_REQUEST,
  UPDATE_TIMESHEET_REQUEST
} from "./constants";
import {
  getTimeSheetSuccess,
  getTimeSheetFailure,
  getTimeSheetDaySuccess,
  getTimeSheetDayFailure,
  getTotalHoursSuccess,
  getTotalHoursFailure,
  updateTimeSheetSuccess,
  updateTimeSheetFailure
} from "./actions";

function* getTimeSheetSaga(action) {
  try {
    const res = yield axios.get(
      `/timeSheet?begin=${action.payload.begin}&end=${action.payload.end}`
    );
    yield put(getTimeSheetSuccess(res.data));
  } catch (err) {
    yield put(getTimeSheetFailure(err));
    console.log(err);
  }
}

function* getTimeSheetDaySaga(action) {
  try {
    const res = yield axios.get(`/timeSheet/${action.payload.date}`);
    yield put(getTimeSheetDaySuccess(res.data));
  } catch (err) {
    yield put(getTimeSheetDayFailure(err));
    console.log(err);
  }
}

function* getTotalHoursSaga(action) {
  let routeString = `/timeSheet/totalHours`;
  if (action.payload.date) {
    routeString += `?date=${action.payload.date}`;
  } else {
    routeString += `?begin=${action.payload.begin}&end=${action.payload.end}`;
  }

  try {
    const res = yield axios.get(routeString);
    yield put(getTotalHoursSuccess(res.data));
  } catch (err) {
    yield put(getTotalHoursFailure(err));
    console.log(err);
  }
}

function* updateTimeSheetSaga(action) {
  try {
    const res = yield axios.put(
      `/timeSheet/${action.payload.date}`,
      action.payload.day
    );
    yield put(updateTimeSheetSuccess(res.data));
  } catch (err) {
    yield put(updateTimeSheetFailure(err));
    console.log(err);
  }
}

export function* watchTimeSheet() {
  yield all([takeEvery(GET_TIMESHEET_REQUEST, getTimeSheetSaga)]);
  yield all([takeEvery(GET_TIMESHEET_DAY_REQUEST, getTimeSheetDaySaga)]);
  yield all([takeEvery(GET_TOTAL_HOURS_REQUEST, getTotalHoursSaga)]);
  yield all([takeEvery(UPDATE_TIMESHEET_REQUEST, updateTimeSheetSaga)]);
}
