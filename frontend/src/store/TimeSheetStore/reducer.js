import {
  GET_TIMESHEET_REQUEST,
  GET_TIMESHEET_SUCCESS,
  GET_TIMESHEET_FAILURE,
  GET_TIMESHEET_DAY_REQUEST,
  GET_TIMESHEET_DAY_SUCCESS,
  GET_TIMESHEET_DAY_FAILURE,
  GET_TOTAL_HOURS_REQUEST,
  GET_TOTAL_HOURS_SUCCESS,
  GET_TOTAL_HOURS_FAILURE,
  UPDATE_TIMESHEET_REQUEST,
  UPDATE_TIMESHEET_SUCCESS,
  UPDATE_TIMESHEET_FAILURE
} from "./constants";

const initialState = {
  timeSheet: {},
  dayRecords: [],
  isLoading: false,
  totalHours: 0,
  error: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_TIMESHEET_REQUEST:
    case GET_TIMESHEET_DAY_REQUEST:
    case GET_TOTAL_HOURS_REQUEST:
    case UPDATE_TIMESHEET_REQUEST:
      return {
        ...state,
        isLoading: true
      };
    case GET_TIMESHEET_SUCCESS:
      return {
        ...state,
        isLoading: false,
        timeSheet: { ...action.payload }
      };
    case GET_TIMESHEET_DAY_SUCCESS:
    case UPDATE_TIMESHEET_SUCCESS:
      return {
        ...state,
        isLoading: false,
        dayRecords: [...action.payload]
      };
    case GET_TOTAL_HOURS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        totalHours: action.payload
      };
    case GET_TIMESHEET_FAILURE:
    case GET_TIMESHEET_DAY_FAILURE:
    case GET_TOTAL_HOURS_FAILURE:
    case UPDATE_TIMESHEET_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: action.payload
      };
    default:
      return state;
  }
};

export default reducer;
