import {
  GET_TIMESHEET_REQUEST,
  GET_TIMESHEET_SUCCESS,
  GET_TIMESHEET_FAILURE,
  GET_TIMESHEET_DAY_REQUEST,
  GET_TIMESHEET_DAY_SUCCESS,
  GET_TIMESHEET_DAY_FAILURE,
  GET_TOTAL_HOURS_REQUEST,
  GET_TOTAL_HOURS_SUCCESS,
  GET_TOTAL_HOURS_FAILURE,
  UPDATE_TIMESHEET_REQUEST,
  UPDATE_TIMESHEET_SUCCESS,
  UPDATE_TIMESHEET_FAILURE
} from "./constants";

export const getTimeSheetRequest = payload => ({
  type: GET_TIMESHEET_REQUEST,
  payload
});

export const getTimeSheetSuccess = payload => ({
  type: GET_TIMESHEET_SUCCESS,
  payload
});

export const getTimeSheetFailure = payload => ({
  type: GET_TIMESHEET_FAILURE,
  payload
});

export const getTimeSheetDayRequest = payload => ({
  type: GET_TIMESHEET_DAY_REQUEST,
  payload
});

export const getTimeSheetDaySuccess = payload => ({
  type: GET_TIMESHEET_DAY_SUCCESS,
  payload
});

export const getTimeSheetDayFailure = payload => ({
  type: GET_TIMESHEET_DAY_FAILURE,
  payload
});

export const getTotalHoursRequest = payload => ({
  type: GET_TOTAL_HOURS_REQUEST,
  payload
});

export const getTotalHoursSuccess = payload => ({
  type: GET_TOTAL_HOURS_SUCCESS,
  payload
});

export const getTotalHoursFailure = payload => ({
  type: GET_TOTAL_HOURS_FAILURE,
  payload
});

export const updateTimeSheetRequest = payload => ({
  type: UPDATE_TIMESHEET_REQUEST,
  payload
});

export const updateTimeSheetSuccess = payload => ({
  type: UPDATE_TIMESHEET_SUCCESS,
  payload
});

export const updateTimeSheetFailure = payload => ({
  type: UPDATE_TIMESHEET_FAILURE,
  payload
});
