import { put, takeEvery, all } from "redux-saga/effects";
import axios from "../../shared/axiosInstance";
import {
  GET_PROJECTS_REQUEST,
  CREATE_PROJECT_REQUEST,
  UPDATE_PROJECT_REQUEST,
  REMOVE_PROJECT_REQUEST,
  GET_PROJECTS_BY_CLIENT_REQUEST,
  GET_PROJECTS_BY_CLIENT_AND_TEAM_MEMBER_REQUEST
} from "./constants";
import {
  getProjectsSuccess,
  getProjectsFailure,
  createProjectSuccess,
  createProjectFailure,
  updateProjectSuccess,
  updateProjectFailure,
  removeProjectSuccess,
  removeProjectFailure,
  getProjectsByClientSuccess,
  getProjectsByClientFailure,
  getProjectsByClientAndTeamMemberSuccess,
  getProjectsByClientAndTeamMemberFailure
} from "./actions";

function* getProjectsSaga(action) {
  let url = "/projects";
  if (action.payload) {
    if (action.payload.searchTerm) {
      url += `?searchTerm=${action.payload.searchTerm}`;
    } else if (action.payload.firstLetter) {
      url += `?firstLetter=${action.payload.firstLetter}`;
    }
  }

  try {
    const res = yield axios.get(url);
    yield put(getProjectsSuccess(res.data));
  } catch (err) {
    yield put(getProjectsFailure(err));
  }
}

function* createProjectSaga(action) {
  try {
    const res = yield axios.post("/projects", action.payload);
    yield put(createProjectSuccess(res.data));
  } catch (err) {
    yield put(createProjectFailure(err));
    console.dir(err);
  }
}

function* updateProjectSaga(action) {
  try {
    const res = yield axios.put(
      `/projects/${action.payload.Id}`,
      action.payload
    );
    yield put(updateProjectSuccess(res.data));
  } catch (err) {
    yield put(updateProjectFailure(err));
    console.dir(err);
  }
}

function* removeProjectSaga(action) {
  try {
    const res = yield axios.delete(`/projects/${action.payload}`);
    yield put(removeProjectSuccess(res.data));
  } catch (err) {
    yield put(removeProjectFailure(err));
    console.dir(err);
  }
}

function* getProjectsByClientSaga(action) {
  try {
    const res = yield axios.get(`/projects/filter-by-client/${action.payload}`);
    yield put(getProjectsByClientSuccess(res.data));
  } catch (err) {
    yield put(getProjectsByClientFailure(err));
    console.dir(err);
  }
}

function* getProjectsByClientAndTeamMemberSaga(action) {
  try {
    const res = yield axios.get(
      `/projects/filter-by-clientAndTeamMember/${action.payload.clientId}/${
        action.payload.teamMemberId
      }`
    );
    yield put(getProjectsByClientAndTeamMemberSuccess(res.data));
  } catch (err) {
    yield put(getProjectsByClientAndTeamMemberFailure(err));
    console.dir(err);
  }
}

export function* watchProjects() {
  yield all([takeEvery(GET_PROJECTS_REQUEST, getProjectsSaga)]);
  yield all([takeEvery(CREATE_PROJECT_REQUEST, createProjectSaga)]);
  yield all([takeEvery(UPDATE_PROJECT_REQUEST, updateProjectSaga)]);
  yield all([takeEvery(REMOVE_PROJECT_REQUEST, removeProjectSaga)]);
  yield all([
    takeEvery(GET_PROJECTS_BY_CLIENT_REQUEST, getProjectsByClientSaga)
  ]);
  yield all([
    takeEvery(
      GET_PROJECTS_BY_CLIENT_AND_TEAM_MEMBER_REQUEST,
      getProjectsByClientAndTeamMemberSaga
    )
  ]);
}
