import {
  GET_PROJECTS_REQUEST,
  GET_PROJECTS_SUCCESS,
  GET_PROJECTS_FAILURE,
  CREATE_PROJECT_REQUEST,
  CREATE_PROJECT_SUCCESS,
  CREATE_PROJECT_FAILURE,
  UPDATE_PROJECT_REQUEST,
  UPDATE_PROJECT_SUCCESS,
  UPDATE_PROJECT_FAILURE,
  REMOVE_PROJECT_REQUEST,
  REMOVE_PROJECT_SUCCESS,
  REMOVE_PROJECT_FAILURE,
  RESET_PROJECTS,
  GET_PROJECTS_BY_CLIENT_REQUEST,
  GET_PROJECTS_BY_CLIENT_SUCCESS,
  GET_PROJECTS_BY_CLIENT_FAILURE,
  GET_PROJECTS_BY_CLIENT_AND_TEAM_MEMBER_REQUEST,
  GET_PROJECTS_BY_CLIENT_AND_TEAM_MEMBER_SUCCESS,
  GET_PROJECTS_BY_CLIENT_AND_TEAM_MEMBER_FAILURE
} from "./constants";

export const getProjectsRequest = payload => ({
  type: GET_PROJECTS_REQUEST,
  payload
});

export const getProjectsSuccess = payload => ({
  type: GET_PROJECTS_SUCCESS,
  payload
});

export const getProjectsFailure = payload => ({
  type: GET_PROJECTS_FAILURE,
  payload
});

export const createProjectRequest = payload => ({
  type: CREATE_PROJECT_REQUEST,
  payload
});

export const createProjectSuccess = payload => ({
  type: CREATE_PROJECT_SUCCESS,
  payload
});

export const createProjectFailure = payload => ({
  type: CREATE_PROJECT_FAILURE,
  payload
});

export const updateProjectRequest = payload => ({
  type: UPDATE_PROJECT_REQUEST,
  payload
});

export const updateProjectSuccess = payload => ({
  type: UPDATE_PROJECT_SUCCESS,
  payload
});

export const updateProjectFailure = payload => ({
  type: UPDATE_PROJECT_FAILURE,
  payload
});

export const removeProjectRequest = payload => ({
  type: REMOVE_PROJECT_REQUEST,
  payload
});

export const removeProjectSuccess = payload => ({
  type: REMOVE_PROJECT_SUCCESS,
  payload
});

export const removeProjectFailure = payload => ({
  type: REMOVE_PROJECT_FAILURE,
  payload
});

export const getProjectsByClientRequest = payload => ({
  type: GET_PROJECTS_BY_CLIENT_REQUEST,
  payload
});

export const getProjectsByClientSuccess = payload => ({
  type: GET_PROJECTS_BY_CLIENT_SUCCESS,
  payload
});

export const getProjectsByClientFailure = payload => ({
  type: GET_PROJECTS_BY_CLIENT_FAILURE,
  payload
});

export const getProjectsByClientAndTeamMemberRequest = payload => ({
  type: GET_PROJECTS_BY_CLIENT_AND_TEAM_MEMBER_REQUEST,
  payload
});

export const getProjectsByClientAndTeamMemberSuccess = payload => ({
  type: GET_PROJECTS_BY_CLIENT_AND_TEAM_MEMBER_SUCCESS,
  payload
});

export const getProjectsByClientAndTeamMemberFailure = payload => ({
  type: GET_PROJECTS_BY_CLIENT_AND_TEAM_MEMBER_FAILURE,
  payload
});

export const resetProjects = () => ({
  type: RESET_PROJECTS
});
