import {
  GET_PROJECTS_REQUEST,
  GET_PROJECTS_SUCCESS,
  GET_PROJECTS_FAILURE,
  CREATE_PROJECT_SUCCESS,
  CREATE_PROJECT_FAILURE,
  UPDATE_PROJECT_SUCCESS,
  UPDATE_PROJECT_FAILURE,
  REMOVE_PROJECT_SUCCESS,
  REMOVE_PROJECT_FAILURE,
  RESET_PROJECTS,
  CREATE_PROJECT_REQUEST,
  UPDATE_PROJECT_REQUEST,
  REMOVE_PROJECT_REQUEST,
  GET_PROJECTS_BY_CLIENT_REQUEST,
  GET_PROJECTS_BY_CLIENT_SUCCESS,
  GET_PROJECTS_BY_CLIENT_FAILURE,
  GET_PROJECTS_BY_CLIENT_AND_TEAM_MEMBER_REQUEST,
  GET_PROJECTS_BY_CLIENT_AND_TEAM_MEMBER_SUCCESS,
  GET_PROJECTS_BY_CLIENT_AND_TEAM_MEMBER_FAILURE
} from "./constants";

const initialState = {
  isLoading: false,
  error: null,
  projectList: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_PROJECTS_REQUEST:
    case UPDATE_PROJECT_REQUEST:
    case REMOVE_PROJECT_REQUEST:
    case GET_PROJECTS_BY_CLIENT_REQUEST:
    case GET_PROJECTS_BY_CLIENT_AND_TEAM_MEMBER_REQUEST:
    case CREATE_PROJECT_REQUEST: {
      return {
        ...state,
        isLoading: true
      };
    }

    case GET_PROJECTS_SUCCESS:
    case GET_PROJECTS_BY_CLIENT_SUCCESS:
    case GET_PROJECTS_BY_CLIENT_AND_TEAM_MEMBER_SUCCESS:
      return {
        ...state,
        projectList: [...action.payload],
        isLoading: false
      };

    case CREATE_PROJECT_SUCCESS:
      return {
        ...state,
        projectList: state.projectList.concat(action.payload),
        isLoading: false
      };
    case UPDATE_PROJECT_SUCCESS:
      const updatedProjectIndex = state.projectList.findIndex(
        project => project.Id === action.payload.Id
      );
      state.projectList[updatedProjectIndex] = { ...action.payload };
      return {
        ...state,
        projectList: [...state.projectList],
        isLoading: false
      };
    case REMOVE_PROJECT_SUCCESS:
      const removedProject = state.projectList.find(
        project => project.Id === action.payload.Id
      );
      state.projectList = state.projectList.filter(
        project => project.Id !== removedProject.Id
      );

      return {
        ...state,
        isLoading: false
      };

    case GET_PROJECTS_FAILURE:
    case UPDATE_PROJECT_FAILURE:
    case REMOVE_PROJECT_FAILURE:
    case CREATE_PROJECT_FAILURE:
    case GET_PROJECTS_BY_CLIENT_FAILURE:
    case GET_PROJECTS_BY_CLIENT_AND_TEAM_MEMBER_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: action.payload
      };
    case RESET_PROJECTS:
      return {
        projectList: []
      };
    default:
      return state;
  }
};

export default reducer;
