import { put, takeEvery, all } from "redux-saga/effects";
import axios from "../../shared/axiosInstance";
import { GET_REPORTS_REQUEST } from "./constants";
import { getReportsSuccess, getReportsFailure } from "./actions";

function* getReportsSaga(action) {
  let url = "/reports";
  if (action.payload) {
    const serializedObject = new URLSearchParams(action.payload).toString();
    url += `?${serializedObject}`;
  }
  try {
    const res = yield axios.get(url);
    yield put(getReportsSuccess(res.data));
  } catch (err) {
    yield put(getReportsFailure(err));
    console.dir(err);
  }
}

export function* watchReports() {
  yield all([takeEvery(GET_REPORTS_REQUEST, getReportsSaga)]);
}
