import {
  GET_REPORTS_REQUEST,
  GET_REPORTS_SUCCESS,
  GET_REPORTS_FAILURE
} from "./constants";

export const getReportsRequest = payload => ({
  type: GET_REPORTS_REQUEST,
  payload
});

export const getReportsSuccess = payload => ({
  type: GET_REPORTS_SUCCESS,
  payload
});

export const getReportsFailure = payload => ({
  type: GET_REPORTS_FAILURE,
  payload
});
