import {
  GET_REPORTS_REQUEST,
  GET_REPORTS_SUCCESS,
  GET_REPORTS_FAILURE
} from "./constants";

const initialState = {
  isLoading: false,
  error: null,
  reportList: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_REPORTS_REQUEST:
      return {
        ...state,
        isLoading: true
      };
    case GET_REPORTS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        reportList: [...action.payload]
      };
    case GET_REPORTS_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: action.payload
      };

    default:
      return state;
  }
};

export default reducer;
