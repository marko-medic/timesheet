import { put, takeEvery, all } from "redux-saga/effects";
import axios from "../../shared/axiosInstance";
import {
  GET_CLIENTS_REQUEST,
  CREATE_CLIENT_REQUEST,
  UPDATE_CLIENT_REQUEST,
  REMOVE_CLIENT_REQUEST
} from "./constants";
import {
  getClientsSuccess,
  getClientsFailure,
  createClientSuccess,
  createClientFailure,
  updateClientSuccess,
  updateClientFailure,
  removeClientSuccess,
  removeClientFailure
} from "./actions";

function* getClientsSaga(action) {
  let url = "/clients";
  if (action.payload) {
    if (action.payload.searchTerm) {
      url += `?searchTerm=${action.payload.searchTerm}`;
    } else if (action.payload.firstLetter) {
      url += `?firstLetter=${action.payload.firstLetter}`;
    }
  }

  try {
    const res = yield axios.get(url);
    yield put(getClientsSuccess(res.data));
  } catch (err) {
    yield put(getClientsFailure(err));
  }
}

function* createClientSaga(action) {
  try {
    const res = yield axios.post("/clients", action.payload);
    yield put(createClientSuccess(res.data));
  } catch (err) {
    yield put(createClientFailure(err));
    console.dir(err);
  }
}

function* updateClientSaga(action) {
  try {
    const res = yield axios.put(
      `/clients/${action.payload.Id}`,
      action.payload
    );
    yield put(updateClientSuccess(res.data));
  } catch (err) {
    yield put(updateClientFailure(err));
    console.dir(err);
  }
}

function* removeClientSaga(action) {
  try {
    const res = yield axios.delete(`/clients/${action.payload}`);
    yield put(removeClientSuccess(res.data));
  } catch (err) {
    yield put(removeClientFailure(err));
    console.dir(err);
  }
}

export function* watchClients() {
  yield all([takeEvery(GET_CLIENTS_REQUEST, getClientsSaga)]);
  yield all([takeEvery(CREATE_CLIENT_REQUEST, createClientSaga)]);
  yield all([takeEvery(UPDATE_CLIENT_REQUEST, updateClientSaga)]);
  yield all([takeEvery(REMOVE_CLIENT_REQUEST, removeClientSaga)]);
}
