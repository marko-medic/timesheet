import {
  GET_CLIENTS_REQUEST,
  GET_CLIENTS_SUCCESS,
  GET_CLIENTS_FAILURE,
  CREATE_CLIENT_SUCCESS,
  CREATE_CLIENT_FAILURE,
  UPDATE_CLIENT_SUCCESS,
  UPDATE_CLIENT_FAILURE,
  REMOVE_CLIENT_SUCCESS,
  REMOVE_CLIENT_FAILURE,
  RESET_CLIENTS,
  CREATE_CLIENT_REQUEST,
  UPDATE_CLIENT_REQUEST,
  REMOVE_CLIENT_REQUEST
} from "./constants";

const initialState = {
  isLoading: false,
  error: null,
  clientList: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_CLIENTS_REQUEST:
    case UPDATE_CLIENT_REQUEST:
    case REMOVE_CLIENT_REQUEST:
    case CREATE_CLIENT_REQUEST: {
      return {
        ...state,
        isLoading: true
      };
    }

    case GET_CLIENTS_SUCCESS:
      return {
        ...state,
        clientList: [...action.payload],
        isLoading: false
      };

    case CREATE_CLIENT_SUCCESS:
      return {
        ...state,
        clientList: state.clientList.concat(action.payload),
        isLoading: false
      };
    case UPDATE_CLIENT_SUCCESS:
      const updatedClientIndex = state.clientList.findIndex(
        client => client.Id === action.payload.Id
      );
      state.clientList[updatedClientIndex] = { ...action.payload };
      return {
        ...state,
        clientList: [...state.clientList],
        isLoading: false
      };
    case REMOVE_CLIENT_SUCCESS:
      const removedClient = state.clientList.find(
        client => client.Id === action.payload.Id
      );
      state.clientList = state.clientList.filter(
        client => client.Id !== removedClient.Id
      );

      return {
        ...state,
        isLoading: false
      };

    case GET_CLIENTS_FAILURE:
    case UPDATE_CLIENT_FAILURE:
    case REMOVE_CLIENT_FAILURE:
    case CREATE_CLIENT_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: action.payload
      };
    case RESET_CLIENTS:
      return {
        clientList: []
      };
    default:
      return state;
  }
};

export default reducer;
