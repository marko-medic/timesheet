import {
  GET_CLIENTS_REQUEST,
  GET_CLIENTS_SUCCESS,
  GET_CLIENTS_FAILURE,
  CREATE_CLIENT_REQUEST,
  CREATE_CLIENT_SUCCESS,
  CREATE_CLIENT_FAILURE,
  UPDATE_CLIENT_REQUEST,
  UPDATE_CLIENT_SUCCESS,
  UPDATE_CLIENT_FAILURE,
  REMOVE_CLIENT_REQUEST,
  REMOVE_CLIENT_SUCCESS,
  REMOVE_CLIENT_FAILURE,
  RESET_CLIENTS
} from "./constants";

export const getClientsRequest = payload => ({
  type: GET_CLIENTS_REQUEST,
  payload
});

export const getClientsSuccess = payload => ({
  type: GET_CLIENTS_SUCCESS,
  payload
});

export const getClientsFailure = payload => ({
  type: GET_CLIENTS_FAILURE,
  payload
});

export const createClientRequest = payload => ({
  type: CREATE_CLIENT_REQUEST,
  payload
});

export const createClientSuccess = payload => ({
  type: CREATE_CLIENT_SUCCESS,
  payload
});

export const createClientFailure = payload => ({
  type: CREATE_CLIENT_FAILURE,
  payload
});

export const updateClientRequest = payload => ({
  type: UPDATE_CLIENT_REQUEST,
  payload
});

export const updateClientSuccess = payload => ({
  type: UPDATE_CLIENT_SUCCESS,
  payload
});

export const updateClientFailure = payload => ({
  type: UPDATE_CLIENT_FAILURE,
  payload
});

export const removeClientRequest = payload => ({
  type: REMOVE_CLIENT_REQUEST,
  payload
});

export const removeClientSuccess = payload => ({
  type: REMOVE_CLIENT_SUCCESS,
  payload
});

export const removeClientFailure = payload => ({
  type: REMOVE_CLIENT_FAILURE,
  payload
});

export const resetClients = () => ({
  type: RESET_CLIENTS
});
