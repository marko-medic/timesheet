import { put, takeEvery, all } from "redux-saga/effects";
import axios from "../../shared/axiosInstance";
import { GET_COUNTRIES_REQUEST } from "./constants";
import { getCountriesSuccess, getCountriesFailure } from "./actions";

function* getCountriesSaga() {
  try {
    const res = yield axios.get("/countries");
    yield put(getCountriesSuccess(res.data));
  } catch (err) {
    yield put(getCountriesFailure(err));
  }
}

export function* watchCountries() {
  yield all([takeEvery(GET_COUNTRIES_REQUEST, getCountriesSaga)]);
}
