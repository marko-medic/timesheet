import { GET_COUNTRIES_SUCCESS, GET_COUNTRIES_FAILURE } from "./constants";

const initialState = {
  countryList: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_COUNTRIES_SUCCESS:
      return {
        ...state,
        countryList: [...action.payload]
      };
    case GET_COUNTRIES_FAILURE:
      return {
        ...state,
        error: action.payload
      };

    default:
      return state;
  }
};

export default reducer;
