import {
  GET_COUNTRIES_REQUEST,
  GET_COUNTRIES_SUCCESS,
  GET_COUNTRIES_FAILURE
} from "./constants";

export const getCountriesRequest = () => ({
  type: GET_COUNTRIES_REQUEST
});

export const getCountriesSuccess = payload => ({
  type: GET_COUNTRIES_SUCCESS,
  payload
});

export const getCountriesFailure = payload => ({
  type: GET_COUNTRIES_FAILURE,
  payload
});
