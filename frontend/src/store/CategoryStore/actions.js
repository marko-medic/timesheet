import {
  GET_CATEGORIES_REQUEST,
  GET_CATEGORIES_SUCCESS,
  GET_CATEGORIES_FAILURE
} from "./constants";

export const getCategoriesRequest = () => ({
  type: GET_CATEGORIES_REQUEST
});

export const getCategoriesSuccess = payload => ({
  type: GET_CATEGORIES_SUCCESS,
  payload
});

export const getCategoriesFailure = payload => ({
  type: GET_CATEGORIES_FAILURE,
  payload
});
