import { GET_CATEGORIES_SUCCESS, GET_CATEGORIES_FAILURE } from "./constants";

const initialState = {
  categoryList: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_CATEGORIES_SUCCESS:
      return {
        ...state,
        categoryList: [...action.payload]
      };
    case GET_CATEGORIES_FAILURE:
      return {
        ...state,
        error: action.payload
      };

    default:
      return state;
  }
};

export default reducer;
