import { put, takeEvery, all } from "redux-saga/effects";
import axios from "../../shared/axiosInstance";
import { GET_CATEGORIES_REQUEST } from "./constants";
import { getCategoriesSuccess, getCategoriesFailure } from "./actions";

function* getCategoriesSaga() {
  try {
    const res = yield axios.get("/categories");
    yield put(getCategoriesSuccess(res.data));
  } catch (err) {
    yield put(getCategoriesFailure(err));
  }
}

export function* watchCategories() {
  yield all([takeEvery(GET_CATEGORIES_REQUEST, getCategoriesSaga)]);
}
