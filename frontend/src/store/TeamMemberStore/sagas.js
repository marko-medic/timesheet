import { put, takeEvery, all } from "redux-saga/effects";
import axios from "../../shared/axiosInstance";
import {
  GET_TEAM_MEMBERS_REQUEST,
  CREATE_TEAM_MEMBER_REQUEST,
  UPDATE_TEAM_MEMBER_REQUEST,
  REMOVE_TEAM_MEMBER_REQUEST
} from "./constants";
import {
  getTeamMembersSuccess,
  getTeamMembersFailure,
  createTeamMemberSuccess,
  createTeamMemberFailure,
  updateTeamMemberSuccess,
  updateTeamMemberFailure,
  removeTeamMemberSuccess,
  removeTeamMemberFailure
} from "./actions";

function* getTeamMembersSaga() {
  try {
    const res = yield axios.get("/teamMembers");
    yield put(getTeamMembersSuccess(res.data));
  } catch (err) {
    yield put(getTeamMembersFailure(err));
  }
}

function* createTeamMemberSaga(action) {
  try {
    const res = yield axios.post("/teamMembers", action.payload);
    yield put(createTeamMemberSuccess(res.data));
  } catch (err) {
    yield put(createTeamMemberFailure(err));
    console.dir(err);
  }
}

function* updateTeamMemberSaga(action) {
  try {
    const res = yield axios.put(
      `/teamMembers/${action.payload.Id}`,
      action.payload
    );
    yield put(updateTeamMemberSuccess(res.data));
  } catch (err) {
    yield put(updateTeamMemberFailure(err));
    console.dir(err);
  }
}

function* removeTeamMemberSaga(action) {
  try {
    const res = yield axios.delete(`/teamMembers/${action.payload}`);
    yield put(removeTeamMemberSuccess(res.data));
  } catch (err) {
    yield put(removeTeamMemberFailure(err));
    console.dir(err);
  }
}

export function* watchTeamMembers() {
  yield all([takeEvery(GET_TEAM_MEMBERS_REQUEST, getTeamMembersSaga)]);
  yield all([takeEvery(CREATE_TEAM_MEMBER_REQUEST, createTeamMemberSaga)]);
  yield all([takeEvery(UPDATE_TEAM_MEMBER_REQUEST, updateTeamMemberSaga)]);
  yield all([takeEvery(REMOVE_TEAM_MEMBER_REQUEST, removeTeamMemberSaga)]);
}
