import {
  GET_TEAM_MEMBERS_REQUEST,
  GET_TEAM_MEMBERS_SUCCESS,
  GET_TEAM_MEMBERS_FAILURE,
  CREATE_TEAM_MEMBER_REQUEST,
  CREATE_TEAM_MEMBER_SUCCESS,
  CREATE_TEAM_MEMBER_FAILURE,
  UPDATE_TEAM_MEMBER_REQUEST,
  UPDATE_TEAM_MEMBER_SUCCESS,
  UPDATE_TEAM_MEMBER_FAILURE,
  REMOVE_TEAM_MEMBER_REQUEST,
  REMOVE_TEAM_MEMBER_SUCCESS,
  REMOVE_TEAM_MEMBER_FAILURE,
  RESET_TEAM_MEMBERS
} from "./constants";

export const getTeamMembersRequest = () => ({
  type: GET_TEAM_MEMBERS_REQUEST
});

export const getTeamMembersSuccess = payload => ({
  type: GET_TEAM_MEMBERS_SUCCESS,
  payload
});

export const getTeamMembersFailure = payload => ({
  type: GET_TEAM_MEMBERS_FAILURE,
  payload
});

export const createTeamMemberRequest = payload => ({
  type: CREATE_TEAM_MEMBER_REQUEST,
  payload
});

export const createTeamMemberSuccess = payload => ({
  type: CREATE_TEAM_MEMBER_SUCCESS,
  payload
});

export const createTeamMemberFailure = payload => ({
  type: CREATE_TEAM_MEMBER_FAILURE,
  payload
});

export const updateTeamMemberRequest = payload => ({
  type: UPDATE_TEAM_MEMBER_REQUEST,
  payload
});

export const updateTeamMemberSuccess = payload => ({
  type: UPDATE_TEAM_MEMBER_SUCCESS,
  payload
});

export const updateTeamMemberFailure = payload => ({
  type: UPDATE_TEAM_MEMBER_FAILURE,
  payload
});

export const removeTeamMemberRequest = payload => ({
  type: REMOVE_TEAM_MEMBER_REQUEST,
  payload
});

export const removeTeamMemberSuccess = payload => ({
  type: REMOVE_TEAM_MEMBER_SUCCESS,
  payload
});

export const removeTeamMemberFailure = payload => ({
  type: REMOVE_TEAM_MEMBER_FAILURE,
  payload
});

export const resetTeamMembers = () => ({
  type: RESET_TEAM_MEMBERS
});
