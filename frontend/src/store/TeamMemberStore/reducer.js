import {
  GET_TEAM_MEMBERS_REQUEST,
  GET_TEAM_MEMBERS_SUCCESS,
  GET_TEAM_MEMBERS_FAILURE,
  CREATE_TEAM_MEMBER_SUCCESS,
  CREATE_TEAM_MEMBER_FAILURE,
  UPDATE_TEAM_MEMBER_SUCCESS,
  UPDATE_TEAM_MEMBER_FAILURE,
  REMOVE_TEAM_MEMBER_SUCCESS,
  REMOVE_TEAM_MEMBER_FAILURE,
  RESET_TEAM_MEMBERS,
  CREATE_TEAM_MEMBER_REQUEST,
  UPDATE_TEAM_MEMBER_REQUEST,
  REMOVE_TEAM_MEMBER_REQUEST
} from "./constants";

const initialState = {
  isLoading: false,
  error: null,
  teamMemberList: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_TEAM_MEMBERS_REQUEST:
    case UPDATE_TEAM_MEMBER_REQUEST:
    case REMOVE_TEAM_MEMBER_REQUEST:
    case CREATE_TEAM_MEMBER_REQUEST: {
      return {
        ...state,
        isLoading: true
      };
    }

    case GET_TEAM_MEMBERS_SUCCESS:
      return {
        ...state,
        teamMemberList: [...action.payload],
        isLoading: false
      };

    case CREATE_TEAM_MEMBER_SUCCESS:
      return {
        ...state,
        teamMemberList: state.teamMemberList.concat(action.payload),
        isLoading: false
      };
    case UPDATE_TEAM_MEMBER_SUCCESS:
      const updatedTeamMemberIndex = state.teamMemberList.findIndex(
        teamMember => teamMember.Id === action.payload.Id
      );
      state.teamMemberList[updatedTeamMemberIndex] = { ...action.payload };
      return {
        ...state,
        teamMemberList: [...state.teamMemberList],
        isLoading: false
      };
    case REMOVE_TEAM_MEMBER_SUCCESS:
      const removedTeamMember = state.teamMemberList.find(
        teamMember => teamMember.Id === action.payload.Id
      );
      state.teamMemberList = state.teamMemberList.filter(
        teamMember => teamMember.Id !== removedTeamMember.Id
      );

      return {
        ...state,
        isLoading: false
      };

    case GET_TEAM_MEMBERS_FAILURE:
    case UPDATE_TEAM_MEMBER_FAILURE:
    case REMOVE_TEAM_MEMBER_FAILURE:
    case CREATE_TEAM_MEMBER_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: action.payload
      };
    case RESET_TEAM_MEMBERS:
      return {
        teamMemberList: []
      };
    default:
      return state;
  }
};

export default reducer;
