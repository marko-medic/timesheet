import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import PageTitle from "../../components/Shared/PageTitle";
import GreyBox from "components/Reports/GreyBox";
import ReportsTable from "components/Reports/ReportsTable";
import ReportsTotal from "components/Reports/ReportsTotal";
import BottomBox from "components/Reports/BottomBox";
import { getReportsRequest } from "store/ReportStore/actions";
import { getTeamMembersRequest } from "store/TeamMemberStore/actions";
import { getClientsRequest } from "store/ClientStore/actions";
import { getCategoriesRequest } from "store/CategoryStore/actions";
import { getProjectsByClientAndTeamMemberRequest } from "store/ProjectStore/actions";
import { areObjectPropsEmpty } from "shared/helpers";

function Reports({
  reports,
  getReports,
  teamMembers,
  getTeamMembers,
  clients,
  getClients,
  categories,
  getCategories,
  projects,
  getProjects
}) {
  const initialState = {
    TeamMemberId: "",
    ClientId: "",
    ProjectId: "",
    CategoryId: "",
    StartDate: "",
    EndDate: ""
  };
  const [reportState, setReportState] = useState(initialState);

  useEffect(() => {
    getTeamMembers();
    getClients();
    getReports();
    getCategories();
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    if (reportState.ClientId && reportState.TeamMemberId) {
      getProjects({
        clientId: reportState.ClientId,
        teamMemberId: reportState.TeamMemberId
      });
    }
    // eslint-disable-next-line
  }, [reportState.ClientId, reportState.TeamMemberId]);

  const resetHandler = e => {
    e.preventDefault();
    setReportState(initialState);
  };

  const searchHandler = e => {
    e.preventDefault();
    const reportPayload = {
      ...reportState,
      StartDate: reportState.StartDate
        ? reportState.StartDate.toLocaleDateString()
        : "",
      EndDate: reportState.EndDate
        ? reportState.EndDate.toLocaleDateString()
        : ""
    };
    getReports(
      areObjectPropsEmpty(reportPayload, Object.keys(reportPayload))
        ? null
        : reportPayload
    );
  };

  const changeReportHandler = (key, value) =>
    setReportState(state => ({
      ...state,
      [key]: value
    }));

  return (
    <>
      <PageTitle textClass="reports">Reports</PageTitle>
      <GreyBox
        reportState={reportState}
        changeReport={changeReportHandler}
        reset={resetHandler}
        search={searchHandler}
        teamMembers={teamMembers}
        clients={clients}
        categories={categories}
        projects={projects}
      />
      <ReportsTable reports={reports} />
      <ReportsTotal>{reports.length}</ReportsTotal>
      <BottomBox />
    </>
  );
}

const mapStateToProps = state => ({
  reports: state.reports.reportList,
  teamMembers: state.teamMembers.teamMemberList,
  clients: state.clients.clientList,
  categories: state.categories.categoryList,
  projects: state.projects.projectList
});

const mapDispatchToProps = dispatch => ({
  getReports: payload => dispatch(getReportsRequest(payload)),
  getTeamMembers: () => dispatch(getTeamMembersRequest()),
  getClients: () => dispatch(getClientsRequest()),
  getCategories: () => dispatch(getCategoriesRequest()),
  getProjects: payload =>
    dispatch(getProjectsByClientAndTeamMemberRequest(payload))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Reports);
