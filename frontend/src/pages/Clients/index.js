import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { ROWS_PER_PAGE, OFFSET } from "../../shared/constants";
import {
  getClientsRequest,
  createClientRequest,
  updateClientRequest,
  removeClientRequest,
  resetClients
} from "../../store/ClientStore/actions";
import { getCurrentLetter, getCurrentPage } from "../../shared/helpers";
import { getCountriesRequest } from "../../store/CountryStore/actions";
import PageTitle from "../../components/Shared/PageTitle";
import GreyBox from "../../components/Shared/GreyBox";
import Dialog from "../../components/Shared/Dialog";
import DialogItems from "../../components/Clients/DialogItems";
import LetterMenu from "../../components/Shared/LetterMenu";
import Accordion from "../../components/Shared/Accordion";
import Pagination from "../../components/Shared/Pagination";
import AccordionItems from "../../components/Clients/AccordionItems";
import AccordionWrap from "components/Shared/AccordionWrap";

function Clients({
  clients,
  countries,
  isLoading,
  error,
  getClients,
  createClient,
  updateClient,
  removeClient,
  resetAllClients,
  getCountries
}) {
  const [isCreateModalOpen, toggleCreateModalOpen] = useState(false);
  const [isErrorModalOpen, toggleErrorModalOpen] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);

  useEffect(() => {
    setCurrentPage(getCurrentPage(window.location.href));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [window.location.href]);

  useEffect(() => {
    if (error) {
      toggleErrorModalOpen(true);
    }
  }, [error]);

  useEffect(() => {
    currentLetter ? getClients({ firstLetter: currentLetter }) : getClients();
    getCountries();
    return () => resetAllClients();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const currentLetter = getCurrentLetter(window.location.href);
  const offset = currentPage * OFFSET - OFFSET;

  const renderClients = () =>
    clients.slice(offset, offset + ROWS_PER_PAGE).map(client => (
      <Accordion key={client.Id} title={client.Name}>
        <AccordionItems
          client={client}
          isLoading={isLoading}
          countries={countries}
          updateClient={updateClient}
          removeClient={removeClient}
        />
      </Accordion>
    ));

  return (
    <>
      <PageTitle textClass="clients">Clients</PageTitle>
      {isErrorModalOpen && (
        <Dialog
          close={() => toggleErrorModalOpen(false)}
          errorMessage={error.response.data.ExceptionMessage}
          title="Error occurred"
        />
      )}
      <GreyBox
        searchItems={getClients}
        openDialog={() => toggleCreateModalOpen(true)}
        show={true}
        inputName="search-clients"
      >
        Create new client
      </GreyBox>

      {isCreateModalOpen && (
        <Dialog
          close={() => toggleCreateModalOpen(false)}
          title="Create new client"
          errorMessage={error ? error.response.data.Message : ""}
        >
          <DialogItems
            isLoading={isLoading}
            error={!!error}
            countries={countries}
            createClient={createClient}
            close={() => toggleCreateModalOpen(false)}
          />
        </Dialog>
      )}

      <LetterMenu items={clients} filterAction={getClients} />
      <AccordionWrap accordionClass="clients">{renderClients()}</AccordionWrap>
      <Pagination
        itemsCount={clients.length}
        currentPage={currentPage}
        setPage={setCurrentPage}
      />
    </>
  );
}

export const mapStateToProps = state => ({
  clients: state.clients.clientList,
  countries: state.countries.countryList,
  error: state.clients.error,
  isLoading: state.clients.isLoading
});

export const mapDispatchToProps = dispatch => ({
  getClients: payload => dispatch(getClientsRequest(payload)),
  createClient: payload => dispatch(createClientRequest(payload)),
  updateClient: payload => dispatch(updateClientRequest(payload)),
  removeClient: payload => dispatch(removeClientRequest(payload)),
  resetAllClients: () => dispatch(resetClients()),
  getCountries: () => dispatch(getCountriesRequest())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Clients);
