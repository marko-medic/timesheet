import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { ROWS_PER_PAGE, OFFSET } from "../../shared/constants";
import {
  getProjectsRequest,
  createProjectRequest,
  updateProjectRequest,
  removeProjectRequest,
  resetProjects
} from "../../store/ProjectStore/actions";
import { getClientsRequest } from "store/ClientStore/actions";
import { getTeamMembersRequest } from "store/TeamMemberStore/actions";
import { getCurrentLetter, getCurrentPage } from "../../shared/helpers";
import PageTitle from "../../components/Shared/PageTitle";
import GreyBox from "../../components/Shared/GreyBox";
import Dialog from "../../components/Shared/Dialog";
import DialogItems from "../../components/Projects/DialogItems";
import LetterMenu from "../../components/Shared/LetterMenu";
import Accordion from "../../components/Shared/Accordion";
import Pagination from "../../components/Shared/Pagination";
import AccordionItems from "../../components/Projects/AccordionItems";
import AccordionWrap from "components/Shared/AccordionWrap";

function Projects({
  projects,
  clients,
  teamMembers,
  isLoading,
  error,
  getProjects,
  getClients,
  getTeamMembers,
  createProject,
  updateProject,
  removeProject,
  resetAllProjects
}) {
  const [isOpen, toggleOpen] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);

  useEffect(() => {
    setCurrentPage(getCurrentPage(window.location.href));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [window.location.href]);

  useEffect(() => {
    currentLetter ? getProjects({ firstLetter: currentLetter }) : getProjects();
    getClients();
    getTeamMembers();
    return () => resetAllProjects();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const currentLetter = getCurrentLetter(window.location.href);
  const offset = currentPage * OFFSET - OFFSET;

  const renderProjects = () =>
    projects.slice(offset, offset + ROWS_PER_PAGE).map(project => (
      <Accordion key={project.Id} title={project.Name}>
        <AccordionItems
          project={project}
          clients={clients}
          teamMembers={teamMembers}
          isLoading={isLoading}
          updateProject={updateProject}
          removeProject={removeProject}
        />
      </Accordion>
    ));

  return (
    <>
      <PageTitle textClass="projects">Projects</PageTitle>
      <GreyBox
        searchItems={getProjects}
        openDialog={() => toggleOpen(true)}
        show={true}
        inputName="search-projects"
      >
        Create new project
      </GreyBox>

      {isOpen && (
        <Dialog
          close={() => toggleOpen(false)}
          title="Create new project"
          errorMessage={error ? error.response.data.Message : ""}
        >
          <DialogItems
            isLoading={isLoading}
            error={!!error}
            createProject={createProject}
            close={() => toggleOpen(false)}
            clients={clients}
            teamMembers={teamMembers}
          />
        </Dialog>
      )}

      <LetterMenu items={projects} filterAction={getProjects} />
      <AccordionWrap accordionClass="projects">
        {renderProjects()}
      </AccordionWrap>
      <Pagination
        itemsCount={projects.length}
        currentPage={currentPage}
        setPage={setCurrentPage}
      />
    </>
  );
}

export const mapStateToProps = state => ({
  projects: state.projects.projectList,
  error: state.projects.error,
  isLoading: state.projects.isLoading,
  teamMembers: state.teamMembers.teamMemberList,
  clients: state.clients.clientList
});

export const mapDispatchToProps = dispatch => ({
  getProjects: payload => dispatch(getProjectsRequest(payload)),
  getClients: () => dispatch(getClientsRequest()),
  getTeamMembers: () => dispatch(getTeamMembersRequest()),
  createProject: payload => dispatch(createProjectRequest(payload)),
  updateProject: payload => dispatch(updateProjectRequest(payload)),
  removeProject: payload => dispatch(removeProjectRequest(payload)),
  resetAllProjects: () => dispatch(resetProjects())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Projects);
