import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import PageTitle from "../../components/Shared/PageTitle";
import GreyBox from "../../components/TimeSheet/GreyBox";
import MonthTable from "../../components/TimeSheet/MonthTable";
import TotalHours from "../../components/TimeSheet/TotalHours";
import { getBeginAndEndDate } from "shared/helpers";
import { DAYS_PER_TABLE } from "components/TimeSheet/MonthTable/constants";
import {
  getTimeSheetRequest,
  getTotalHoursRequest
} from "store/TimeSheetStore/actions";
import { TIMESHEET_PAGE_PATH } from "shared/pagePaths";

function TimeSheet({
  timeSheet,
  getTimeSheet,
  totalHours,
  getTotalHours,
  match
}) {
  const [date, setDate] = useState(new Date());
  const [param, setParam] = useState(match.params.dateQuery);

  useEffect(() => {
    let d = new Date(date);
    if (param && window.location.pathname !== TIMESHEET_PAGE_PATH) {
      const [year, month] = param.split("-");
      if (year && month) {
        const monthNum = Number(month - 1);
        const yearNum = Number(year);
        d.setMonth(monthNum);
        d.setFullYear(yearNum);
      }
    } else {
      d = new Date();
    }

    const { beginDate, endDate } = getBeginAndEndDate(d, DAYS_PER_TABLE);
    const dateObject = {
      begin: beginDate.toLocaleDateString(),
      end: endDate.toLocaleDateString()
    };

    getTotalHours(dateObject);
    getTimeSheet(dateObject);

    setDate(d);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [window.location.pathname]);

  return (
    <>
      <PageTitle textClass="timesheet">TimeSheet</PageTitle>
      <GreyBox setParam={setParam} date={date} path={TIMESHEET_PAGE_PATH} />
      <MonthTable
        date={date}
        timeSheet={timeSheet}
        daysPerTable={DAYS_PER_TABLE}
      />
      <TotalHours>{totalHours}</TotalHours>
    </>
  );
}

const mapStateToProps = state => ({
  timeSheet: state.timeSheetReducer.timeSheet,
  totalHours: state.timeSheetReducer.totalHours
});

const mapDispatchToProps = dispatch => ({
  getTimeSheet: payload => dispatch(getTimeSheetRequest(payload)),
  getTotalHours: payload => dispatch(getTotalHoursRequest(payload))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TimeSheet);
