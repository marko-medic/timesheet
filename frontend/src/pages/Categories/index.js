import React from "react";
import PageTitle from "../../components/Shared/PageTitle";

function Categories() {
  return (
    <>
      <PageTitle textClass="categories">Categories</PageTitle>
    </>
  );
}

export default Categories;
