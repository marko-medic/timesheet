import React, { useState, useEffect, useMemo } from "react";
import { v4 } from "uuid";
import { connect } from "react-redux";
import PageTitle from "../../components/Shared/PageTitle";
import GreyBox from "components/Days/GreyBox";
import DaysTable from "components/Days/DaysTable";
import TotalHours from "components/Days/TotalHours";
import { getSelectedWeek, isDateGreater, isRowFilled } from "shared/helpers";
import { REQUIRED_INPUT_FIELDS } from "./constants";
import {
  getTotalHoursRequest,
  getTimeSheetDayRequest,
  updateTimeSheetRequest
} from "store/TimeSheetStore/actions";
import { MIN_ROWS_PER_DAY } from "components/Days/constants";
import DayRow from "components/Days/DayRow";
import useStyles from "./styles";
import { getClientsRequest } from "store/ClientStore/actions";
import { getCategoriesRequest } from "store/CategoryStore/actions";
import { TIMESHEET_PAGE_PATH } from "shared/pagePaths";
import { getProjectsRequest } from "store/ProjectStore/actions";

function Days({
  totalHours,
  getTotalHours,
  dayRecords,
  getDayRecords,
  updateTimeSheet,
  clients,
  projects,
  categories,
  getClients,
  getProjects,
  getCategories,
  history,
  match
}) {
  const [date, setDate] = useState(new Date());
  const { week, selectedDay } = getSelectedWeek(date);
  const [param, setParam] = useState(match.params.day);
  const [dayState, setDayState] = useState(
    new Array(MIN_ROWS_PER_DAY).fill({})
  );
  const [isValid, setIsValid] = useState(true);
  const classes = useStyles();
  useEffect(() => {
    const d = new Date(param);
    if (isDateGreater(d, new Date())) {
      history.push(TIMESHEET_PAGE_PATH);
    }
    setDate(d);
    getDayRecords({ date: d.toDateString() });
    getClients();
    getProjects();
    getCategories();
    // eslint-disable-next-line
  }, [window.location.href]);

  useEffect(() => {
    getTotalHours({ date: date.toDateString() });
    // eslint-disable-next-line
  }, [date, dayRecords]);

  const updateDayState = (newDayState, index) => {
    dayState[index] = newDayState;
    setDayState([...dayState]);
  };

  const updateDayHandler = e => {
    e.preventDefault();
    if (!isValid) {
      return;
    }

    const dayRecords = dayState
      .filter(day => isRowFilled(day, REQUIRED_INPUT_FIELDS))
      .map(day => ({ ...day, DayDate: date.toLocaleDateString() }));

    updateTimeSheet({
      date: date.toDateString(),
      day: dayRecords
    });
  };

  const mapDayObject = o => ({
    Id: "",
    Description: "",
    Time: "",
    Overtime: "",
    ClientId: "",
    ProjectId: "",
    CategoryId: "",
    ...o
  });

  const renderDays = useMemo(() => {
    const rowsToFill =
      Math.max(MIN_ROWS_PER_DAY, dayRecords.length) + 1 - dayRecords.length;
    const emptyRows = new Array(rowsToFill).fill({});
    const daysData = [...dayRecords, ...emptyRows];
    return daysData
      .map((dayObject, index) => {
        const mappedDay = mapDayObject(dayObject);
        updateDayState(mappedDay, index);
        return mappedDay;
      })
      .map((day, index) => (
        <DayRow
          setValidity={setIsValid}
          clients={clients}
          projects={projects}
          categories={categories}
          index={index}
          requiredFields={REQUIRED_INPUT_FIELDS}
          updateDayState={updateDayState}
          day={day}
          key={v4()}
        />
      ));
    // eslint-disable-next-line
  }, [dayRecords, clients, categories]);

  const buttonClass = isValid ? "" : `${classes.disabled} disable`;

  return (
    <>
      <PageTitle textClass="timesheet">TimeSheet</PageTitle>
      <GreyBox
        date={date}
        selectedWeek={week}
        selectedDay={selectedDay}
        setDate={setDate}
        setParam={setParam}
      />
      {dayRecords && <DaysTable>{renderDays}</DaysTable>}
      <div className="buttons">
        <div className="inner">
          <a
            onClick={updateDayHandler}
            href="/"
            className={`btn green ${classes.button} ${buttonClass}`}
          >
            Save
          </a>
        </div>
      </div>
      <TotalHours>{totalHours}</TotalHours>
    </>
  );
}

const mapStateToProps = state => ({
  dayRecords: state.timeSheetReducer.dayRecords,
  totalHours: state.timeSheetReducer.totalHours,
  clients: state.clients.clientList,
  projects: state.projects.projectList,
  categories: state.categories.categoryList
});

const mapDispatchToProps = dispatch => ({
  getTotalHours: payload => dispatch(getTotalHoursRequest(payload)),
  getDayRecords: payload => dispatch(getTimeSheetDayRequest(payload)),
  updateTimeSheet: payload => dispatch(updateTimeSheetRequest(payload)),
  getClients: () => dispatch(getClientsRequest()),
  getProjects: () => dispatch(getProjectsRequest()),
  getCategories: () => dispatch(getCategoriesRequest())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Days);
