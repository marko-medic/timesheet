import { makeStyles } from "@material-ui/styles";

export default makeStyles({
  button: {
    margin: "-.3rem .5rem .5rem .5rem",
    float: "right"
  },
  disabled: {
    pointer: "no-drop",
    pointerEvents: "none",
    opacity: 0.7
  }
});
