import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { OFFSET, ROWS_PER_PAGE } from "../../shared/constants";
import { getCurrentPage } from "../../shared/helpers";
import {
  getTeamMembersRequest,
  createTeamMemberRequest,
  updateTeamMemberRequest,
  removeTeamMemberRequest,
  resetTeamMembers
} from "../../store/TeamMemberStore/actions";
import PageTitle from "../../components/Shared/PageTitle";
import GreyBox from "components/Shared/GreyBox";
import Accordion from "components/Shared/Accordion";
import Dialog from "components/Shared/Dialog";
import DialogItems from "components/TeamMembers/DialogItems";
import AccordionItems from "components/TeamMembers/AccordionItems";
import AccordionWrap from "components/Shared/AccordionWrap";
import Pagination from "components/Shared/Pagination";

function TeamMembers({
  teamMembers,
  getTeamMembers,
  createTeamMember,
  updateTeamMember,
  removeTeamMember,
  resetAllTeamMembers,
  isLoading,
  error
}) {
  const [isCreateModalOpen, toggleCreateModalOpen] = useState(false);
  const [isErrorModalOpen, toggleErrorModalOpen] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const offset = OFFSET * currentPage - OFFSET;

  useEffect(() => {
    getTeamMembers();
    return () => resetAllTeamMembers();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    setCurrentPage(getCurrentPage(window.location.href));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [window.location.href]);

  useEffect(() => {
    if (error) {
      toggleErrorModalOpen(true);
    }
  }, [error]);

  const renderTeamMembers = () =>
    teamMembers.slice(offset, offset + ROWS_PER_PAGE).map(teamMember => (
      <Accordion key={teamMember.Id} title={teamMember.Name}>
        <AccordionItems
          teamMember={teamMember}
          isLoading={isLoading}
          updateTeamMember={updateTeamMember}
          removeTeamMember={removeTeamMember}
        />
      </Accordion>
    ));

  return (
    <>
      <PageTitle textClass="team-member">Team members</PageTitle>
      {isErrorModalOpen && (
        <Dialog
          errorMessage={error.response.data.ExceptionMessage}
          title="Error occurred"
          close={() => toggleErrorModalOpen(false)}
        />
      )}
      <GreyBox
        openDialog={() => toggleCreateModalOpen(true)}
        titleClass="ico-member"
      >
        Create new member
      </GreyBox>
      {isCreateModalOpen && (
        <Dialog
          close={() => toggleCreateModalOpen(false)}
          title="Create new team member"
          errorMessage={error ? error.response.data.Message : ""}
        >
          <DialogItems
            isLoading={isLoading}
            error={!!error}
            createTeamMember={createTeamMember}
            close={() => toggleCreateModalOpen(false)}
          />
        </Dialog>
      )}
      <AccordionWrap>{renderTeamMembers()}</AccordionWrap>
      <Pagination
        itemsCount={teamMembers.length}
        currentPage={currentPage}
        setPage={setCurrentPage}
      />
    </>
  );
}

export const mapStateToProps = state => ({
  teamMembers: state.teamMembers.teamMemberList,
  error: state.teamMembers.error,
  isLoading: state.teamMembers.isLoading
});

export const mapDispatchToProps = dispatch => ({
  getTeamMembers: () => dispatch(getTeamMembersRequest()),
  createTeamMember: payload => dispatch(createTeamMemberRequest(payload)),
  updateTeamMember: payload => dispatch(updateTeamMemberRequest(payload)),
  removeTeamMember: payload => dispatch(removeTeamMemberRequest(payload)),
  resetAllTeamMembers: () => dispatch(resetTeamMembers())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TeamMembers);
