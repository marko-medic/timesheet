import React from "react";
import logo from "../../assets/img/logo-large.png";

function Login() {
  return (
    <>
      <div className="logo-wrap">
        <a href="index.html" className="inner">
          <img src={logo} alt="Logo" />
        </a>
      </div>
      <div className="centered-content-wrap">
        <div className="centered-block">
          <h1>Login</h1>
          <ul>
            <li>
              <input
                type="text"
                placeholder="Email"
                className="in-text large"
              />
            </li>
            <li>
              <input
                type="password"
                placeholder="Password"
                className="in-pass large"
              />
            </li>
            <li className="last">
              <input type="checkbox" className="in-checkbox" id="remember" />
              <label className="in-label" htmlFor="remember">
                Remember me
              </label>
              <span className="right">
                <a href="/" className="link">
                  Forgot password?
                </a>
                <a href="/" className="btn orange">
                  Login
                </a>
              </span>
            </li>
          </ul>
        </div>
      </div>
    </>
  );
}

export default Login;
