import axios from "axios";

const axiosInstance = axios.create({
  baseURL: "http://localhost:50039/api/"
});

export default axiosInstance;
