import { pick } from "lodash";
export const mapItem = item => {
  for (let key in item) {
    if (item[key] === null) {
      item[key] = "";
    }
  }
  return item;
};

export const areObjectPropsEmpty = (o, propsToLook) => {
  const mappedObject = pick(o, propsToLook);
  for (let key in mappedObject) {
    if (mappedObject[key] !== "") {
      return false;
    }
  }
  return true;
};

export const isRowFilled = (o, propsToLook) => {
  const mappedObject = pick(o, propsToLook);
  for (let key in mappedObject) {
    if (mappedObject[key] === "") {
      return false;
    }
  }
  return true;
};

export const getCurrentLetter = href => {
  const isLetterEntered = href.includes("firstLetter");
  if (!isLetterEntered) return false;
  const urlStringsArray = href.split("firstLetter=");
  return urlStringsArray[urlStringsArray.length - 1].toUpperCase();
};

export const getCountryId = (client, countries) => {
  const country = countries.find(country => country.Id === client.CountryId);
  if (country === undefined) return "initial";
  return country.Id;
};

export const getFirstLetters = items =>
  items
    .map(item => item.Name[0].toUpperCase())
    .filter((letter, index, array) => array.indexOf(letter) === index);

export const getCurrentPage = href => {
  const isPageNumberEntered = href.includes("page");
  if (!isPageNumberEntered) {
    return 1;
  }
  const urlStringsArray = href.split("page=");
  return Number(urlStringsArray[urlStringsArray.length - 1]);
};

export const getInvalidFiels = (item, fields) =>
  fields.filter(field => item[field] === "");

export const isEmptyGuid = val =>
  val === "00000000-0000-0000-0000-000000000000";

export const getMonthName = monthNum => {
  if (monthNum < 0 || monthNum > 11) {
    return "Invalid month";
  }
  const months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
  ];
  return months[monthNum];
};

const isLeapYear = year =>
  (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0;

export const getNumOfDaysForMonth = monthName => {
  switch (monthName.toLowerCase()) {
    case "january":
    case "march":
    case "may":
    case "july":
    case "august":
    case "october":
    case "december":
      return 31;
    case "february":
      return isLeapYear(2019) ? 29 : 28;
    case "april":
    case "june":
    case "september":
    case "november":
      return 30;
    default:
      throw new Error("Invalid month name");
  }
};

const getFirstDayInMonth = date => {
  const d = new Date(date);
  d.setDate(1);
  const firstDayInMonth = d.getDay();
  return firstDayInMonth;
};

export const isDateGreater = (date1, date2) =>
  date1.getTime() > date2.getTime();

export const areDatesSame = (date1, date2) =>
  date1.toDateString() === date2.toDateString();

export const getBeginAndEndDate = (date, daysPerPage) => {
  const firstDayInMonth =
    getFirstDayInMonth(date) !== 0 ? getFirstDayInMonth(date) : 7;

  const beginDate = new Date(date);
  beginDate.setDate(2 - firstDayInMonth);
  const endDate = new Date(beginDate);
  endDate.setDate(endDate.getDate() + daysPerPage - 1);

  return {
    beginDate,
    endDate
  };
};

export const getDayName = dayNum => {
  const days = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday"
  ];
  return days[dayNum];
};

export const isObjectEmpty = o => Object.keys(o).length === 0;

export const getSelectedWeek = date => {
  const weekArray = [];
  let d = new Date(date);
  let selectedDay = 1;
  while (d.getDay() !== 1) {
    if (areDatesSame(d, date)) {
      selectedDay = d.getDay();
    }
    d.setDate(d.getDate() - 1);
  }

  for (let i = 0; i < 7; i++) {
    weekArray.push(new Date(d));

    d.setDate(d.getDate() + 1);
  }
  return { week: weekArray, selectedDay: selectedDay !== 0 ? selectedDay : 7 };
};

export const getMonthAndDateString = date => {
  const dateStringArray = date.toDateString().split(" ");
  return `${dateStringArray[1]} ${dateStringArray[2]}`;
};
