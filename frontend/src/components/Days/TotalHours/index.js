import React from "react";
import { withRouter } from "react-router-dom";
import { TIMESHEET_PAGE_PATH } from "shared/pagePaths";

function TotalHours({ children, history }) {
  const goToTimeSheet = e => {
    e.preventDefault();
    history.push(TIMESHEET_PAGE_PATH);
  };
  return (
    <div className="total">
      <a onClick={goToTimeSheet} href="/">
        <i />
        back to monthly view
      </a>
      <span>
        Total hours: <em>{children}</em>
      </span>
    </div>
  );
}

export default withRouter(TotalHours);
