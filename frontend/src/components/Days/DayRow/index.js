import React, { useState, useEffect } from "react";
import { isEmpty } from "lodash";
import axios from "../../../shared/axiosInstance";
import { areObjectPropsEmpty, isRowFilled } from "shared/helpers";

function DayRow({
  updateDayState,
  day,
  index,
  clients,
  projects,
  setValidity,
  categories,
  requiredFields
}) {
  const [dayState, setDayState] = useState({ ...day });
  const [clientProjects, setClientProjects] = useState([]);

  useEffect(() => {
    let shouldFetch = false;
    if (dayState.ProjectId) {
      shouldFetch = true;
      axios
        .get(`clients/filter-by-project/${dayState.ProjectId}`)
        .then(res => {
          if (shouldFetch && res.data) {
            changeDayHandler(res.data.Id, "ClientId");
            shouldFetch = false;
          }
        })
        .catch(err => {
          console.log(err);
        });
      return () => (shouldFetch = false);
    }
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    let shouldFetch = false;
    if (dayState.ClientId) {
      shouldFetch = true;
      axios
        .get(`/projects/filter-by-client/${dayState.ClientId}`)
        .then(res => {
          if (shouldFetch && res.data) {
            setClientProjects(res.data);
            updateProjectId(res.data);
            shouldFetch = false;
          }
        })
        .catch(err => {
          console.log(err);
        });
    } else {
      changeDayHandler("", "ProjectId");
    }

    return () => (shouldFetch = false);
    // eslint-disable-next-line
  }, [dayState.ClientId]);

  const updateProjectId = projects => {
    const clientProject = projects.find(
      project => project.Id === dayState.ProjectId
    );
    const projectIdValue = clientProject ? clientProject.Id : "";
    changeDayHandler(projectIdValue, "ProjectId");
  };

  const changeDayHandler = (value, key) => {
    const newDayState = { ...dayState, [key]: value };
    updateDayState(newDayState, index);
    setDayState(newDayState);

    if (
      areObjectPropsEmpty(newDayState, requiredFields) ||
      isRowFilled(newDayState, requiredFields)
    ) {
      setValidity(true);
    } else {
      setValidity(false);
    }
  };

  const renderClients = () =>
    clients.map(client => (
      <option key={client.Id} value={client.Id}>
        {client.Name}
      </option>
    ));

  const renderProjects = () => {
    const projectsToMap = isEmpty(clientProjects) ? projects : clientProjects;
    return projectsToMap.map(project => (
      <option key={project.Id} value={project.Id}>
        {project.Name}
      </option>
    ));
  };

  const renderCategories = () =>
    categories.map(category => (
      <option key={category.Id} value={category.Id}>
        {category.Name}
      </option>
    ));

  return (
    <tr>
      <td>
        <select
          value={dayState.ClientId}
          onChange={e => {
            changeDayHandler(e.target.value, "ClientId");
          }}
        >
          <option value="">Choose client</option>
          {renderClients()}
        </select>
      </td>
      <td>
        <select
          disabled={!dayState.ClientId ? true : ""}
          value={dayState.ProjectId}
          onChange={e => changeDayHandler(e.target.value, "ProjectId")}
        >
          <option value="">Select project</option>
          {renderProjects()}
        </select>
      </td>
      <td>
        <select
          value={dayState.CategoryId}
          onChange={e => changeDayHandler(e.target.value, "CategoryId")}
        >
          <option value="">Choose category</option>
          {renderCategories()}
        </select>
      </td>
      <td>
        <input
          type="text"
          className="in-text medium"
          value={dayState.Description}
          onChange={e => changeDayHandler(e.target.value, "Description")}
        />
      </td>
      <td className="small">
        <input
          type="text"
          className="in-text xsmall"
          value={dayState.Time}
          onChange={e => changeDayHandler(e.target.value, "Time")}
        />
      </td>
      <td className="small">
        <input
          type="text"
          className="in-text xsmall"
          value={dayState.Overtime || ""}
          onChange={e => changeDayHandler(e.target.value, "Overtime")}
        />
      </td>
    </tr>
  );
}

export default DayRow;
