import React from "react";

function DaysTable({ children }) {
  return (
    <table className="default-table">
      <tbody>
        <tr>
          <th>
            Client <em>*</em>
          </th>
          <th>
            Project <em>*</em>
          </th>
          <th>
            Category <em>*</em>
          </th>
          <th>Description</th>
          <th className="small">
            Time <em>*</em>
          </th>
          <th className="small">Overtime</th>
        </tr>
        {children}
      </tbody>
    </table>
  );
}

export default DaysTable;
