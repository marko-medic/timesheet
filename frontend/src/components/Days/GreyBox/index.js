import React from "react";
import { getDayName, getMonthAndDateString } from "shared/helpers";
import { DAYS_PAGE_PATH } from "shared/pagePaths";

function GreyBox({ date, selectedWeek, selectedDay, setParam }) {
  const changeDayHandler = (e, passedDate) => {
    e.preventDefault();
    const paramString = passedDate.toLocaleDateString().replace(/\//g, "-");
    setParam(paramString);
    window.history.replaceState(null, null, `${DAYS_PAGE_PATH}/${paramString}`);
  };

  const changeWeekHandler = (e, count) => {
    e.preventDefault();
    const d = new Date(date);
    d.setDate(d.getDate() + count);
    changeDayHandler(e, d);
  };

  const renderSelectedWeek = () =>
    selectedWeek.map(day => (
      <li
        key={day.getTime()}
        className={date.toDateString() === day.toDateString() ? "active" : ""}
      >
        <a onClick={e => changeDayHandler(e, day)} href="/">
          <b>{getMonthAndDateString(day)}</b>
          <span>{getDayName(day.getDay())}</span>
        </a>
      </li>
    ));
  return (
    <div className="grey-box-wrap">
      <div className="top">
        <a onClick={e => changeWeekHandler(e, -7)} href="/" className="prev">
          <i className="zmdi zmdi-chevron-left" />
          previous week
        </a>
        <span className="center">
          {getMonthAndDateString(selectedWeek[0])} -{" "}
          {getMonthAndDateString(selectedWeek[selectedWeek.length - 1])},{" "}
          {date.getFullYear()} (day {selectedDay})
        </span>
        <a onClick={e => changeWeekHandler(e, 7)} href="/" className="next">
          next week
          <i className="zmdi zmdi-chevron-right" />
        </a>
      </div>
      <div className="bottom">
        <ul className="days">{renderSelectedWeek()}</ul>
      </div>
    </div>
  );
}

export default GreyBox;
