import React, { useState, useEffect } from "react";
import { getInvalidFiels } from "../../../shared/helpers";
import { REQUIRED_FIELDS } from "../constants";
import ErrorMessage from "../../Shared/ErrorMessage";
import useStyles from "../../Shared/styles";

function DialogItems({ countries, createClient, close, error, isLoading }) {
  const [buttonClicked, setButtonClicked] = useState(false);
  const [isValid, setIsValid] = useState(true);
  const [clientState, setClientState] = useState({
    Name: "",
    Address: "",
    City: "",
    ZipCode: "",
    CountryId: ""
  });
  const classes = useStyles();

  useEffect(() => {
    if (buttonClicked && !isLoading && !error) {
      close();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isLoading, buttonClicked, error]);

  const renderCountries = () =>
    countries.map(country => (
      <option key={country.Id} value={country.Id}>
        {country.Name}
      </option>
    ));

  const createClientHandler = e => {
    e.preventDefault();

    if (getInvalidFiels(clientState, REQUIRED_FIELDS).length > 0) {
      return setIsValid(false);
    }
    const countryId =
      clientState.CountryId === "" ? null : clientState.CountryId;
    createClient({ ...clientState, CountryId: countryId });
    setButtonClicked(true);
  };

  const inputHandler = (key, value) =>
    setClientState({ ...clientState, [key]: value });

  return (
    <>
      <ul className="form">
        <li>
          <label>Client name:</label>
          <input
            value={clientState.Name}
            onChange={e => inputHandler("Name", e.target.value)}
            type="text"
            className="in-text"
          />
          {!isValid && (
            <ErrorMessage
              fieldName="Name"
              itemState={clientState}
              requiredFields={REQUIRED_FIELDS}
            />
          )}
        </li>
        <li>
          <label>Address:</label>
          <input
            value={clientState.Address}
            onChange={e => inputHandler("Address", e.target.value)}
            type="text"
            className="in-text"
          />
        </li>
        <li>
          <label>City:</label>
          <input
            value={clientState.City}
            onChange={e => inputHandler("City", e.target.value)}
            type="text"
            className="in-text"
          />
        </li>
        <li>
          <label>Zip/Postal code:</label>
          <input
            value={clientState.ZipCode}
            onChange={e => inputHandler("ZipCode", e.target.value)}
            type="text"
            className="in-text"
          />
        </li>
        <li>
          <label>Country:</label>
          <select
            onChange={e => inputHandler("CountryId", e.target.value)}
            value={clientState.CountryId}
          >
            <option value="">Select country</option>
            {renderCountries()}
          </select>
        </li>
      </ul>
      <div className="buttons">
        <div className="inner">
          <a
            onClick={createClientHandler}
            href="/"
            className={`btn green ${isLoading ? classes.disabled : ""}`}
          >
            Save
          </a>
        </div>
      </div>
    </>
  );
}

export default DialogItems;
