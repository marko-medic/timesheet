import React, { useState } from "react";
import { REQUIRED_FIELDS } from "../constants";
import { mapItem, getInvalidFiels, isEmptyGuid } from "shared/helpers";
import ErrorMessage from "../../Shared/ErrorMessage";
import useStyles from "../../Shared/styles";

function AccordionItems({
  client,
  updateClient,
  removeClient,
  isLoading,
  countries
}) {
  const [clientState, setClientState] = useState(mapItem(client));
  const [isValid, setIsValid] = useState(true);
  const classes = useStyles();

  const handleChange = (key, value) =>
    setClientState({ ...clientState, [key]: value });

  const renderCountries = () =>
    countries.map(country => (
      <option key={country.Id} value={country.Id}>
        {country.Name}
      </option>
    ));

  const updateClientHandler = e => {
    e.preventDefault();

    if (getInvalidFiels(clientState, REQUIRED_FIELDS).length > 0) {
      return setIsValid(false);
    }

    if (isLoading) {
      return;
    }

    const countryId =
      clientState.CountryId === "" || isEmptyGuid(clientState.CountryId)
        ? null
        : clientState.CountryId;
    updateClient({
      ...clientState,
      CountryId: countryId
    });
  };

  const removeClientHandler = e => {
    e.preventDefault();
    if (isLoading) {
      return;
    }
    removeClient(clientState.Id);
  };

  return (
    <>
      <ul className="form">
        <li>
          <label>Client name:</label>
          <input
            type="text"
            className="in-text"
            value={clientState.Name}
            onChange={e => handleChange("Name", e.target.value)}
          />
          {!isValid && (
            <ErrorMessage
              fieldName="Name"
              itemState={clientState}
              requiredFields={REQUIRED_FIELDS}
            />
          )}
        </li>
        <li>
          <label>Zip/Postal code:</label>
          <input
            type="text"
            className="in-text"
            value={clientState.ZipCode}
            onChange={e => handleChange("ZipCode", e.target.value)}
          />
        </li>
      </ul>
      <ul className="form">
        <li>
          <label>Address:</label>
          <input
            type="text"
            className="in-text"
            value={clientState.Address}
            onChange={e => handleChange("Address", e.target.value)}
          />
        </li>
        <li>
          <label>Country:</label>
          <select
            value={clientState.CountryId}
            onChange={e => handleChange("CountryId", e.target.value)}
          >
            <option value="">Select country</option>
            {renderCountries()}
          </select>
        </li>
      </ul>
      <ul className="form last">
        <li>
          <label>City:</label>
          <input
            type="text"
            className="in-text"
            value={clientState.City}
            onChange={e => handleChange("City", e.target.value)}
          />
        </li>
      </ul>
      <div className="buttons">
        <div className="inner">
          <a
            onClick={updateClientHandler}
            href="/"
            className={`btn green ${isLoading ? classes.disabled : ""}`}
          >
            Save
          </a>
          <a
            onClick={removeClientHandler}
            href="/"
            className={`btn red ${isLoading ? classes.disabled : ""}`}
          >
            Delete
          </a>
        </div>
      </div>
    </>
  );
}

export default AccordionItems;
