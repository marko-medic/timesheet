import React from "react";

function ReportsTotal({ children }) {
  return (
    <div className="total">
      <span>
        Report total: <em>{children}</em>
      </span>
    </div>
  );
}

export default ReportsTotal;
