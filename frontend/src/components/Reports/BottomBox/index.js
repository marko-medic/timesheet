import React from "react";

function BottomBox() {
  return (
    <div className="grey-box-wrap reports">
      <div className="btns-inner">
        <a href="/" className="btn white">
          <span>Print report</span>
        </a>
        <a href="/" className="btn white">
          <span>Create PDF</span>
        </a>
        <a href="/" className="btn white">
          <span>Export to excel</span>
        </a>
      </div>
    </div>
  );
}

export default BottomBox;
