import React from "react";
import { v4 } from "uuid";
function ReportsTable({ reports }) {
  const renderReports = () =>
    reports.map(report => (
      <tr key={v4()}>
        <td>{report.Date && new Date(report.Date).toLocaleDateString()}</td>
        <td>{report.TeamMemberName}</td>
        <td>{report.ProjectName}</td>
        <td>{report.CategoryName}</td>
        <td>{report.Description || "-"}</td>
        <td>{report.Time}</td>
      </tr>
    ));

  return (
    <table className="default-table">
      <tbody>
        <tr>
          <th>Date</th>
          <th>Team member</th>
          <th>Projects</th>
          <th>Categories</th>
          <th>Description</th>
          <th className="small">Time</th>
        </tr>
        {renderReports()}
      </tbody>
    </table>
  );
}

export default ReportsTable;
