import React from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { isDateGreater } from "shared/helpers";

function GreyBox({
  reportState,
  changeReport,
  reset,
  search,
  teamMembers,
  clients,
  categories,
  projects
}) {
  const renderTeamMembers = () =>
    teamMembers.map(teamMember => (
      <option key={teamMember.Id} value={teamMember.Id}>
        {teamMember.Name}
      </option>
    ));

  const renderCategories = () =>
    categories.map(category => (
      <option key={category.Id} value={category.Id}>
        {category.Name}
      </option>
    ));

  const renderClients = () =>
    clients.map(client => (
      <option key={client.Id} value={client.Id}>
        {client.Name}
      </option>
    ));

  const renderProjects = () =>
    projects.map(project => (
      <option key={project.Id} value={project.Id}>
        {project.Name}
      </option>
    ));

  return (
    <div className="grey-box-wrap reports">
      <ul className="form">
        <li>
          <label>Team member:</label>
          <select
            value={reportState.TeamMemberId}
            onChange={e => changeReport("TeamMemberId", e.target.value)}
          >
            <option value="">All</option>
            {renderTeamMembers()}
          </select>
        </li>
        <li>
          <label>Category:</label>
          <select
            value={reportState.CategoryId}
            onChange={e => changeReport("CategoryId", e.target.value)}
          >
            <option value="">All</option>
            {renderCategories()}
          </select>
        </li>
      </ul>
      <ul className="form">
        <li>
          <label>Client:</label>
          <select
            value={reportState.ClientId}
            onChange={e => changeReport("ClientId", e.target.value)}
          >
            <option value="">All</option>
            {renderClients()}
          </select>
        </li>
        <li>
          <label>Start date:</label>
          <DatePicker
            className="in-text datepicker"
            selected={
              isDateGreater(new Date(reportState.StartDate), new Date())
                ? new Date()
                : reportState.StartDate
            }
            onChange={date =>
              changeReport(
                "StartDate",
                isDateGreater(new Date(date), new Date()) ? new Date() : date
              )
            }
          />
        </li>
      </ul>
      <ul className="form last">
        <li>
          <label>Project:</label>
          <select
            value={reportState.ProjectId}
            onChange={e => changeReport("ProjectId", e.target.value)}
          >
            <option value="">All</option>
            {renderProjects()}
          </select>
        </li>
        <li>
          <label>End date:</label>
          <DatePicker
            className="in-text datepicker"
            selected={
              isDateGreater(new Date(reportState.EndDate), new Date())
                ? new Date()
                : reportState.EndDate
            }
            onChange={date =>
              changeReport(
                "EndDate",
                isDateGreater(new Date(date), new Date()) ? new Date() : date
              )
            }
          />
        </li>
        <li>
          <a onClick={reset} href="/" className="btn orange right">
            Reset
          </a>
          <a onClick={search} href="/" className="btn green right">
            Search
          </a>
        </li>
      </ul>
    </div>
  );
}

export default GreyBox;
