import React, { useState, useEffect } from "react";
import { REQUIRED_FIELDS } from "../constants";
import { getInvalidFiels } from "../../../shared/helpers";
import ErrorMessage from "../../Shared/ErrorMessage";
import useStyles from "../../Shared/styles";

function DialogItems({ isLoading, error, createTeamMember, close }) {
  const [buttonClicked, setButtonClicked] = useState(false);
  const [isValid, setIsValid] = useState(true);
  const [teamMemberState, setTeamMemberState] = useState({
    Name: "",
    Username: "",
    HoursPerWeek: "",
    Email: "",
    IsActive: "",
    IsAdmin: ""
  });

  useEffect(() => {
    if (buttonClicked && !isLoading && !error) {
      close();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isLoading, buttonClicked, error]);

  const classes = useStyles();

  const handleChange = (key, value) =>
    setTeamMemberState({
      ...teamMemberState,
      [key]: value
    });

  const createTeamMemberHandler = e => {
    e.preventDefault();
    if (getInvalidFiels(teamMemberState, REQUIRED_FIELDS).length > 0) {
      return setIsValid(false);
    }
    if (isLoading) {
      return;
    }

    createTeamMember({
      ...teamMemberState,
      HoursPerWeek: Number(teamMemberState.HoursPerWeek)
    });
    setButtonClicked(true);
  };

  return (
    <>
      <ul className="form">
        <li>
          <label>Name:</label>
          <input
            value={teamMemberState.Name}
            onChange={e => handleChange("Name", e.target.value)}
            type="text"
            className="in-text"
          />
          {!isValid && (
            <ErrorMessage
              fieldName="Name"
              itemState={teamMemberState}
              requiredFields={REQUIRED_FIELDS}
            />
          )}
        </li>
        <li>
          <label>Hours per week:</label>
          <input
            value={teamMemberState.HoursPerWeek}
            onChange={e => handleChange("HoursPerWeek", e.target.value)}
            type="number"
            className={`in-text ${classes.fullWidth}`}
          />
          {!isValid && (
            <ErrorMessage
              fieldName="HoursPerWeek"
              text="Hours per week is required"
              itemState={teamMemberState}
              requiredFields={REQUIRED_FIELDS}
            />
          )}
        </li>
        <li>
          <label>Username:</label>
          <input
            value={teamMemberState.Username}
            onChange={e => handleChange("Username", e.target.value)}
            type="text"
            className="in-text"
          />
          {!isValid && (
            <ErrorMessage
              fieldName="Username"
              itemState={teamMemberState}
              requiredFields={REQUIRED_FIELDS}
            />
          )}
        </li>
        <li>
          <label>Email:</label>
          <input
            value={teamMemberState.Email}
            onChange={e => handleChange("Email", e.target.value)}
            type="email"
            className={`in-text ${classes.fullWidth}`}
          />
          {!isValid && (
            <ErrorMessage
              fieldName="Email"
              itemState={teamMemberState}
              requiredFields={REQUIRED_FIELDS}
            />
          )}
        </li>
        <li className="inline">
          <label>Status:</label>
          <span className="radio">
            <label htmlFor="inactive">Inactive:</label>
            <input
              onChange={() => handleChange("IsActive", false)}
              type="radio"
              value={teamMemberState.IsActive}
              name="status"
              id="inactive"
            />
          </span>
          <span className="radio">
            <label htmlFor="active">Active:</label>
            <input
              onChange={() => handleChange("IsActive", true)}
              type="radio"
              value={teamMemberState.IsActive}
              name="status"
              id="active"
            />
          </span>
          {!isValid && (
            <ErrorMessage
              fieldName="IsActive"
              text="Status is required"
              itemState={teamMemberState}
              requiredFields={REQUIRED_FIELDS}
            />
          )}
        </li>

        <li className="inline">
          <label>Role:</label>
          <span className="radio">
            <label htmlFor="admin">Admin:</label>
            <input
              onChange={() => handleChange("IsAdmin", true)}
              type="radio"
              value={teamMemberState.IsAdmin}
              name="role"
              id="admin"
            />
          </span>
          <span className="radio">
            <label htmlFor="worker">Worker:</label>
            <input
              onChange={() => handleChange("IsAdmin", false)}
              type="radio"
              value={teamMemberState.IsAdmin}
              name="role"
              id="worker"
            />
          </span>
          {!isValid && (
            <ErrorMessage
              fieldName="IsAdmin"
              text="Role is required"
              itemState={teamMemberState}
              requiredFields={REQUIRED_FIELDS}
            />
          )}
        </li>
      </ul>
      <div className="buttons">
        <div className="inner">
          <a
            onClick={createTeamMemberHandler}
            href="/"
            className={`btn green ${isLoading ? classes.disabled : ""}`}
          >
            Invite team member
          </a>
        </div>
      </div>
    </>
  );
}

export default DialogItems;
