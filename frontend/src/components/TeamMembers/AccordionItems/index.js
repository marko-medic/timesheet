import React, { useState } from "react";
import { REQUIRED_FIELDS } from "../constants";
import { mapItem, getInvalidFiels } from "shared/helpers";
import ErrorMessage from "../../Shared/ErrorMessage";
import useStyles from "../../Shared/styles";

function AccordionItems({
  teamMember,
  isLoading,
  updateTeamMember,
  removeTeamMember
}) {
  const [teamMemberState, setTeamMemberState] = useState(
    mapItem({ ...teamMember })
  );
  const [isValid, setIsValid] = useState(true);
  const classes = useStyles();
  const className = isLoading ? classes.disabled : "";

  const handleChange = (key, value) =>
    setTeamMemberState({
      ...teamMemberState,
      [key]: value
    });

  const removeTeamMemberHandler = e => {
    e.preventDefault();
    if (isLoading) {
      return;
    }
    removeTeamMember(teamMemberState.Id);
  };

  const updateTeamMemberHandler = e => {
    e.preventDefault();

    if (getInvalidFiels(teamMemberState, REQUIRED_FIELDS).length > 0) {
      return setIsValid(false);
    }
    if (isLoading) {
      return;
    }
    updateTeamMember({
      ...teamMemberState,
      HoursPerWeek: Number(teamMemberState.HoursPerWeek)
    });
  };

  return (
    <>
      <ul className="form">
        <li>
          <label>Name:</label>
          <input
            onChange={e => handleChange("Name", e.target.value)}
            value={teamMemberState.Name}
            type="text"
            className="in-text"
          />
          {!isValid && (
            <ErrorMessage
              fieldName="Name"
              requiredFields={REQUIRED_FIELDS}
              itemState={teamMemberState}
            />
          )}
        </li>
        <li>
          <label>Hours per week:</label>
          <input
            onChange={e => handleChange("HoursPerWeek", e.target.value)}
            value={teamMemberState.HoursPerWeek}
            type="number"
            step="0.1"
            className="in-text"
          />
        </li>
        {!isValid && (
          <ErrorMessage
            fieldName="HoursPerWeek"
            text="Hours per week is required"
            requiredFields={REQUIRED_FIELDS}
            itemState={teamMemberState}
          />
        )}
      </ul>
      <ul className="form">
        <li>
          <label>Username:</label>
          <input
            onChange={e => handleChange("Username", e.target.value)}
            value={teamMemberState.Username}
            type="text"
            className="in-text"
          />
          {!isValid && (
            <ErrorMessage
              fieldName="Username"
              requiredFields={REQUIRED_FIELDS}
              itemState={teamMemberState}
            />
          )}
        </li>
        <li>
          <label>Email:</label>
          <input
            onChange={e => handleChange("Email", e.target.value)}
            value={teamMemberState.Email}
            type="email"
            className="in-text"
          />
          {!isValid && (
            <ErrorMessage
              fieldName="Email"
              requiredFields={REQUIRED_FIELDS}
              itemState={teamMemberState}
            />
          )}
        </li>
      </ul>
      <ul className="form last">
        <li>
          <label>Status:</label>
          <span className="radio">
            <label htmlFor="inactive">Inactive:</label>
            <input
              onChange={() => handleChange("IsActive", false)}
              type="radio"
              id="inactive"
              checked={!teamMemberState.IsActive}
            />
          </span>
          <span className="radio">
            <label htmlFor="active">Active:</label>
            <input
              onChange={() => handleChange("IsActive", true)}
              type="radio"
              id="active"
              checked={teamMemberState.IsActive}
            />
          </span>
          {!isValid && (
            <ErrorMessage
              fieldName="IsActive"
              text="Status is required"
              requiredFields={REQUIRED_FIELDS}
              itemState={teamMemberState}
            />
          )}
        </li>
        <li>
          <label>Role:</label>
          <span className="radio">
            <label htmlFor="admin">Admin:</label>
            <input
              onChange={() => handleChange("IsAdmin", true)}
              type="radio"
              id="admin"
              checked={teamMemberState.IsAdmin}
            />
          </span>
          <span className="radio">
            <label htmlFor="worker">Worker:</label>
            <input
              onChange={() => handleChange("IsAdmin", false)}
              type="radio"
              id="worker"
              checked={!teamMemberState.IsAdmin}
            />
          </span>
          {!isValid && (
            <ErrorMessage
              fieldName="IsAdmin"
              text="Role is required"
              requiredFields={REQUIRED_FIELDS}
              itemState={teamMemberState}
            />
          )}
        </li>
      </ul>
      <div className="buttons">
        <div className="inner">
          <a
            onClick={updateTeamMemberHandler}
            href="/"
            className={`btn green ${className}`}
          >
            Save
          </a>
          <a
            onClick={removeTeamMemberHandler}
            href="/"
            className={`btn red ${className}`}
          >
            Delete
          </a>
          <a
            onClick={e => e.preventDefault()}
            href="/"
            className={`btn orange ${className}`}
          >
            Reset Password
          </a>
        </div>
      </div>
    </>
  );
}

export default AccordionItems;
