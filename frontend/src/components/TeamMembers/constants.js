export const REQUIRED_FIELDS = [
  "Name",
  "Username",
  "HoursPerWeek",
  "Email",
  "IsActive",
  "IsAdmin"
];
