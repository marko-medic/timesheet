import React from "react";

function TotalHours({ children }) {
  return (
    <div className="total">
      <span>
        Total hours: <em>{children}</em>
      </span>
    </div>
  );
}

export default TotalHours;
