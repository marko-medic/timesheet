import React from "react";
import { v4 } from "uuid";
import { DAYS_PAGE_PATH } from "../../../shared/pagePaths";
import MobileHead from "./MobileHead";
import Day from "./Day";
import Head from "./Head";
import {
  getBeginAndEndDate,
  isObjectEmpty,
  isDateGreater,
  areDatesSame
} from "../../../shared/helpers";
import { DAYS_IN_WEEK } from "./constants";
import useStyles from "../../Shared/styles";

function MonthTable({ date, timeSheet, daysPerTable }) {
  const classes = useStyles();
  const getDaysData = () => {
    const daysData = [];
    const { beginDate } = getBeginAndEndDate(date, daysPerTable);
    let dateCounter = beginDate.getDate();
    for (let i = 0; i < daysPerTable; i++) {
      const d = new Date(beginDate);
      d.setDate(dateCounter++);
      const dayFromDb = timeSheet.list.find(
        record => new Date(record.Date).toDateString() === d.toDateString()
      );

      const hoursPositive =
        dayFromDb && timeSheet.minExpectedHours <= dayFromDb.TotalHours;
      let className = "";
      if (
        isDateGreater(date, new Date()) ||
        (areDatesSame(date, new Date()) && isDateGreater(d, new Date()))
      ) {
        className = `${classes.disabled} disable`;
      } else if (hoursPositive) {
        className = "positive";
      } else if (dayFromDb && dayFromDb.TotalHours !== 0) {
        className = "negative";
      }

      daysData.push({
        text: d.getDate(),
        hours: dayFromDb ? dayFromDb.TotalHours : 0,
        dateString: d.toLocaleDateString().replace(/\//g, "-"),
        className
      });
    }
    return daysData;
  };

  const daysData = isObjectEmpty(timeSheet) ? [] : getDaysData();

  const renderWeek = week => (
    <tr>
      {daysData
        .slice(
          (week - 1) * DAYS_IN_WEEK,
          (week - 1) * DAYS_IN_WEEK + DAYS_IN_WEEK
        )
        .map(dayData => (
          <Day
            key={v4()}
            dayNum={dayData.text}
            className={dayData.className}
            hours={dayData.hours}
            href={`${DAYS_PAGE_PATH}/${dayData.dateString}`}
          />
        ))}
    </tr>
  );

  return (
    <table className="month-table">
      <tbody>
        <Head />
        <MobileHead />
        {renderWeek(1)}
        {renderWeek(2)}
        {renderWeek(3)}
        {renderWeek(4)}
        {renderWeek(5)}
      </tbody>
    </table>
  );
}

export default MonthTable;
