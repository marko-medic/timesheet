import React from "react";

// positive previous (prosli dani meseca koji imaju zabelezeno vreme)
// samo positive -> ovaj mesec gde je moguce editvati + 7.5 sati ili vise (imaju zabelezeno vreme)
// td bez klase -> vikend
// negative -> manje od 7.5 sati rada (moguce editovati)
// disable -> dani u buducnosti (ne mogu se editovati)

function Day({ href, className, hours, dayNum }) {
  return (
    <td className={className}>
      <div className="date">
        <span>{dayNum}.</span>
      </div>
      <div className="hours">
        <a href={href}>
          Hours: <span>{hours}</span>
        </a>
      </div>
    </td>
  );
}

export default Day;
