import React from "react";
import { getMonthName } from "shared/helpers";

function GreyBox({ date, setParam, path }) {
  const setDateHandler = (e, monthIncrement) => {
    e.preventDefault();
    const d = new Date(date);
    const newMonth = d.getMonth() + monthIncrement;
    d.setMonth(newMonth);
    const stateObject = {
      year: d.getFullYear(),
      month: d.getMonth() + 1
    };
    const newUrl = `${stateObject.year}-${stateObject.month}`;
    setParam(newUrl);
    window.history.replaceState(null, null, `${path}/${newUrl}`);
  };
  return (
    <div className="grey-box-wrap">
      <div className="top">
        <a onClick={e => setDateHandler(e, -1)} href="/" className="prev">
          <i className="zmdi zmdi-chevron-left" />
          previous month
        </a>
        <span className="center">
          {getMonthName(date.getMonth())}, {date.getFullYear()}
        </span>
        <a onClick={e => setDateHandler(e, 1)} href="/" className="next">
          next month
          <i className="zmdi zmdi-chevron-right" />
        </a>
      </div>
      <div className="bottom" />
    </div>
  );
}

export default GreyBox;
