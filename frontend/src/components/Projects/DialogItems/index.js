import React, { useState, useEffect } from "react";
import { getInvalidFiels } from "../../../shared/helpers";
import { REQUIRED_FIELDS } from "../constants";
import ErrorMessage from "../../Shared/ErrorMessage";
import useStyles from "../../Shared/styles";

function DialogItems({
  clients,
  teamMembers,
  createProject,
  close,
  error,
  isLoading
}) {
  const [buttonClicked, setButtonClicked] = useState(false);
  const [isValid, setIsValid] = useState(true);
  const [projectState, setProjectState] = useState({
    Name: "",
    Description: "",
    ClientId: "",
    TeamMemberId: ""
  });
  const classes = useStyles();

  useEffect(() => {
    if (buttonClicked && !isLoading && !error) {
      close();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isLoading, buttonClicked, error]);

  const renderClients = () =>
    clients.map(client => (
      <option key={client.Id} value={client.Id}>
        {client.Name}
      </option>
    ));

  const renderTeamMembers = () =>
    teamMembers.map(teamMember => (
      <option key={teamMember.Id} value={teamMember.Id}>
        {teamMember.Name}
      </option>
    ));

  const createProjectHandler = e => {
    e.preventDefault();

    if (getInvalidFiels(projectState, REQUIRED_FIELDS).length > 0) {
      return setIsValid(false);
    }

    createProject({
      ...projectState,
      IsActive: true
    });
    setButtonClicked(true);
  };

  const inputHandler = (key, value) =>
    setProjectState({ ...projectState, [key]: value });

  return (
    <>
      <ul className="form">
        <li>
          <label>Project name:</label>
          <input
            value={projectState.Name}
            onChange={e => inputHandler("Name", e.target.value)}
            type="text"
            className="in-text"
          />
          {!isValid && (
            <ErrorMessage
              fieldName="Name"
              itemState={projectState}
              requiredFields={REQUIRED_FIELDS}
            />
          )}
        </li>
        <li>
          <label>Description:</label>
          <input
            value={projectState.Description}
            onChange={e => inputHandler("Description", e.target.value)}
            type="text"
            className="in-text"
          />
        </li>

        <li>
          <label>Client:</label>
          <select
            value={projectState.ClientId}
            onChange={e => inputHandler("ClientId", e.target.value)}
          >
            <option value="">Select client</option>
            {renderClients()}
          </select>
          {!isValid && (
            <ErrorMessage
              fieldName="ClientId"
              text="You must select client"
              itemState={projectState}
              requiredFields={REQUIRED_FIELDS}
            />
          )}
        </li>
        <li>
          <label>Lead:</label>
          <select
            value={projectState.TeamMemberId}
            onChange={e => inputHandler("TeamMemberId", e.target.value)}
          >
            <option value="">Select lead</option>
            {renderTeamMembers()}
          </select>
          {!isValid && (
            <ErrorMessage
              fieldName="TeamMemberId"
              text="You must select team leader"
              itemState={projectState}
              requiredFields={REQUIRED_FIELDS}
            />
          )}
        </li>
      </ul>
      <div className="buttons">
        <div className="inner">
          <a
            onClick={createProjectHandler}
            href="/"
            className={`btn green ${isLoading ? classes.disabled : ""}`}
          >
            Save
          </a>
        </div>
      </div>
    </>
  );
}

export default DialogItems;
