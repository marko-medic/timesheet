import React, { useState } from "react";
import { mapItem, getInvalidFiels } from "shared/helpers";
import { REQUIRED_FIELDS } from "../constants";
import ErrorMessage from "../../Shared/ErrorMessage";
import useStyles from "../../Shared/styles";

function AccordionItems({
  project,
  updateProject,
  removeProject,
  isLoading,
  clients,
  teamMembers
}) {
  const [projectState, setProjectState] = useState(mapItem(project));
  const [isValid, setIsValid] = useState(true);
  const classes = useStyles();

  const handleChange = (key, value) =>
    setProjectState({ ...projectState, [key]: value });

  const renderClients = () =>
    clients.map(client => (
      <option key={client.Id} value={client.Id}>
        {client.Name}
      </option>
    ));

  const renderTeamMembers = () =>
    teamMembers.map(teamMember => (
      <option key={teamMember.Id} value={teamMember.Id}>
        {teamMember.Name}
      </option>
    ));

  const updateProjectHandler = e => {
    e.preventDefault();
    if (getInvalidFiels(projectState, REQUIRED_FIELDS).length > 0) {
      return setIsValid(false);
    }
    if (isLoading) {
      return;
    }
    updateProject({
      ...projectState
    });
  };

  const removeProjectHandler = e => {
    e.preventDefault();
    if (isLoading) {
      return;
    }
    removeProject(projectState.Id);
  };

  return (
    <>
      <ul className="form">
        <li>
          <label>Project name:</label>
          <input
            value={projectState.Name}
            onChange={e => handleChange("Name", e.target.value)}
            type="text"
            className="in-text"
          />
          {!isValid && (
            <ErrorMessage
              fieldName="Name"
              itemState={projectState}
              requiredFields={REQUIRED_FIELDS}
            />
          )}
        </li>
        <li>
          <label>Lead:</label>
          <select
            value={projectState.TeamMemberId}
            onChange={e => handleChange("TeamMemberId", e.target.value)}
          >
            <option value="">Select lead</option>
            {renderTeamMembers()}
          </select>
        </li>
        {!isValid && (
          <ErrorMessage
            fieldName="TeamMemberId"
            text="You must select lead member"
            itemState={projectState}
            requiredFields={REQUIRED_FIELDS}
          />
        )}
      </ul>
      <ul className="form">
        <li>
          <label>Description:</label>
          <input
            value={projectState.Description}
            onChange={e => handleChange("Description", e.target.value)}
            type="text"
            className="in-text"
          />
        </li>
      </ul>
      <ul className="form last">
        <li>
          <label>Client:</label>
          <select
            value={projectState.ClientId}
            onChange={e => handleChange("ClientId", e.target.value)}
          >
            <option value="">Select client</option>
            {renderClients()}
          </select>
          {!isValid && (
            <ErrorMessage
              fieldName="ClientId"
              text="You must select client"
              itemState={projectState}
              requiredFields={REQUIRED_FIELDS}
            />
          )}
        </li>
        <li className="inline">
          <label>Status:</label>
          <span className="radio">
            <label htmlFor="inactive">Inactive:</label>
            <input
              type="radio"
              onChange={() => handleChange("IsActive", false)}
              checked={!projectState.IsActive}
              id="inactive"
            />
          </span>
          <span className="radio">
            <label htmlFor="active">Active:</label>
            <input
              type="radio"
              onChange={() => handleChange("IsActive", true)}
              checked={projectState.IsActive}
              id="active"
            />
          </span>
          {!isValid && (
            <ErrorMessage
              fieldName="IsActive"
              text="Status is required"
              itemState={projectState}
              requiredFields={REQUIRED_FIELDS}
            />
          )}
        </li>
      </ul>
      <div className="buttons">
        <div className="inner">
          <a
            onClick={updateProjectHandler}
            href="/"
            className={`btn green ${isLoading ? classes.disabled : ""}`}
          >
            Save
          </a>
          <a
            onClick={removeProjectHandler}
            href="/"
            className={`btn red ${isLoading ? classes.disabled : ""}`}
          >
            Delete
          </a>
        </div>
      </div>
    </>
  );
}

export default AccordionItems;
