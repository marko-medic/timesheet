import React, { useState } from "react";
import { getFirstLetters, getCurrentLetter } from "shared/helpers";

function LetterMenu({ items, filterAction }) {
  const [selectedLetter, setSelectedLetter] = useState(
    getCurrentLetter(window.location.href)
  );
  const firstLetters = getFirstLetters(items);

  const searchHandler = showAll => e => {
    e.preventDefault();
    const letter = showAll ? "" : e.target.textContent;
    const queryString = showAll ? "" : `?firstLetter=${letter}`;
    setSelectedLetter(letter);
    const newUrl = `${window.location.pathname}${queryString}`;
    window.history.replaceState("", "", newUrl);
    filterAction({ firstLetter: letter });
  };

  const renderLetters = () => {
    let firstLetterCode = "A".charCodeAt();
    const arrayCount = 26;
    const letters = Array.from(Array(arrayCount), (_, index) => {
      const currentLetter = String.fromCharCode(firstLetterCode + index);
      const isLetterActive = selectedLetter === currentLetter;
      const activeClass = isLetterActive ? "active" : "";
      const isLetterDisabled = !firstLetters.includes(currentLetter);
      const listElement = isLetterDisabled ? (
        <li key={currentLetter} className="disabled">
          <a onClick={e => e.preventDefault()} href="/">
            {currentLetter}
          </a>
        </li>
      ) : (
        <li key={currentLetter} className={`${activeClass}`}>
          <a
            onClick={searchHandler(isLetterActive)}
            href={`?firstLetter=${currentLetter}`}
          >
            {currentLetter}
          </a>
        </li>
      );
      return listElement;
    });
    return letters;
  };

  return (
    <div className="alpha">
      <ul>{renderLetters()}</ul>
    </div>
  );
}

export default LetterMenu;
