import React from "react";
import uuid from "uuid";
import { ROWS_PER_PAGE } from "../../../shared/constants";

function Pagination({ itemsCount, currentPage, setPage }) {
  const numberOfPages = Math.max(1, Math.ceil(itemsCount / ROWS_PER_PAGE));

  const clickHandler = page => e => {
    e.preventDefault();
    const newUrl = `${window.location.pathname}?page=${page}`;
    window.history.replaceState("", "", newUrl);
    setPage(page);
  };

  const renderPageLinks = () =>
    Array.from(Array(numberOfPages), (_, index) => (
      <li key={uuid.v4()}>
        <a onClick={clickHandler(index + 1)} href={`?page=${index + 1}`}>
          {index + 1}
        </a>
      </li>
    ));

  return (
    <div className="pagination">
      <ul>
        {currentPage !== 1 && numberOfPages !== 1 && (
          <li>
            <a onClick={clickHandler(currentPage - 1)} href="/">
              Previous
            </a>
          </li>
        )}
        {renderPageLinks()}
        {currentPage !== numberOfPages && numberOfPages !== 1 && (
          <li>
            <a onClick={clickHandler(currentPage + 1)} href="/">
              Next
            </a>
          </li>
        )}
      </ul>
    </div>
  );
}

export default Pagination;
