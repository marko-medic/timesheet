import React from "react";

function PageTitle({ textClass, children }) {
  return (
    <h2>
      <i className={`ico ${textClass}`} />
      {children}
    </h2>
  );
}

export default PageTitle;
