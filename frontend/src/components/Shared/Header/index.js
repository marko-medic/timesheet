import React from "react";
import logo from "../../../assets/img/logo.png";
import TopMenu from "./Menu/TopMenu";
import DesktopMenu from "./Menu/DesktopMenu";
import MobileMenu from "./Menu/MobileMenu";

function Header() {
  return (
    <header className="header">
      <div className="top-bar" />
      <div className="wrapper">
        <a href="/" className="logo">
          <img src={logo} alt="VegaITSourcing Timesheet" />
        </a>
        <TopMenu />
        <nav>
          <DesktopMenu />
          <MobileMenu />
          <span className="line" />
        </nav>
      </div>
    </header>
  );
}

export default Header;
