import React from "react";
import { NavLink } from "react-router-dom";

function TopMenu() {
  return (
    <ul className="user right">
      <li>
        <NavLink to="/">Sladjana Miljanovic</NavLink>
        <div className="invisible" />
        <div className="user-menu">
          <ul>
            <li>
              <a href="/" className="link">
                Change password
              </a>
            </li>
            <li>
              <a href="/" className="link">
                Settings
              </a>
            </li>
            <li>
              <a href="/" className="link">
                Export all data
              </a>
            </li>
          </ul>
        </div>
      </li>
      <li className="last">
        <NavLink to="/">Logout</NavLink>
      </li>
    </ul>
  );
}

export default TopMenu;
