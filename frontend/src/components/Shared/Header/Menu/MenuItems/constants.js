import {
  TIMESHEET_PAGE_PATH,
  CLIENTS_PAGE_PATH,
  PROJECTS_PAGE_PATH,
  CATEGORIES_PAGE_PATH,
  TEAM_MEMBERS_PAGE_PATH,
  REPORTS_PAGE_PATH
} from "shared/pagePaths";

export const MENU_ITEMS = [
  { text: "TimeSheet", url: TIMESHEET_PAGE_PATH, exact: false },
  { text: "Clients", url: CLIENTS_PAGE_PATH, exact: false },
  { text: "Projects", url: PROJECTS_PAGE_PATH, exact: false },
  { text: "Categories", url: CATEGORIES_PAGE_PATH, exact: false },
  { text: "Team members", url: TEAM_MEMBERS_PAGE_PATH, exact: false },
  { text: "Reports", url: REPORTS_PAGE_PATH, exact: false }
];
