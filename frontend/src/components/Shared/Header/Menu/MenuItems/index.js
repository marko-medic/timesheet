import React from "react";
import { NavLink } from "react-router-dom";
import { MENU_ITEMS } from "./constants";

function MenuItems() {
  return MENU_ITEMS.map(item => (
    <li key={item.text}>
      <NavLink exact={item.exact} to={item.url} className="btn nav">
        {item.text}
      </NavLink>
    </li>
  ));
}

export default MenuItems;
