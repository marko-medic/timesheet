import React from "react";
import { NavLink } from "react-router-dom";
import MenuItems from "../MenuItems";

function MobileMenu() {
  return (
    <div className="mobile-menu">
      <NavLink to="/" className="menu-btn">
        <i className="zmdi zmdi-menu" />
      </NavLink>
      <ul>
        <MenuItems />
      </ul>
    </div>
  );
}

export default MobileMenu;
