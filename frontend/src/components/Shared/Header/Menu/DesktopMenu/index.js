import React from "react";
import MenuItems from "../MenuItems";

function UserMenu() {
  return (
    <ul className="menu">
      <MenuItems />
    </ul>
  );
}

export default UserMenu;
