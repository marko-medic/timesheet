import { makeStyles } from "@material-ui/styles";
import fancyBoxImg from "../../../assets/img/fancybox/fancybox.png";

export default makeStyles({
  overlay: {
    background: "rgba(0, 0, 0, .5)",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    position: "fixed",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    zIndex: 4
  },
  memberWrap: {
    display: "block",
    position: "relative",
    zIndex: 5
  },
  memberInner: {
    background: "white",
    padding: "1rem",
    zIndex: 5
  },
  close: {
    position: "absolute",
    top: "-15px",
    right: "-15px",
    width: "30px",
    height: "30px",
    background: `transparent url(${fancyBoxImg}) -40px 0px`,
    cursor: "pointer",
    zIndex: 1103
  }
});
