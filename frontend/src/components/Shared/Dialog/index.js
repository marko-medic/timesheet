import React from "react";
import useStyles from "./styles";

function Dialog({ title, close, errorMessage, children }) {
  const classes = useStyles();

  return (
    <div
      className={classes.overlay}
      onClick={e => {
        if (e.target === e.currentTarget) {
          close();
        }
      }}
    >
      <div className={`new-member-wrap ${classes.memberWrap}`}>
        <a
          className={classes.close}
          href="/"
          onClick={e => {
            e.preventDefault();
            close();
          }}
        >
          &times;
        </a>
        <div
          id="new-member"
          className={`new-member-inner ${classes.memberInner}`}
        >
          <h2>{title}</h2>
          {errorMessage && <span>Error: {errorMessage}</span>}
          {children}
        </div>
      </div>
    </div>
  );
}

export default Dialog;
