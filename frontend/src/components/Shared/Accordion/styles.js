import { makeStyles } from "@material-ui/styles";

export default makeStyles({
  shown: {
    display: "block !important"
  }
});
