import React, { useState } from "react";
import { animated, useTransition } from "react-spring";
import useStyles from "./styles";

function Accordion({ title, children }) {
  const [isOpen, toggleOpen] = useState(false);
  const transitions = useTransition(isOpen, null, {
    from: { opacity: 0 },
    enter: { opacity: 1 },
    leave: { opacity: 0 }
  });
  const classes = useStyles();
  return (
    <div className="item">
      <div className="heading" onClick={() => toggleOpen(!isOpen)}>
        <span>{title}</span>
        <i>+</i>
      </div>
      {transitions.map(
        ({ item, key, props }) =>
          item && (
            <animated.div key={key} style={props}>
              <div className={`details ${classes.shown}`}>{children}</div>
            </animated.div>
          )
      )}
    </div>
  );
}

export default Accordion;
