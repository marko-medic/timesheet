import React from "react";
import { getInvalidFiels } from "../../../shared/helpers";

function ErrorMessage({ fieldName, text = "", itemState, requiredFields }) {
  return getInvalidFiels(itemState, requiredFields).includes(fieldName) ? (
    <span>{text ? text : `${fieldName} is required`}</span>
  ) : null;
}

export default ErrorMessage;
