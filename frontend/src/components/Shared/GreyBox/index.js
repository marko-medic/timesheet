import React, { useState } from "react";

function GreyBox({
  inputName,
  show,
  openDialog,
  searchItems,
  titleClass = "",
  children
}) {
  const [inputText, setInputText] = useState("");

  const inputHandler = e => {
    const text = e.target.value.trim();
    setInputText(text);
    const queryString = text ? `?searchTerm=${text}` : "";
    const newUrl = `${window.location.pathname}${queryString}`;
    window.history.replaceState("", "", newUrl);
    searchItems({
      searchTerm: text
    });
  };

  return (
    <div className={`grey-box-wrap reports ${titleClass}`}>
      <a
        href="/"
        onClick={e => {
          e.preventDefault();
          openDialog();
        }}
        className="link new-member-popup"
      >
        {children}
      </a>
      {show && (
        <div className="search-page">
          <input
            value={inputText}
            onChange={inputHandler}
            type="search"
            name={inputName}
            className="in-search"
          />
        </div>
      )}
    </div>
  );
}

export default GreyBox;
