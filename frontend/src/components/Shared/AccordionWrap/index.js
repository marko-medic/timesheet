import React from "react";

function AccordionWrap({ accordionClass = "", children }) {
  return <div className={`accordion-wrap ${accordionClass}`}>{children}</div>;
}

export default AccordionWrap;
