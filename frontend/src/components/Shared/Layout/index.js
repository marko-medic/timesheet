import React from "react";
import { withRouter } from "react-router-dom";
import { LOGIN_PAGE_PATH } from "shared/pagePaths";
import Header from "../Header";
import Footer from "../Footer";

function Layout({ history, children }) {
  const { pathname } = history.location;
  const loginPage = pathname === LOGIN_PAGE_PATH;
  const layout = loginPage ? (
    <div className="wrapper centered">{children}</div>
  ) : (
    <div className="container">
      <Header />
      <div className="wrapper">
        <section className="content">{children}</section>
      </div>
      <Footer />
    </div>
  );

  return layout;
}

export default withRouter(Layout);
