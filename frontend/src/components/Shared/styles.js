import { makeStyles } from "@material-ui/styles";

export default makeStyles({
  disabled: {
    pointerEvents: "none",
    cursor: "not-allowed",
    opacity: ".7"
  },
  fullWidth: {
    width: "100% !important"
  }
});
