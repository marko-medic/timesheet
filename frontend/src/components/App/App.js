import React, { Suspense, lazy } from "react";
import { withRouter, Switch, Route, Redirect } from "react-router-dom";
import Layout from "../Shared/Layout";
import {
  TIMESHEET_PAGE_PATH,
  CLIENTS_PAGE_PATH,
  PROJECTS_PAGE_PATH,
  CATEGORIES_PAGE_PATH,
  TEAM_MEMBERS_PAGE_PATH,
  REPORTS_PAGE_PATH,
  LOGIN_PAGE_PATH,
  TIMESHEET_COMPONENT_PATH,
  DAYS_COMPONENT_PAGE_PATH
} from "../../shared/pagePaths";

const TimeSheet = lazy(() => import("../../pages/TimeSheet"));
const Clients = lazy(() => import("../../pages/Clients"));
const Projects = lazy(() => import("../../pages/Projects"));
const Categories = lazy(() => import("../../pages/Categories"));
const TeamMembers = lazy(() => import("../../pages/TeamMembers"));
const Reports = lazy(() => import("../../pages/Reports"));
const Days = lazy(() => import("../../pages/Days"));
const Login = lazy(() => import("../../pages/Login"));

function App() {
  return (
    <Layout>
      <Suspense fallback={<div>Loading...</div>}>
        <Switch>
          <Route path={TIMESHEET_COMPONENT_PATH} component={TimeSheet} />
          <Route path={CLIENTS_PAGE_PATH} component={Clients} />
          <Route path={PROJECTS_PAGE_PATH} component={Projects} />
          <Route path={CATEGORIES_PAGE_PATH} component={Categories} />
          <Route path={TEAM_MEMBERS_PAGE_PATH} component={TeamMembers} />
          <Route path={REPORTS_PAGE_PATH} component={Reports} />
          <Route path={DAYS_COMPONENT_PAGE_PATH} component={Days} />
          <Route path={LOGIN_PAGE_PATH} component={Login} />
          <Redirect to={TIMESHEET_PAGE_PATH} />
        </Switch>
      </Suspense>
    </Layout>
  );
}

export default withRouter(App);
